<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Part-III-MoveFast" ID="ID_1817315106" CREATED="1451538604763" MODIFIED="1451539061620" LINK="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;.mm">
<edge STYLE="bezier" COLOR="#7c007c" WIDTH="thin"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="8"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="08-&#x7528;&#x52a8;&#x4f5c;&#x547d;&#x4ee4;&#x5728;&#x6587;&#x6863;&#x4e2d;&#x79fb;&#x52a8;" POSITION="right" ID="ID_1182728901" CREATED="1451538630078" MODIFIED="1453284125370">
<edge COLOR="#7c0000"/>
<node TEXT="no.46&#x8ba9;&#x624b;&#x6307;&#x4fdd;&#x6301;&#x5728;&#x672c;&#x4f4d;&#x884c;&#x4e0a;" ID="ID_747654250" CREATED="1453284213909" MODIFIED="1453715389145"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      motions/disable-arrowkeys.vim
    </p>
  </body>
</html>
</richcontent>
<node TEXT="noremap &lt;Up&gt; &lt;Nop&gt;&#xa;noremap &lt;Down&gt; &lt;Nop&gt;&#xa;noremap &lt;Left&gt; &lt;Nop&gt;&#xa;noremap &lt;Right&gt; &lt;Nop&gt;" ID="ID_114509117" CREATED="1453715367977" MODIFIED="1453715431428"/>
</node>
<node TEXT="no.47&#x533a;&#x5206;&#x5b9e;&#x9645;&#x884c;&#x4e0e;&#x5c4f;&#x5e55;&#x884c;" ID="ID_1852828692" CREATED="1453702724154" MODIFIED="1453715478853"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      motions/cursor-maps.vim
    </p>
  </body>
</html>
</richcontent>
<node TEXT="nnoremap k gk&#xa;nnoremap gk k&#xa;nnoremap j gj&#xa;nnoremap gj j" ID="ID_82364908" CREATED="1453715480949" MODIFIED="1453715512791"/>
</node>
<node TEXT="no.48&#x57fa;&#x4e8e;&#x5355;&#x8bcd;&#x79fb;&#x52a8;" ID="ID_273220093" CREATED="1453702712237" MODIFIED="1453715646861"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h word-motions
    </p>
    <p>
      :h word
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="no.49&#x5bf9;&#x5b57;&#x7b26;&#x8fdb;&#x884c;&#x67e5;&#x627e;" ID="ID_1945211642" CREATED="1453702712237" MODIFIED="1453715682051"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h f
    </p>
  </body>
</html>
</richcontent>
<node TEXT="noremap &lt;Leader&gt;n nzz&#xa;noremap &lt;Leader&gt;N Nzz&#xa;let mapleader=&quot;,&quot;&#xa;normap \ ," ID="ID_562282122" CREATED="1453715704590" MODIFIED="1453715787020"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h mapleader
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x9009;&#x62e9;&#x975e;&#x5e38;&#x89c1;&#x5b57;&#x6bcd;&#x5f62;&#x6210;&#x4e60;&#x60ef;" ID="ID_1342796941" CREATED="1453715824278" MODIFIED="1453715843998">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="no.50&#x901a;&#x8fc7;&#x67e5;&#x627e;&#x8fdb;&#x884c;&#x79fb;&#x52a8;" ID="ID_186728267" CREATED="1453702712239" MODIFIED="1453715876965"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      motions/search-haiku.txt
    </p>
  </body>
</html>
</richcontent>
<node TEXT="set hlsearch" ID="ID_947382672" CREATED="1453715917928" MODIFIED="1453715924929"/>
<node TEXT="set incsearch" ID="ID_1427176684" CREATED="1453715925864" MODIFIED="1453715936343"/>
<node TEXT="d\ge" ID="ID_998200543" CREATED="1453716029041" MODIFIED="1453716040089"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h exclusive
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="no.81" ID="ID_170416157" CREATED="1453715936736" MODIFIED="1453715940113"/>
</node>
<node TEXT="no.51&#x7528;&#x7cbe;&#x786e;&#x7684;&#x6587;&#x672c;&#x5bf9;&#x8c61;&#x9009;&#x62e9;&#x9009;&#x533a;" ID="ID_426069722" CREATED="1453702712242" MODIFIED="1453942764029"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      motions/template.js
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x5206;&#x9694;&#x7b26;&#x6587;&#x672c;&#x5bf9;&#x8c61;" ID="ID_1456060243" CREATED="1453942614513" MODIFIED="1453942853405"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h text-objects
    </p>
    <p>
      a for around
    </p>
    <p>
      i for inside
    </p>
  </body>
</html>
</richcontent>
<node TEXT="a) ab" ID="ID_1836966946" CREATED="1453942622300" MODIFIED="1453942629389"/>
<node TEXT="i) ib" ID="ID_1783463847" CREATED="1453942630872" MODIFIED="1453942641895"/>
<node TEXT="a} aB" ID="ID_1848436836" CREATED="1453942644652" MODIFIED="1453942652889"/>
<node TEXT="i} iB" ID="ID_843487723" CREATED="1453942654177" MODIFIED="1453942659728"/>
<node TEXT="a]" ID="ID_329661013" CREATED="1453942661223" MODIFIED="1453942684499"/>
<node ID="ID_272884488" CREATED="1453942686914" MODIFIED="1453942686914"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      i]
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="a&gt;" ID="ID_94410154" CREATED="1453942661223" MODIFIED="1453942700326"/>
<node TEXT="i&gt;" ID="ID_1519287359" CREATED="1453942686914" MODIFIED="1453942702906"/>
<node TEXT="a&quot;" ID="ID_600445694" CREATED="1453942661223" MODIFIED="1453942707251"/>
<node TEXT="i&quot;" ID="ID_1601818268" CREATED="1453942686914" MODIFIED="1453942709794"/>
<node TEXT="a&apos;" ID="ID_1649457599" CREATED="1453942661223" MODIFIED="1453942711906"/>
<node TEXT="i&apos;" ID="ID_1343188524" CREATED="1453942686914" MODIFIED="1453942715823"/>
<node TEXT="a`" ID="ID_115841169" CREATED="1453942661223" MODIFIED="1453942742277">
<font NAME="DejaVu Sans Mono"/>
</node>
<node TEXT="i`" ID="ID_1219342140" CREATED="1453942686914" MODIFIED="1453942742285">
<font NAME="DejaVu Sans Mono"/>
</node>
<node TEXT="at" ID="ID_280700834" CREATED="1453942661223" MODIFIED="1453942754781">
<font NAME="DejaVu Sans Mono"/>
</node>
<node TEXT="it" ID="ID_249907136" CREATED="1453942686914" MODIFIED="1453942757020">
<font NAME="DejaVu Sans Mono"/>
</node>
</node>
</node>
<node TEXT="no.52&#x5220;&#x9664;&#x5468;&#x8fb9;,&#x4fee;&#x6539;&#x5185;&#x90e8;" ID="ID_717360753" CREATED="1453702712244" MODIFIED="1453702712244">
<node TEXT="&#x8303;&#x56f4;&#x6587;&#x672c;&#x5bf9;&#x8c61;" ID="ID_649097712" CREATED="1453942791887" MODIFIED="1453942861154"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      a for around
    </p>
    <p>
      i for inside
    </p>
  </body>
</html>
</richcontent>
<node TEXT="iw" ID="ID_348741324" CREATED="1453942797887" MODIFIED="1453942803107"/>
<node TEXT="aw" ID="ID_1129062049" CREATED="1453942804936" MODIFIED="1453942808011"/>
<node TEXT="iW" ID="ID_596698418" CREATED="1453942797887" MODIFIED="1453942819505"/>
<node TEXT="aW" ID="ID_1353862987" CREATED="1453942804936" MODIFIED="1453942820914"/>
<node TEXT="is" ID="ID_698643168" CREATED="1453942797887" MODIFIED="1453942823743"/>
<node TEXT="as" ID="ID_770994694" CREATED="1453942804936" MODIFIED="1453942825807"/>
<node TEXT="ip" ID="ID_1363504108" CREATED="1453942797887" MODIFIED="1453942828232"/>
<node TEXT="ap" ID="ID_1219972762" CREATED="1453942804936" MODIFIED="1453942829876"/>
</node>
</node>
<node TEXT="no.53&#x8bbe;&#x7f6e;&#x4f4d;&#x7f6e;&#x6807;&#x8bb0;,&#x4ee5;&#x4fbf;&#x5feb;&#x901f;&#x8df3;&#x56de;" ID="ID_494657323" CREATED="1453702712246" MODIFIED="1453702712246">
<node TEXT="m{a-zA-Z}" ID="ID_1816961263" CREATED="1453942877839" MODIFIED="1453942893063"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h m
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="mm `m" ID="ID_1989177384" CREATED="1453942908285" MODIFIED="1453942925207">
<font NAME="DejaVu Sans Mono"/>
</node>
<node TEXT="&#x81ea;&#x52a8;&#x4f4d;&#x7f6e;&#x6807;&#x8bb0;" ID="ID_1300196773" CREATED="1453942946390" MODIFIED="1453942953831">
<node TEXT="``" ID="ID_1749064962" CREATED="1453942965856" MODIFIED="1453943051573">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x5f53;&#x524d;&#x6587;&#x4ef6;&#x4e2d;&#x4e0a;&#x6b21;&#x8df3;&#x8f6c;&#x52a8;&#x4f5c;&#x4e4b;&#x524d;&#x7684;&#x4f4d;&#x7f6e;" ID="ID_451260084" CREATED="1453943011116" MODIFIED="1453943027353"/>
</node>
<node TEXT="`." ID="ID_1601841282" CREATED="1453942969830" MODIFIED="1453943051574">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x4e0a;&#x6b21;&#x4fee;&#x6539;&#x7684;&#x5730;&#x65b9;" ID="ID_1376316214" CREATED="1453943029631" MODIFIED="1453943035149"/>
</node>
<node TEXT="`^" ID="ID_1260345576" CREATED="1453942973961" MODIFIED="1453943051576">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x4e0a;&#x6b21;&#x63d2;&#x5165;&#x7684;&#x5730;&#x65b9;" ID="ID_174215225" CREATED="1453943066273" MODIFIED="1453943076143"/>
</node>
<node TEXT="`[" ID="ID_1590827464" CREATED="1453942977236" MODIFIED="1453943051577">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x4e0a;&#x6b21;&#x4fee;&#x6539;&#x6216;&#x590d;&#x5236;&#x7684;&#x8d77;&#x59cb;&#x4f4d;&#x7f6e;" ID="ID_207020017" CREATED="1453943078100" MODIFIED="1453943088888"/>
</node>
<node TEXT="`]" ID="ID_932193601" CREATED="1453942980699" MODIFIED="1453943051577">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x4e0a;&#x6b21;&#x4fee;&#x6539;&#x6216;&#x590d;&#x5236;&#x7684;&#x7ed3;&#x675f;&#x4f4d;&#x7f6e;" ID="ID_1312193318" CREATED="1453943091032" MODIFIED="1453943100180"/>
</node>
<node TEXT="`&lt;" ID="ID_360211638" CREATED="1453942983755" MODIFIED="1453943051578">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x4e0a;&#x6b21;&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x7684;&#x8d77;&#x59cb;&#x4f4d;&#x7f6e;" ID="ID_1806357222" CREATED="1453943102523" MODIFIED="1453943115928"/>
</node>
<node TEXT="`&gt;" ID="ID_1649518031" CREATED="1453942987491" MODIFIED="1453943051569">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x4e0a;&#x6b21;&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x7684;&#x7ed3;&#x675f;&#x4f4d;&#x7f6e;" ID="ID_1648122536" CREATED="1453943117078" MODIFIED="1453943121513"/>
</node>
</node>
</node>
<node TEXT="no.54&#x5728;&#x5339;&#x914d;&#x62ec;&#x53f7;&#x95f4;&#x8df3;&#x8f6c;" ID="ID_251329843" CREATED="1453702712248" MODIFIED="1453702712248">
<node TEXT="%" ID="ID_1813065086" CREATED="1453943137209" MODIFIED="1453943143803"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h %
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x5728;&#x914d;&#x5bf9;&#x7684;&#x5173;&#x952e;&#x5b57;&#x4e4b;&#x95f4;&#x8df3;&#x8f6c;" ID="ID_1867498409" CREATED="1453943159184" MODIFIED="1453943184903"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h matchit-install
    </p>
  </body>
</html>
</richcontent>
<node TEXT="set nocompatible&#xa;filetype plugin on&#xa;runtime macros/matchit.vim" ID="ID_74012100" CREATED="1453943190548" MODIFIED="1453943216548"/>
<node TEXT="plugin surround.vim" ID="ID_382606918" CREATED="1453943230762" MODIFIED="1453943299286"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      tim pope: http://github.com/tpope/vim-surround
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="09-&#x5728;&#x6587;&#x4ef6;&#x4e4b;&#x95f4;&#x8df3;&#x8f6c;" POSITION="right" ID="ID_1651133525" CREATED="1451538648922" MODIFIED="1453284127433">
<edge COLOR="#00007c"/>
<node TEXT="no.55&#x904d;&#x5386;&#x8df3;&#x8f6c;&#x5217;&#x8868;" ID="ID_1989047449" CREATED="1453702864046" MODIFIED="1453702864046">
<node TEXT="&lt;C-o&gt;" ID="ID_939353140" CREATED="1453943315650" MODIFIED="1453943354197"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21518;&#36864;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;C-i&gt;" ID="ID_1339681334" CREATED="1453943328180" MODIFIED="1453943352500"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21069;&#36827;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT=":jumps" ID="ID_1966668531" CREATED="1453943362642" MODIFIED="1453943365720"/>
<node TEXT="&#x8df3;&#x8f6c;&#x52a8;&#x4f5c;" ID="ID_1537890629" CREATED="1453943432066" MODIFIED="1453943436433">
<node TEXT="[count]G" ID="ID_449867697" CREATED="1453943380985" MODIFIED="1453943387988">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x6307;&#x5b9a;&#x884c;&#x53f7;" ID="ID_1372945513" CREATED="1453943647171" MODIFIED="1453943654637"/>
</node>
<node TEXT="/pattern&lt;CR&gt;/?pattern&lt;CR&gt;/n/N" ID="ID_588344406" CREATED="1453943449324" MODIFIED="1453943470543">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x4e0b;&#x4e00;&#x4e2a;/&#x4e0a;&#x4e00;&#x4e2a;&#x6a21;&#x5f0f;&#x51fa;&#x73b0;&#x4e4b;&#x5904;" ID="ID_109420903" CREATED="1453943656561" MODIFIED="1453943672629"/>
</node>
<node TEXT="%" ID="ID_442799907" CREATED="1453943473202" MODIFIED="1453943475981">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x5339;&#x914d;&#x7684;&#x62ec;&#x53f7;&#x6240;&#x5728;&#x4e4b;&#x5904;" ID="ID_645623785" CREATED="1453943674888" MODIFIED="1453943687855"/>
</node>
<node TEXT="(/)" ID="ID_7763291" CREATED="1453943476800" MODIFIED="1453943489101">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x4e0a;&#x4e00;&#x53e5;/&#x4e0b;&#x4e00;&#x53e5;&#x7684;&#x5f00;&#x5934;" ID="ID_1081052113" CREATED="1453943689618" MODIFIED="1453943702645"/>
</node>
<node TEXT="{/}" ID="ID_144350156" CREATED="1453943489960" MODIFIED="1453943497667">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x4e0a;&#x4e00;&#x6bb5;/&#x4e0b;&#x4e00;&#x6bb5;&#x7684;&#x5f00;&#x5934;" ID="ID_627990953" CREATED="1453943705050" MODIFIED="1453943716328"/>
</node>
<node TEXT="H/M/L" ID="ID_218610637" CREATED="1453943512251" MODIFIED="1453943516407">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x5c4f;&#x5e55;&#x6700;&#x4e0a;&#x65b9;/&#x6b63;&#x4e2d;&#x95f4;/&#x6700;&#x4e0b;&#x65b9;" ID="ID_426731246" CREATED="1453943718266" MODIFIED="1453943738889"/>
</node>
<node TEXT="gf" ID="ID_1805953113" CREATED="1453943519942" MODIFIED="1453943526748">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x5149;&#x6807;&#x4e0b;&#x7684;&#x6587;&#x4ef6;&#x540d;" ID="ID_1765519143" CREATED="1453943528546" MODIFIED="1453943538000"/>
</node>
<node TEXT="&lt;C-]&gt;" ID="ID_1108237343" CREATED="1453943542939" MODIFIED="1453943552721">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x5149;&#x6807;&#x4e0b;&#x5173;&#x952e;&#x5b57;&#x7684;&#x5b9a;&#x4e49;&#x4e4b;&#x5904;" ID="ID_284834995" CREATED="1453943554707" MODIFIED="1453943574829"/>
</node>
<node TEXT="`{mark}/&apos;{mark}" ID="ID_1312396945" CREATED="1453943440197" MODIFIED="1453943630574">
<node TEXT="&#x8df3;&#x8f6c;&#x5230;&#x4e00;&#x4e2a;&#x4f4d;&#x7f6e;&#x6807;&#x8bb0;" ID="ID_1204078927" CREATED="1453943637559" MODIFIED="1453943642369"/>
</node>
</node>
</node>
<node TEXT="no.56&#x904d;&#x5386;&#x6539;&#x53d8;&#x5217;&#x8868;" ID="ID_1998162222" CREATED="1453702864046" MODIFIED="1453702864046">
<node TEXT="u &lt;C-r&gt;" ID="ID_663514969" CREATED="1453943895695" MODIFIED="1453943900619"/>
<node TEXT=":changes" ID="ID_1387727755" CREATED="1453943851350" MODIFIED="1453943860886"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h changes
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x6807;&#x8bc6;&#x4e0a;&#x6b21;&#x4fee;&#x6539;&#x65b9;&#x4f4d;&#x7684;&#x4f4d;&#x7f6e;&#x6807;&#x8bb0;" ID="ID_659048802" CREATED="1453944011707" MODIFIED="1453944031758">
<node TEXT="`." ID="ID_1679724900" CREATED="1453944049637" MODIFIED="1453944132770"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h `.
    </p>
  </body>
</html>
</richcontent>
<font NAME="DejaVu Sans Mono"/>
</node>
<node TEXT="`^" ID="ID_1145681207" CREATED="1453944054237" MODIFIED="1453944132773"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h `^
    </p>
  </body>
</html>
</richcontent>
<font NAME="DejaVu Sans Mono"/>
</node>
<node TEXT="gi" ID="ID_881602667" CREATED="1453944078357" MODIFIED="1453944132773"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h gi
    </p>
  </body>
</html>
</richcontent>
<font NAME="DejaVu Sans Mono"/>
</node>
</node>
</node>
<node TEXT="no.57&#x8df3;&#x8f6c;&#x5230;&#x5149;&#x6807;&#x4e0b;&#x7684;&#x6587;&#x4ef6;" ID="ID_1615905599" CREATED="1453702864050" MODIFIED="1453960376846"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      jumps/practical_vim.rb
    </p>
  </body>
</html>

</richcontent>
<node TEXT="gf &lt;C-o&gt;" ID="ID_255357444" CREATED="1453960359056" MODIFIED="1453960452094"/>
<node TEXT=":set suffixesadd+=.rb" ID="ID_672252103" CREATED="1453960416739" MODIFIED="1453960430131"/>
<node TEXT=":set path=.,/usr/include,," ID="ID_483499916" CREATED="1453960466884" MODIFIED="1453960479734"/>
<node TEXT="plugin: tim pope:bundler.vim" ID="ID_388591703" CREATED="1453960504762" MODIFIED="1453960517233"/>
<node TEXT="&lt;C-]&gt; no.103" ID="ID_562632700" CREATED="1453960533653" MODIFIED="1453960541324"/>
</node>
<node TEXT="no.58&#x7528;&#x5168;&#x5c40;&#x4f4d;&#x7f6e;&#x6807;&#x8bb0;&#x5728;&#x6587;&#x4ef6;&#x95f4;&#x5feb;&#x901f;&#x8df3;&#x8f6c;" ID="ID_1126901316" CREATED="1453702864051" MODIFIED="1453702864051">
<node TEXT="quickfix&#x5217;&#x8868;" ID="ID_1365244993" CREATED="1453960645891" MODIFIED="1453960651759">
<node TEXT=":make" ID="ID_992444724" CREATED="1453960653522" MODIFIED="1453960668152"/>
<node TEXT=":vimgrep" ID="ID_1545639237" CREATED="1453960657679" MODIFIED="1453960662207"/>
<node TEXT=":grep" ID="ID_1120261753" CREATED="1453960662999" MODIFIED="1453960665981"/>
</node>
<node TEXT="m{letter}" ID="ID_1578531148" CREATED="1453960698610" MODIFIED="1453960728695">
<node TEXT="&#x5c0f;&#x5199;&#x5b57;&#x6bcd; &#x5c40;&#x90e8;&#x6807;&#x8bb0;" ID="ID_1086818223" CREATED="1453960702018" MODIFIED="1453960713745"/>
<node TEXT="&#x5927;&#x5199;&#x5b57;&#x6bcd; &#x5168;&#x5c40;&#x6807;&#x8bb0;" ID="ID_963844554" CREATED="1453960714814" MODIFIED="1453960722693"/>
</node>
</node>
</node>
</node>
</map>

<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Vim&#x7f16;&#x8f91;&#x5668;&#x8fdb;&#x9636;" ID="ID_1096445921" CREATED="1449803376340" MODIFIED="1449805951934" LINK="RH134.mm">
<edge STYLE="bezier" COLOR="#ff00ff" WIDTH="thin"/>
<hook NAME="MapStyle" zoom="0.912">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="12"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="INSERT" POSITION="right" ID="ID_1516058896" CREATED="1448612413991" MODIFIED="1448612522318">
<edge COLOR="#00007c"/>
<node TEXT="Ctrl+o" ID="ID_1292411792" CREATED="1449812043315" MODIFIED="1449812043315">
<node TEXT="&#x63d2;&#x5165;&#x6a21;&#x5f0f;&#x4e0b;&#x8fdb;&#x5165;&#x547d;&#x4ee4;&#x6a21;&#x5f0f;, &#x4e4b;&#x540e;&#x8fd4;&#x56de;&#x63d2;&#x5165;&#x6a21;&#x5f0f;" ID="ID_6976350" CREATED="1449812051601" MODIFIED="1449812051601"/>
</node>
<node TEXT="Ctrl+p" ID="ID_956154853" CREATED="1449812053029" MODIFIED="1449812056116">
<node TEXT="&#x8fdb;&#x884c;&#x5168;&#x6587;&#x7d22;&#x5f15;&#x8865;&#x5168;" ID="ID_581526344" CREATED="1449812057854" MODIFIED="1449812072357"/>
</node>
</node>
<node TEXT="NORMAL" POSITION="right" ID="ID_1846572897" CREATED="1448612416270" MODIFIED="1448612522318">
<edge COLOR="#007c00"/>
<node ID="ID_298220675" CREATED="1449812189716" MODIFIED="1449812209379"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <u>N</u>j[khl]
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x79fb;&#x52a8;N&#x884c;&#x6216;&#x5b57;&#x7b26;" ID="ID_1118564459" CREATED="1449812211992" MODIFIED="1449812226977"/>
</node>
<node TEXT="(" ID="ID_1213671722" CREATED="1450425618688" MODIFIED="1450425621025">
<node TEXT="&#x5149;&#x6807;&#x79fb;&#x52a8;&#x5230;&#x5f53;&#x524d;&#x6216;&#x4e0a;&#x4e00;&#x4e2a;&#x53e5;&#x5b50;&#x7684;&#x5f00;&#x5934;" ID="ID_1378677510" CREATED="1450425733959" MODIFIED="1450425770263"/>
</node>
<node TEXT=")" ID="ID_935850998" CREATED="1450425716721" MODIFIED="1450425718963">
<node TEXT="&#x5149;&#x6807;&#x79fb;&#x52a8;&#x5230;&#x4e0b;&#x4e00;&#x4e2a;&#x53e5;&#x5b50;&#x7684;&#x5f00;&#x5934;" ID="ID_906649830" CREATED="1450425733959" MODIFIED="1450425766310"/>
</node>
<node TEXT="{" ID="ID_911023960" CREATED="1450425625871" MODIFIED="1450425628060">
<node TEXT="&#x5149;&#x6807;&#x79fb;&#x52a8;&#x5230;&#x5f53;&#x524d;&#x6216;&#x4e0a;&#x4e00;&#x4e2a;&#x6bb5;&#x843d;&#x7684;&#x5f00;&#x5934;" ID="ID_1081209557" CREATED="1450425733959" MODIFIED="1450425782021"/>
</node>
<node TEXT="}" ID="ID_1272060743" CREATED="1450425720518" MODIFIED="1450425722417">
<node TEXT="&#x5149;&#x6807;&#x79fb;&#x52a8;&#x5230;&#x4e0b;&#x4e00;&#x4e2a;&#x6bb5;&#x843d;&#x7684;&#x5f00;&#x5934;" ID="ID_655958509" CREATED="1450425733959" MODIFIED="1450425793370"/>
</node>
<node TEXT="c$, C" ID="ID_1154450031" CREATED="1450425604267" MODIFIED="1450425687112">
<node TEXT="&#x5220;&#x9664;&#x5149;&#x6807;&#x81f3;&#x7ed3;&#x5c3e;&#x5b57;&#x7b26;,&#x5e76;&#x8fdb;&#x5165;&#x63d2;&#x5165;&#x6a21;&#x5f0f;" ID="ID_748651898" CREATED="1450425648550" MODIFIED="1450425678931"/>
</node>
<node TEXT="caw[Wp]" ID="ID_40143780" CREATED="1449812233711" MODIFIED="1450425633044">
<node TEXT="&#x5220;&#x9664;&#x4e00;&#x4e2a;&#x5355;&#x8bcd;&#x6216;&#x4e00;&#x4e2a;&#x6bb5;&#x843d;&#x5e76;&#x8fdb;&#x5165;&#x63d2;&#x5165;&#x6a21;&#x5f0f;" ID="ID_1614612813" CREATED="1449812386773" MODIFIED="1449812402890"/>
</node>
<node TEXT="daw[Wp]" ID="ID_1008376404" CREATED="1449812272148" MODIFIED="1450425637527">
<node TEXT="&#x5220;&#x9664;&#x4e00;&#x4e2a;&#x5355;&#x8bcd;&#x6216;&#x4e00;&#x4e2a;&#x6bb5;&#x843d;" ID="ID_1850767720" CREATED="1449812335858" MODIFIED="1449812383562"/>
</node>
<node TEXT="ci&quot;[([{t]" ID="ID_1217548835" CREATED="1449812298276" MODIFIED="1449812325626">
<node TEXT="&#x5220;&#x9664;&#x5982;&quot;&quot;(){}[]&#x4e4b;&#x95f4;&#x7684;&#x5b57;&#x7b26;&#x5e76;&#x8fdb;&#x5165;&#x63d2;&#x5165;&#x6a21;&#x5f0f;" ID="ID_1490034813" CREATED="1449812335858" MODIFIED="1449812471273">
<node TEXT="&quot; Motion for &quot;next object&quot;. For example, &quot;din(&quot; would go to the next &quot;()&quot; pair&#xa;&quot; and delete its contents.&#xa;onoremap an :&lt;c-u&gt;call &lt;SID&gt;NextTextObject(&apos;a&apos;)&lt;cr&gt;&#xa;xnoremap an :&lt;c-u&gt;call &lt;SID&gt;NextTextObject(&apos;a&apos;)&lt;cr&gt;&#xa;onoremap in :&lt;c-u&gt;call &lt;SID&gt;NextTextObject(&apos;i&apos;)&lt;cr&gt;&#xa;xnoremap in :&lt;c-u&gt;call &lt;SID&gt;NextTextObject(&apos;i&apos;)&lt;cr&gt;&#xa;function! s:NextTextObject(motion)&#xa;echo&#xa;let c = nr2char(getchar())&#xa;exe &quot;normal! f&quot;.c.&quot;v&quot;.a:motion.c&#xa;endfunction" ID="ID_544856016" CREATED="1450445124215" MODIFIED="1450445153267"/>
</node>
</node>
<node TEXT="&#x6587;&#x672c;&#x7f29;&#x8fdb;" ID="ID_1851062192" CREATED="1450425054184" MODIFIED="1450425058364">
<node TEXT="&gt;&gt; &lt;&lt;" ID="ID_651767530" CREATED="1449812033326" MODIFIED="1449812160990">
<node TEXT="&#x7f29;&#x8fdb;" ID="ID_1077744553" CREATED="1449812162024" MODIFIED="1449812165319"/>
</node>
<node TEXT="==" ID="ID_400269249" CREATED="1449812033326" MODIFIED="1449812152581">
<node TEXT="&#x5f53;&#x524d;&#x884c;&#x81ea;&#x52a8;&#x7f29;&#x8fdb;" ID="ID_295234228" CREATED="1449812154309" MODIFIED="1449812154309"/>
</node>
<node TEXT="gg=G" ID="ID_16342961" CREATED="1449812033334" MODIFIED="1449812099041">
<node ID="ID_355531352" CREATED="1449812099899" MODIFIED="1449812099899"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20840;&#25991;&#33258;&#21160;&#32553;&#36827;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="C-a" ID="ID_653094050" CREATED="1450425799402" MODIFIED="1450425802258">
<node TEXT="&#x589e;&#x52a0;1" ID="ID_1373669794" CREATED="1450425805300" MODIFIED="1450425824301"/>
</node>
<node TEXT="C-x" ID="ID_150204340" CREATED="1450425814099" MODIFIED="1450425816794">
<node TEXT="&#x51cf;&#x5c11;1" ID="ID_1185576360" CREATED="1450425826822" MODIFIED="1450425829869"/>
</node>
<node TEXT="." ID="ID_1974570892" CREATED="1449812476910" MODIFIED="1449812479264">
<node TEXT="&#x91cd;&#x590d;&#x6700;&#x8fd1;&#x4e00;&#x6b21;&#x7684;&#x7f16;&#x8f91;&#x64cd;&#x4f5c;" ID="ID_1282828748" CREATED="1449812480777" MODIFIED="1449812495489"/>
</node>
</node>
<node TEXT="COMMAND" POSITION="right" ID="ID_374792524" CREATED="1448612417087" MODIFIED="1448613013817">
<edge COLOR="#7c007c"/>
<node TEXT=":%s/foo/bar/gi" ID="ID_1257631979" CREATED="1449812603756" MODIFIED="1450425915079">
<node TEXT="s/foo/bar/&#x641c;&#x7d22;&#x66ff;&#x6362;&#x64cd;&#x4f5c;" ID="ID_601827003" CREATED="1449812615107" MODIFIED="1450430517292"/>
<node TEXT="%&#x4ee3;&#x8868;&#x5168;&#x6587;" ID="ID_360781823" CREATED="1450425922594" MODIFIED="1450425928371"/>
<node TEXT="g&#x4ee3;&#x8868;&#x9488;&#x5bf9;&#x6574;&#x884c;,&#x505a;&#x591a;&#x6b21;&#x66ff;&#x6362;" ID="ID_396979480" CREATED="1450425929642" MODIFIED="1450430486939"/>
<node TEXT="i&#x4ee3;&#x8868;&#x5ffd;&#x7565;&#x5927;&#x5c0f;&#x5199;" ID="ID_1940731102" CREATED="1450425938438" MODIFIED="1450425950413"/>
</node>
<node TEXT=":qa!" ID="ID_1201684824" CREATED="1449813352107" MODIFIED="1449813357854">
<node TEXT="&#x6240;&#x6709;&#x7a97;&#x53e3;&#x4e0d;&#x4fdd;&#x5b58;&#x9000;&#x51fa;" ID="ID_440320560" CREATED="1449813359245" MODIFIED="1449813368005"/>
</node>
<node TEXT=":set nu[!]" ID="ID_1284675368" CREATED="1449812561846" MODIFIED="1449812573281">
<node TEXT="&#x663e;&#x793a;&#x884c;&#x53f7;" ID="ID_1399327247" CREATED="1450426076691" MODIFIED="1450426082489"/>
</node>
<node TEXT=":set hidden" ID="ID_630376489" CREATED="1450426063948" MODIFIED="1450426067741">
<node TEXT="&#x5141;&#x8bb8;&#x4e0d;&#x4fdd;&#x5b58;&#x5207;&#x6362;buffer" ID="ID_696359846" CREATED="1450426084248" MODIFIED="1450426094964"/>
</node>
<node TEXT=":nt. n,mt. ntN n,mtN" ID="ID_1532087286" CREATED="1449812090055" MODIFIED="1450441613563">
<node TEXT="&#x5c06;&#x7279;&#x5b9a;&#x884c;&#x590d;&#x5236;&#x5230;&#x67d0;&#x884c;&#x4e0b;&#x65b9;" ID="ID_1922958316" CREATED="1449812531920" MODIFIED="1449812551871"/>
</node>
<node TEXT=":nm" ID="ID_835967982" CREATED="1451536466429" MODIFIED="1451536472360"/>
</node>
<node TEXT="&#x591a;&#x6587;&#x4ef6;&#x7f16;&#x8f91;" POSITION="right" ID="ID_787649447" CREATED="1449812967875" MODIFIED="1449812980071">
<edge COLOR="#007c7c"/>
<node TEXT=":buffers" ID="ID_314599309" CREATED="1449812994280" MODIFIED="1449813014107">
<node ID="ID_695590423" CREATED="1449813014578" MODIFIED="1449813014578"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21015;&#20986;&#24403;&#21069;&#25171;&#24320;&#30340;&#25991;&#20214;&#21015;&#34920;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT=":e file" ID="ID_354656195" CREATED="1449812994277" MODIFIED="1449813020767">
<node ID="ID_156304303" CREATED="1449813021245" MODIFIED="1449813021245"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      vim&#20013;&#25171;&#24320;file
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT=":bn" ID="ID_349165666" CREATED="1449812994277" MODIFIED="1449813024351">
<node ID="ID_208534828" CREATED="1449813024815" MODIFIED="1449813024815"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20999;&#25442;&#21040;&#19979;&#19968;&#20010;buffer
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT=":bp" ID="ID_1511250685" CREATED="1449812994278" MODIFIED="1449813027736">
<node ID="ID_1717986350" CREATED="1449813028296" MODIFIED="1449813028296"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20999;&#25442;&#21040;&#19978;&#19968;&#20010;buffer
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1221032965" CREATED="1449812994281" MODIFIED="1449813038316"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :b<u>N</u>
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_1091499660" CREATED="1449813038819" MODIFIED="1449813038819"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20999;&#25442;&#21040;&#25351;&#23450;&#30340;buffer
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="&#x6807;&#x8bb0;&#x64cd;&#x4f5c;" POSITION="right" ID="ID_254892389" CREATED="1449812778343" MODIFIED="1449812789809">
<edge COLOR="#007c00"/>
<node TEXT=":marks" ID="ID_652569113" CREATED="1449812798913" MODIFIED="1449812806488">
<node TEXT="&#x770b;&#x6807;&#x8bb0;(&#x4f4d;&#x7f6e;)&#x5217;&#x8868;" ID="ID_677984376" CREATED="1449812807736" MODIFIED="1449812839745"/>
</node>
<node TEXT="ma" ID="ID_1432427062" CREATED="1449812798913" MODIFIED="1449812811229">
<node TEXT="&#x5c06;&#x5f53;&#x524d;&#x4f4d;&#x7f6e;&#x653e;&#x5230;&#x6807;&#x8bb0;a&#x4e2d;" ID="ID_729211259" CREATED="1449812812204" MODIFIED="1449812837693"/>
</node>
<node TEXT=" &apos;a" ID="ID_1548002273" CREATED="1449812798916" MODIFIED="1449812829350">
<node TEXT="&#x8fd4;&#x56de;&#x6807;&#x8bb0;a&#x4f4d;&#x7f6e;" ID="ID_1419082525" CREATED="1449812816427" MODIFIED="1449812835170"/>
</node>
</node>
<node TEXT="&#x5bc4;&#x5b58;&#x5668;&#x64cd;&#x4f5c;" POSITION="right" ID="ID_293579866" CREATED="1449812648849" MODIFIED="1449812654535">
<edge COLOR="#00007c"/>
<node TEXT=":reg" ID="ID_1703662712" CREATED="1449812668498" MODIFIED="1449812676160">
<node TEXT="&#x770b;&#x5bc4;&#x5b58;&#x5668;(&#x526a;&#x8d34;&#x677f;)&#x5217;&#x8868;" ID="ID_930611121" CREATED="1449812676778" MODIFIED="1449812680757"/>
</node>
<node TEXT="&quot;1p" ID="ID_536257085" CREATED="1449812668498" MODIFIED="1449812696407">
<node TEXT="&#x8d34;&#x51fa;&#x5bc4;&#x5b58;&#x5668;1&#x7684;&#x5185;&#x5bb9;" ID="ID_1773672576" CREATED="1449812700011" MODIFIED="1449812709650"/>
</node>
<node TEXT="&quot;ayy" ID="ID_253480895" CREATED="1449812668502" MODIFIED="1449812716121">
<node TEXT="&#x590d;&#x5236;&#x5f53;&#x524d;&#x884c;&#x5230;&#x5bc4;&#x5b58;&#x5668;a" ID="ID_1330497102" CREATED="1449812714889" MODIFIED="1449812718331"/>
</node>
<node TEXT="&quot;ap" ID="ID_1448550870" CREATED="1449812668503" MODIFIED="1449812726132">
<node TEXT="&#x5c06;&#x5bc4;&#x5b58;&#x5668;a&#x5185;&#x5bb9;&#x7c98;&#x5e16;&#x5230;&#x5f53;&#x524d;&#x884c;&#x4e0b;&#x65b9;" ID="ID_420923956" CREATED="1449812726799" MODIFIED="1449812744105"/>
</node>
<node TEXT="n&quot;ap" ID="ID_1651583904" CREATED="1449812668504" MODIFIED="1449812668504">
<node TEXT="&#x6267;&#x884c;n&#x6b21;&#x5c06;&#x5bc4;&#x5b58;&#x5668;a&#x5185;&#x5bb9;&#x7c98;&#x5e16;&#x5230;&#x5f53;&#x524d;&#x884c;&#x4e0b;&#x65b9;" ID="ID_1934346465" CREATED="1449812737451" MODIFIED="1449812758777"/>
</node>
<node TEXT="0&#x53f7;&#x5bc4;&#x5b58;&#x5668;&#x5b58;&#x653e;&#x6700;&#x8fd1;&#x4e00;&#x6b21;&#x590d;&#x5236;&#x5185;&#x5bb9;(y)" ID="ID_632234712" CREATED="1449812668505" MODIFIED="1449812668505"/>
<node TEXT="&quot;&#x53f7;&#x5bc4;&#x5b58;&#x5668;&#x5b58;&#x653e;&#x6700;&#x8fd1;&#x4e00;&#x6b21;&#x5220;&#x9664;&#x5185;&#x5bb9;(d, c)" ID="ID_487240266" CREATED="1449812668505" MODIFIED="1449812668505"/>
<node TEXT="Ctrl+r" ID="ID_1149756050" CREATED="1449813683903" MODIFIED="1449813689608">
<node TEXT="&#x63d2;&#x5165;&#x6a21;&#x5f0f;&#x4e0b;&#x8fdb;&#x5165;&#x7c98;&#x8d34;&#x6a21;&#x5f0f;, &#x53ef;&#x7c98;&#x8d34;&#x6307;&#x5b9a;&#x5bc4;&#x5b58;&#x5668;&#x4e2d;&#x5185;&#x5bb9;" ID="ID_1645362811" CREATED="1449813702554" MODIFIED="1449813702554"/>
</node>
</node>
<node TEXT="&#x5b8f;&#x64cd;&#x4f5c;" POSITION="right" ID="ID_376603077" CREATED="1449812860745" MODIFIED="1449812866564">
<edge COLOR="#7c007c"/>
<node TEXT="qa5jq" ID="ID_1616664358" CREATED="1449812879575" MODIFIED="1449812883770">
<node ID="ID_325725520" CREATED="1449812884414" MODIFIED="1449812884414"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#24405;&#21046;5j&#30340;&#25805;&#20316;&#21040;&#23439;a&#20013;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="@a" ID="ID_682990115" CREATED="1449812879575" MODIFIED="1449812887615">
<node ID="ID_1213764444" CREATED="1449812888183" MODIFIED="1449812888183"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#25191;&#34892;&#23439;a&#25805;&#20316;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="n@a" ID="ID_933519269" CREATED="1449812879586" MODIFIED="1449812935162" LINK="mailto:n@a">
<node ID="ID_596927096" CREATED="1449812935774" MODIFIED="1449812935774"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#25191;&#34892;n&#27425;&#23439;a
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="@@" ID="ID_1644722663" CREATED="1449812879586" MODIFIED="1449812940128">
<node TEXT="&#x9488;&#x5bf9;&#x5f53;&#x524d;&#x4f4d;&#x7f6e;&#x5feb;&#x901f;&#x6267;&#x884c;&#x4e0a;&#x4e00;&#x6b21;&#x8fd0;&#x884c;&#x8fc7;&#x7684;&#x5b8f;" ID="ID_564608291" CREATED="1449812940593" MODIFIED="1449812953772"/>
</node>
</node>
<node TEXT="&#x7a97;&#x53e3;&#x64cd;&#x4f5c;" POSITION="right" ID="ID_925642231" CREATED="1449813069774" MODIFIED="1449813080070">
<edge COLOR="#7c7c00"/>
<node TEXT="Ctrl + ws&#x6216;:split file" ID="ID_1925091530" CREATED="1449813097157" MODIFIED="1449813415322">
<node ID="ID_1741445339" CREATED="1449813101829" MODIFIED="1449813101829"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#27178;&#21521;&#20999;&#20998;&#19968;&#20010;&#31383;&#21475;&#24182;&#25171;&#24320;file
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ctrl + wv&#x6216;:vsplit file" ID="ID_1583185878" CREATED="1449813097157" MODIFIED="1449813412147">
<node ID="ID_1175194355" CREATED="1449813105652" MODIFIED="1449813105652"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#32437;&#21521;&#20999;&#20998;&#19968;&#20010;&#31383;&#21475;&#24182;&#25171;&#24320;file
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ctrl + ww" ID="ID_1997463086" CREATED="1449813097162" MODIFIED="1449813118337">
<node ID="ID_1607442633" CREATED="1449813110840" MODIFIED="1449813110840"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#36825;&#20010;&#21629;&#20196;&#20250;&#22312;&#25152;&#26377;&#31383;&#21475;&#20013;&#24490;&#29615;&#31227;&#21160;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ctrl + w h[jkl]" ID="ID_205123084" CREATED="1449813097163" MODIFIED="1449813221486">
<node ID="ID_139550946" CREATED="1449813125710" MODIFIED="1449813125710"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21521;&#24038;&#19979;&#19978;&#21491;&#31227;&#21160;&#31383;&#21475;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ctrl + w +-_=" ID="ID_1082969819" CREATED="1449813603511" MODIFIED="1450443160191">
<node TEXT="&#x8c03;&#x6574;&#x5f53;&#x524d;&#x7a97;&#x53e3;&#x884c;&#x6570;" ID="ID_1237830108" CREATED="1449813617566" MODIFIED="1449813653542"/>
</node>
<node TEXT="Ctrl + wq" ID="ID_421599024" CREATED="1449813097165" MODIFIED="1449813135739">
<node ID="ID_332990022" CREATED="1449813129820" MODIFIED="1449813129820"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20851;&#38381;&#24403;&#21069;&#31383;&#21475;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ctrl + s" ID="ID_179828363" CREATED="1449813497280" MODIFIED="1449813503329">
<node TEXT="&#x9501;&#x5b9a;&#x7a97;&#x53e3;" ID="ID_220749431" CREATED="1449813512787" MODIFIED="1449813523201"/>
</node>
<node TEXT="Ctrl + q" ID="ID_1060775828" CREATED="1449813505868" MODIFIED="1449813510449">
<node TEXT="&#x9000;&#x51fa;&#x9501;&#x5b9a;" ID="ID_144147114" CREATED="1449813525556" MODIFIED="1449813532646"/>
</node>
</node>
<node TEXT="VISUAL" POSITION="right" ID="ID_1283083012" CREATED="1448612482250" MODIFIED="1448612522320">
<edge COLOR="#007c7c"/>
<node TEXT="&#x9009;&#x62e9;&#x6a21;&#x5f0f;&#x4e0b;, &#x53ef;&#x4ee5;&#x8fd0;&#x884c;:s&#x5bf9;&#x6587;&#x672c;&#x5757;&#x8fdb;&#x884c;&#x66ff;&#x6362;&#x64cd;&#x4f5c;" ID="ID_1190345678" CREATED="1449813746672" MODIFIED="1449813809941"/>
</node>
<node TEXT="INSERT" POSITION="left" ID="ID_4512256" CREATED="1448612413991" MODIFIED="1448612522318">
<edge COLOR="#00007c"/>
<node TEXT="i,I" ID="ID_306685708" CREATED="1448612527289" MODIFIED="1448612538844"/>
<node TEXT="a,A" ID="ID_255648169" CREATED="1448612543449" MODIFIED="1448612545674"/>
<node TEXT="c,C" ID="ID_1759746843" CREATED="1448612546074" MODIFIED="1448612578861"/>
<node TEXT="s,S" ID="ID_1307972024" CREATED="1448612551860" MODIFIED="1448612590980"/>
</node>
<node TEXT="NORMAL" POSITION="left" ID="ID_742248015" CREATED="1448612416270" MODIFIED="1448612522318">
<edge COLOR="#007c00"/>
<node TEXT="Movement" ID="ID_101247337" CREATED="1448612928236" MODIFIED="1448612934116">
<node TEXT="j,C-d,C-f" ID="ID_200452874" CREATED="1448612655314" MODIFIED="1448613405485">
<icon BUILTIN="down"/>
</node>
<node TEXT="k,C-u,C-b" ID="ID_1149792887" CREATED="1448612713415" MODIFIED="1448613412652">
<icon BUILTIN="up"/>
</node>
<node TEXT="l,w,W,e,E,$" ID="ID_237941417" CREATED="1448612662758" MODIFIED="1448613138958">
<icon BUILTIN="forward"/>
</node>
<node TEXT="h,b,B,0" ID="ID_527929816" CREATED="1448612674157" MODIFIED="1448613142794">
<icon BUILTIN="back"/>
</node>
<node TEXT="gg" ID="ID_1101053887" CREATED="1448612743634" MODIFIED="1448612746580"/>
<node TEXT="G" ID="ID_503835212" CREATED="1448612748294" MODIFIED="1448612748827"/>
<node TEXT="zz" ID="ID_1850103109" CREATED="1448612738922" MODIFIED="1448612740179"/>
<node TEXT="ZZ" ID="ID_1475269365" CREATED="1448612877536" MODIFIED="1448612880837"/>
</node>
<node TEXT="find" ID="ID_251140336" CREATED="1448613203813" MODIFIED="1448613206221">
<node TEXT="f, &quot;; ,&quot;" ID="ID_1545352913" CREATED="1448613208191" MODIFIED="1448613259767"/>
<node TEXT="/" ID="ID_974260966" CREATED="1448613210622" MODIFIED="1448613211721"/>
<node TEXT="?" ID="ID_149746379" CREATED="1448613212862" MODIFIED="1448613214354"/>
</node>
<node TEXT="Delete" ID="ID_14513212" CREATED="1448612942191" MODIFIED="1448612944951">
<node TEXT="x" ID="ID_1592142381" CREATED="1448612946220" MODIFIED="1448612948434"/>
<node TEXT="dd" ID="ID_448963252" CREATED="1448612948771" MODIFIED="1448612951270"/>
</node>
<node TEXT="Copy" ID="ID_1152211158" CREATED="1448612958257" MODIFIED="1448612960604">
<node TEXT="yy" ID="ID_1948397377" CREATED="1448612961801" MODIFIED="1448612963089"/>
<node TEXT="p,P" ID="ID_121885489" CREATED="1448612963322" MODIFIED="1448612988137"/>
</node>
<node TEXT="Modify" ID="ID_1350719771" CREATED="1448613085181" MODIFIED="1448613087639">
<node TEXT="J" ID="ID_1889799583" CREATED="1448613089394" MODIFIED="1448613093255"/>
</node>
</node>
<node TEXT="COMMAND" POSITION="left" ID="ID_137615238" CREATED="1448612417087" MODIFIED="1448613013817">
<edge COLOR="#7c007c"/>
<node TEXT=":" ID="ID_662435752" CREATED="1448612763505" MODIFIED="1448612766857">
<node ID="ID_1619220985" CREATED="1448612817653" MODIFIED="1448613511736"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      w <u>FILENAME</u>
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_328316246" CREATED="1448613562315" MODIFIED="1448613572367"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      r <u>FILENAME</u>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="q" ID="ID_840697460" CREATED="1448612819757" MODIFIED="1448612821176"/>
<node TEXT="x" ID="ID_1795215625" CREATED="1448612838619" MODIFIED="1448612848774"/>
<node TEXT="!" ID="ID_195682262" CREATED="1448613018900" MODIFIED="1448613020141"/>
</node>
<node TEXT="q" ID="ID_568587848" CREATED="1448612784006" MODIFIED="1448612786070">
<node TEXT=":" ID="ID_1135218257" CREATED="1448612790596" MODIFIED="1448612792914"/>
<node TEXT="/" ID="ID_1642806071" CREATED="1448612794086" MODIFIED="1448612795348"/>
<node TEXT="q" ID="ID_270739142" CREATED="1448612812777" MODIFIED="1448612814421"/>
</node>
</node>
<node TEXT="VISUAL" POSITION="left" ID="ID_653942648" CREATED="1448612482250" MODIFIED="1448612522320">
<edge COLOR="#007c7c"/>
<node TEXT="v" ID="ID_1750288815" CREATED="1448613027691" MODIFIED="1448613033047"/>
<node TEXT="V" ID="ID_1959409166" CREATED="1448613033765" MODIFIED="1448613034592"/>
<node TEXT="C-v" ID="ID_157852538" CREATED="1448613034920" MODIFIED="1448613185959"/>
</node>
<node TEXT="&#x4f8b;" POSITION="right" ID="ID_1545330741" CREATED="1449806206616" MODIFIED="1449813929466">
<edge COLOR="#7c0000"/>
<node TEXT="&#x7f16;&#x8f91;&#x4e8c;&#x8fdb;&#x5236;&#x6587;&#x4ef6;" ID="ID_1728424411" CREATED="1449806219742" MODIFIED="1449806235578">
<node TEXT="vim -b /var/log/wtmp" ID="ID_1243921188" CREATED="1449806271199" MODIFIED="1449806271199"/>
<node TEXT=":%!xxd" ID="ID_697159571" CREATED="1449806282899" MODIFIED="1449806282899"/>
</node>
</node>
</node>
</map>

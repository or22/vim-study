<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="PartI-Mode" ID="ID_270924088" CREATED="1451538498124" MODIFIED="1451539057340" LINK="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;.mm">
<edge STYLE="bezier" COLOR="#007c00" WIDTH="thin"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="8"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="02-&#x666e;&#x901a;&#x6a21;&#x5f0f;" POSITION="right" ID="ID_1573774898" CREATED="1451538425208" MODIFIED="1453169153733">
<edge COLOR="#00ffff"/>
<node TEXT="K" ID="ID_876622424" CREATED="1453164941991" MODIFIED="1453164956397"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h K
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="no.7-&#x505c;&#x987f;&#x65f6;&#x8bf7;&#x79fb;&#x5f00;&#x753b;&#x7b14;" ID="ID_1174461709" CREATED="1453169126281" MODIFIED="1453169143543">
<node TEXT="Esc" ID="ID_1667795860" CREATED="1453169189814" MODIFIED="1453169192410"/>
</node>
<node TEXT="no.8-&#x628a;&#x64a4;&#x9500;&#x5355;&#x5143;&#x5207;&#x6210;&#x5757;" ID="ID_928391090" CREATED="1453169162110" MODIFIED="1453169175050">
<node TEXT="i{insert some text}esc" ID="ID_1093350720" CREATED="1453169550185" MODIFIED="1453169586370"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      u &#21487;&#25764;&#38144;&#22359;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x63a7;&#x5236;esc&#x7684;&#x4f7f;&#x7528;,&#x6765;&#x63a7;&#x5236;&#x53ef;&#x64a4;&#x9500;&#x5757;&#x7684;&#x7c92;&#x5ea6;" ID="ID_59645825" CREATED="1453169594372" MODIFIED="1453169609514"/>
<node TEXT="&#x63d2;&#x5165;&#x8fc7;&#x7a0b;&#x4e2d;&lt;Up&gt;&lt;Down&gt;&lt;Left&gt;&lt;Right&gt;&#x4f1a;&#x4ea7;&#x751f;&#x65b0;&#x7684;&#x64a4;&#x9500;&#x5757;" ID="ID_358787831" CREATED="1453169670452" MODIFIED="1453169700661"/>
</node>
<node TEXT="no.9-&#x6784;&#x9020;&#x53ef;&#x91cd;&#x590d;&#x7684;&#x4fee;&#x6539;" ID="ID_1384544589" CREATED="1453169717443" MODIFIED="1453169728113">
<node TEXT="&#x6548;&#x7387; VimGolf" ID="ID_1489714406" CREATED="1453169747658" MODIFIED="1453169791688"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      normal_mode/the_end.txt
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="daw" ID="ID_1765784230" CREATED="1453170056918" MODIFIED="1453170065045"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h aw
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.10-&#x7528;&#x6b21;&#x6570;&#x505a;&#x7b80;&#x5355;&#x7684;&#x7b97;&#x672f;&#x8fd0;&#x7b97;" ID="ID_1887312924" CREATED="1453170119411" MODIFIED="1453170150397"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      normal_mode/sprite.css
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&lt;C-a&gt;" ID="ID_1126067331" CREATED="1453170346190" MODIFIED="1453170376910"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h ctrl-a
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;C-a&gt;" ID="ID_1109565213" CREATED="1453170350585" MODIFIED="1453170381942"/>
<node TEXT="007&#xa;010" ID="ID_1249651791" CREATED="1453170427703" MODIFIED="1453170483730"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      set nrformats=
    </p>
    <p>
      &#23558;&#25152;&#26377;&#25968;&#23383;&#37117;&#24403;&#25104;10&#36827;&#21046;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.11-&#x80fd;&#x591f;&#x91cd;&#x590d;,&#x5c31;&#x522b;&#x7528;&#x6b21;&#x6570;" ID="ID_1584820051" CREATED="1453170512093" MODIFIED="1453170675429"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#32500;&#25345;&#37325;&#22797;&#30340;&#32454;&#31890;&#24230;,&#32780;&#21482;&#22312;&#24517;&#35201;&#30340;&#26102;&#20505;&#20351;&#29992;&#27425;&#25968;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Delete more than one word" ID="ID_615241428" CREATED="1453170765108" MODIFIED="1453170776159"/>
<node TEXT="I have a couple of questions -&gt; I have some more questions" ID="ID_687653840" CREATED="1453170719805" MODIFIED="1453170741075"/>
</node>
<node TEXT="no.12-&#x53cc;&#x5251;&#x5408;&#x74a7;,&#x5929;&#x4e0b;&#x65e0;&#x654c;" ID="ID_311865671" CREATED="1453170808498" MODIFIED="1453170819499">
<node TEXT="&#x64cd;&#x4f5c;&#x7b26; + &#x52a8;&#x4f5c;&#x547d;&#x4ee4; = &#x64cd;&#x4f5c;" ID="ID_1064461645" CREATED="1453170995859" MODIFIED="1453171021157"/>
<node TEXT="vim&#x7684;&#x64cd;&#x4f5c;&#x7b26;&#x547d;&#x4ee4;" ID="ID_207502876" CREATED="1453171045979" MODIFIED="1453172089979"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#24403;&#19968;&#20010;&#25805;&#20316;&#31526;&#21629;&#20196;&#34987;&#36830;&#32493;&#35843;&#29992;&#20004;&#27425;&#26102;,
    </p>
    <p>
      &#23427;&#20250;&#20316;&#29992;&#20110;&#24403;&#21069;&#34892;
    </p>
    <p>
      :h operator
    </p>
    <p>
      :h gU
    </p>
  </body>
</html>
</richcontent>
<node TEXT="c" ID="ID_743656986" CREATED="1453171145986" MODIFIED="1453171148505">
<node TEXT="&#x4fee;&#x6539;" ID="ID_533562113" CREATED="1453171322661" MODIFIED="1453171325518"/>
</node>
<node TEXT="d" ID="ID_302940115" CREATED="1453171161046" MODIFIED="1453171164130">
<node TEXT="&#x5220;&#x9664;(&#x526a;&#x5207;)" ID="ID_254176459" CREATED="1453171314164" MODIFIED="1453171321158"/>
</node>
<node TEXT="y" ID="ID_1954449199" CREATED="1453171165924" MODIFIED="1453171167473">
<node TEXT="&#x590d;&#x5236;&#x5230;&#x5bc4;&#x5b58;&#x5668;" ID="ID_1612075933" CREATED="1453171306582" MODIFIED="1453171312673"/>
</node>
<node TEXT="g~" ID="ID_1903439346" CREATED="1453171168111" MODIFIED="1453171172285">
<node TEXT="&#x53cd;&#x8f6c;&#x5927;&#x5c0f;&#x5199;" ID="ID_1033711034" CREATED="1453171298338" MODIFIED="1453171304908"/>
</node>
<node TEXT="gu" ID="ID_36502298" CREATED="1453171173184" MODIFIED="1453171174985">
<node TEXT="&#x8f6c;&#x6362;&#x4e3a;&#x5c0f;&#x5199;" ID="ID_960672591" CREATED="1453171287961" MODIFIED="1453171296995"/>
</node>
<node TEXT="gU" ID="ID_1167750537" CREATED="1453171176134" MODIFIED="1453171177888">
<node TEXT="&#x8f6c;&#x6362;&#x4e3a;&#x5927;&#x5199;" ID="ID_237907823" CREATED="1453171282005" MODIFIED="1453171286494"/>
</node>
<node TEXT="&gt;" ID="ID_206186597" CREATED="1453171179368" MODIFIED="1453171181047">
<node TEXT="&#x589e;&#x52a0;&#x7f29;&#x8fdb;" ID="ID_1129248972" CREATED="1453171272837" MODIFIED="1453171279153"/>
</node>
<node TEXT="&lt;" ID="ID_945486735" CREATED="1453171181824" MODIFIED="1453171184116">
<node TEXT="&#x51cf;&#x5c0f;&#x7f29;&#x8fdb;" ID="ID_1887185966" CREATED="1453171239925" MODIFIED="1453171269449"/>
</node>
<node TEXT="=" ID="ID_1993058023" CREATED="1453171184557" MODIFIED="1453171186336">
<node TEXT="&#x81ea;&#x52a8;&#x7f29;&#x8fdb;" ID="ID_1687955206" CREATED="1453171226575" MODIFIED="1453171237113"/>
</node>
<node TEXT="!" ID="ID_1541314405" CREATED="1453171186936" MODIFIED="1453171191289">
<node TEXT="&#x4f7f;&#x7528;&#x5916;&#x90e8;&#x7a0b;&#x5e8f;&#x8fc7;&#x6ee4;{motion}&#x6240;&#x8de8;&#x8d8a;&#x7684;&#x884c;" ID="ID_318727084" CREATED="1453171206128" MODIFIED="1453171223872"/>
</node>
</node>
<node TEXT="&#x81ea;&#x5b9a;&#x4e49;&#x64cd;&#x4f5c;&#x7b26;&#x4e0e;&#x5df2;&#x6709;&#x52a8;&#x4f5c;&#x547d;&#x4ee4;&#x534f;&#x540c;&#x5de5;&#x4f5c;" ID="ID_1349041996" CREATED="1453171349824" MODIFIED="1453171941511"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      https://github.com/tpope/vim-commentary
    </p>
    <p>
      :h map-operator
    </p>
  </body>
</html>
</richcontent>
<node TEXT="\\{motion}" ID="ID_1784190838" CREATED="1453171397716" MODIFIED="1453171406101"/>
<node TEXT="\\ap" ID="ID_1306414313" CREATED="1453171408030" MODIFIED="1453171411297"/>
<node TEXT="\\G" ID="ID_1721990399" CREATED="1453171504765" MODIFIED="1453171507762"/>
<node TEXT="\\\" ID="ID_253433947" CREATED="1453171417578" MODIFIED="1453171496563"/>
</node>
<node TEXT="&#x81ea;&#x5b9a;&#x4e49;&#x52a8;&#x4f5c;&#x547d;&#x4ee4;&#x4e0e;&#x5df2;&#x6709;&#x64cd;&#x4f5c;&#x7b26;&#x534f;&#x540c;&#x5de5;&#x4f5c;" ID="ID_1589394471" CREATED="1453172116994" MODIFIED="1453172272437"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      https://github.com/kana/vim-textobj-entire
    </p>
    <p>
      :h omap-info
    </p>
  </body>
</html>
</richcontent>
<node TEXT="ie" ID="ID_1567499504" CREATED="1453172208871" MODIFIED="1453172234406"/>
<node TEXT="ae \\ae =ae" ID="ID_1165056303" CREATED="1453172235780" MODIFIED="1453172250734"/>
</node>
<node TEXT="&#x7ed3;&#x8bc6;&#x64cd;&#x4f5c;&#x7b26;&#x5f85;&#x51b3;&#x6a21;&#x5f0f;" ID="ID_1033514682" CREATED="1453172320497" MODIFIED="1453172507312"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Operator-Pending Mode
    </p>
    <p>
      &#21482;&#26377;&#25805;&#20316;&#31526;&#25165;&#20250;&#28608;&#27963;&#24453;&#20915;&#27169;&#24335;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="d[ai]w" ID="ID_284929444" CREATED="1453172405050" MODIFIED="1453172416212"/>
<node TEXT="namespace&#xa;:h g&#xa;:h z&#xa;:h ctrl-w&#xa;:h [" ID="ID_1068073487" CREATED="1453172458869" MODIFIED="1453172569216"/>
</node>
</node>
</node>
<node TEXT="03-&#x63d2;&#x5165;&#x6a21;&#x5f0f;" POSITION="right" ID="ID_1942766257" CREATED="1451538435601" MODIFIED="1453169153743">
<edge COLOR="#ffff00"/>
<node TEXT="no.13&#x5728;&#x63d2;&#x5165;&#x6a21;&#x5f0f;&#x4e2d;&#x5373;&#x65f6;&#x66f4;&#x6b63;" ID="ID_22847547" CREATED="1453251489891" MODIFIED="1453251597893"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      emacs
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;C-h&gt;" ID="ID_707900494" CREATED="1453251564515" MODIFIED="1453251571509"/>
<node TEXT="&lt;C-w&gt;" ID="ID_217980233" CREATED="1453251572622" MODIFIED="1453251576897"/>
<node TEXT="&lt;C-u&gt;" ID="ID_872501030" CREATED="1453251577525" MODIFIED="1453251584013"/>
</node>
<node TEXT="no.14&#x8fd4;&#x56de;&#x666e;&#x901a;&#x6a21;&#x5f0f;" ID="ID_844663997" CREATED="1453251489891" MODIFIED="1453251710782">
<node TEXT="&lt;Esc&gt;" ID="ID_349413856" CREATED="1453251701561" MODIFIED="1453251794257"/>
<node TEXT="&lt;C-[&gt;" ID="ID_1161888283" CREATED="1453251786070" MODIFIED="1453251872340"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h i_CTRL-[
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&lt;C-o&gt;" ID="ID_31841882" CREATED="1453251802571" MODIFIED="1453252296827"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h i_CTRL-O
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x5207;&#x6362;&#x5230;&quot;&#x63d2;&#x5165;-&#x666e;&#x901a;&quot;&#x6a21;&#x5f0f;" ID="ID_1427662847" CREATED="1453251830115" MODIFIED="1453251846265"/>
</node>
</node>
<node TEXT="no.15&#x4e0d;&#x79bb;&#x5f00;&#x63d2;&#x5165;&#x6a21;&#x5f0f;,&#x7c98;&#x8d34;&#x5bc4;&#x5b58;&#x5668;&#x4e2d;&#x7684;&#x6587;&#x672c;" ID="ID_849424668" CREATED="1453251489891" MODIFIED="1453252129183"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      insert_mode/practical-vim.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;C-r&gt;" ID="ID_443062810" CREATED="1453252083209" MODIFIED="1453252269648"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h i_CTRL-R
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&lt;C-r&gt;&lt;C-p&gt;{register}" ID="ID_1945698253" CREATED="1453252368884" MODIFIED="1453252384200"/>
<node TEXT="no.62" ID="ID_567203166" CREATED="1453253331909" MODIFIED="1453253334391"/>
</node>
<node TEXT="no.16&#x968f;&#x65f6;&#x968f;&#x5730;&#x505a;&#x8fd0;&#x7b97;" ID="ID_718575976" CREATED="1453251489891" MODIFIED="1453253204919"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      insert_mode/back-of-envelope.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;C-r&gt;=" ID="ID_1781452813" CREATED="1453253214997" MODIFIED="1453253244743"/>
<node TEXT="no.70" ID="ID_259459708" CREATED="1453253319722" MODIFIED="1453253323658"/>
</node>
<node TEXT="no.17&#x7528;&#x5b57;&#x7b26;&#x7f16;&#x7801;&#x63d2;&#x5165;&#x975e;&#x5e38;&#x7528;&#x5b57;&#x7b26;" ID="ID_1388941333" CREATED="1453251489891" MODIFIED="1453253358108">
<node TEXT="&lt;C-v&gt;{code}" ID="ID_1506092422" CREATED="1453253424525" MODIFIED="1453253488023"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h i_CTRL_V_digit
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&lt;C-v&gt;u{code}" ID="ID_1786708849" CREATED="1453253454093" MODIFIED="1453253466599"/>
<node TEXT="&lt;C-v&gt;{nondigit}" ID="ID_1630642540" CREATED="1453253844192" MODIFIED="1453253861966"/>
<node TEXT="&lt;C-k&gt;{char1}{char2}" ID="ID_743397128" CREATED="1453253866315" MODIFIED="1453253892034"/>
<node TEXT="ga" ID="ID_1081968687" CREATED="1453253498069" MODIFIED="1453253512353"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h ga
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.18&#x7528;&#x4e8c;&#x5408;&#x5b57;&#x6bcd;&#x63d2;&#x5165;&#x975e;&#x5e38;&#x7528;&#x5b57;&#x7b26;" ID="ID_1121912764" CREATED="1453251489891" MODIFIED="1453253919630">
<node TEXT="&lt;C-k&gt;{char1}{char2}" ID="ID_1481024255" CREATED="1453253866315" MODIFIED="1453253951674"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h digraphs-default
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.19&#x7528;&#x66ff;&#x6362;&#x6a21;&#x5f0f;&#x66ff;&#x6362;&#x5df2;&#x6709;&#x6587;&#x672c;" ID="ID_1319529186" CREATED="1453251489891" MODIFIED="1453254175606"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      insert_mode/replace.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="r" ID="ID_1189669033" CREATED="1453254566371" MODIFIED="1453254590316"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h r
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="gr" ID="ID_1748042097" CREATED="1453254561973" MODIFIED="1453254565440"/>
<node TEXT="R" ID="ID_1823213675" CREATED="1453254573967" MODIFIED="1453254575297"/>
<node TEXT="gR" ID="ID_1759406674" CREATED="1453254569030" MODIFIED="1453254572676"/>
</node>
</node>
<node TEXT="04-&#x53ef;&#x89c6;&#x6a21;&#x5f0f;" POSITION="right" ID="ID_324847076" CREATED="1451538445622" MODIFIED="1453255974620">
<edge COLOR="#7c0000"/>
<node TEXT="no.20&#x6df1;&#x5165;&#x7406;&#x89e3;&#x53ef;&#x89c6;&#x6a21;&#x5f0f;" ID="ID_476017950" CREATED="1453254756301" MODIFIED="1453255807112">
<node TEXT="&lt;C-g&gt;&#x5207;&#x6362;&#x53ef;&#x89c6;&#x6a21;&#x5f0f;&#x4e0e;&#x9009;&#x62e9;&#x6a21;&#x5f0f;" ID="ID_61341423" CREATED="1453255901210" MODIFIED="1453255979650"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h Select-mode
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.21&#x9009;&#x62e9;&#x9ad8;&#x4eae;&#x9009;&#x533a;" ID="ID_1156694371" CREATED="1453254759348" MODIFIED="1453255822797">
<node TEXT="v" ID="ID_1097747463" CREATED="1453255953972" MODIFIED="1453255958785"/>
<node TEXT="V" ID="ID_263434647" CREATED="1453255959501" MODIFIED="1453255960926"/>
<node TEXT="&lt;C-v&gt;" ID="ID_503400319" CREATED="1453255961384" MODIFIED="1453255966478"/>
<node TEXT="gv" ID="ID_645541584" CREATED="1453255966848" MODIFIED="1453255968093"/>
<node TEXT="o o" ID="ID_1729497490" CREATED="1453256161000" MODIFIED="1453256164093">
<node TEXT="&#x9009;&#x62e9;&#x6a21;&#x5f0f;&#x65f6;&#x5207;&#x6362;&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x6d3b;&#x52a8;&#x7aef;" ID="ID_915800868" CREATED="1453256166455" MODIFIED="1453256179361"/>
</node>
</node>
<node TEXT="no.22&#x91cd;&#x590d;&#x6267;&#x884c;&#x9762;&#x5411;&#x884c;&#x7684;&#x53ef;&#x89c6;&#x547d;&#x4ee4;" ID="ID_1357476472" CREATED="1453254762330" MODIFIED="1453256216302"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      visual_mode/fibonacci-malformed.py
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x8bbe;&#x7f6e;&#x7f29;&#x8fdb;" ID="ID_1706841392" CREATED="1453256244242" MODIFIED="1453256248580">
<node TEXT="set tabstop=4 shiftwidth=4 softtabstop=4 expandtab" ID="ID_1044244862" CREATED="1453256249874" MODIFIED="1453256291238"/>
</node>
<node TEXT="&#x5148;&#x7f29;&#x8fdb;&#x4e00;&#x6b21;,&#x7136;&#x540e;&#x91cd;&#x590d;" ID="ID_766490264" CREATED="1453256336340" MODIFIED="1453256352218">
<node TEXT="gv&gt;." ID="ID_1857323621" CREATED="1453256354731" MODIFIED="1453256449394"/>
</node>
</node>
<node TEXT="no.23&#x53ea;&#x8981;&#x53ef;&#x80fd;,&#x6700;&#x597d;&#x7528;&#x64cd;&#x4f5c;&#x7b26;&#x547d;&#x4ee4;,&#x800c;&#x4e0d;&#x662f;&#x53ef;&#x89c6;&#x547d;&#x4ee4;" ID="ID_622664950" CREATED="1453256463307" MODIFIED="1453256543535"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      visual_mode/list-of-links.html
    </p>
  </body>
</html>

</richcontent>
<node TEXT="vit U j. j." ID="ID_1504550586" CREATED="1453256555649" MODIFIED="1453256639433"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h v_U
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="gUit j. j." ID="ID_212669917" CREATED="1453256593214" MODIFIED="1453256668915"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      gU{motion}
    </p>
    <p>
      :h gU
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.24&#x7528;&#x9762;&#x5411;&#x5217;&#x5757;&#x7684;&#x53ef;&#x89c6;&#x6a21;&#x5f0f;&#x7f16;&#x8f91;&#x8868;&#x683c;&#x6570;&#x636e;" ID="ID_1475016340" CREATED="1453256686116" MODIFIED="1453256765104"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      visual_mode/chapter-table.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;C-v&gt;3j x... gv r| yyp Vr-" ID="ID_1027006344" CREATED="1453256824494" MODIFIED="1453256863768"/>
</node>
<node TEXT="no.25&#x4fee;&#x6539;&#x5217;&#x6587;&#x672c;" ID="ID_1430130745" CREATED="1453256886561" MODIFIED="1453256915061"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      visual_mode/sprite.css
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;C-v&gt;jje c components &lt;Esc&gt;" ID="ID_1455919619" CREATED="1453256956654" MODIFIED="1453256982207"/>
</node>
<node TEXT="no.26&#x5728;&#x957f;&#x77ed;&#x4e0d;&#x4ee5;&#x7684;&#x9ad8;&#x4eae;&#x5757;&#x540e;&#x6dfb;&#x52a0;&#x6587;&#x672c;" ID="ID_802920364" CREATED="1453256995125" MODIFIED="1453257033389"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the_vim_way/2_foo_bar.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;C-v&gt;jj$ A; &lt;Esc&gt;" ID="ID_574320480" CREATED="1453257041134" MODIFIED="1453257068467"/>
<node TEXT="&lt;C-v&gt;jj$ I# &lt;Esc&gt;" ID="ID_497620434" CREATED="1453257041134" MODIFIED="1453257100079"/>
<node TEXT="&#x6280;&#x5de7;51 &#x53ef;&#x89c6;&#x6a21;&#x5f0f;&#x4e2d;&#x7684; i a &#x6587;&#x672c;&#x5bf9;&#x8c61;" ID="ID_1871853456" CREATED="1453257114191" MODIFIED="1453257136210"/>
</node>
</node>
<node TEXT="05-&#x547d;&#x4ee4;&#x884c;&#x6a21;&#x5f0f;" POSITION="right" ID="ID_180061440" CREATED="1451538457351" MODIFIED="1453169153769">
<edge COLOR="#00007c"/>
<node TEXT="no.27&#x7ed3;&#x8bc6;vim&#x7684;&#x547d;&#x4ee4;&#x884c;&#x6a21;&#x5f0f;" ID="ID_1313434979" CREATED="1453258808923" MODIFIED="1453259961318"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h vi-differences
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x64cd;&#x4f5c;&#x7f13;&#x51b2;&#x533a;&#x6587;&#x672c;&#x7684;Ex&#x547d;&#x4ee4;" ID="ID_1533844995" CREATED="1453259839407" MODIFIED="1453279399798"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#34920;5-1
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":[range]delete [x]" ID="ID_1071928143" CREATED="1453259068302" MODIFIED="1453259089321">
<node TEXT="&#x5220;&#x9664;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x7684;&#x884c;[&#x5230;&#x5bc4;&#x5b58;&#x5668; x &#x4e2d;]" ID="ID_906614184" CREATED="1453259288657" MODIFIED="1453259310265"/>
</node>
<node TEXT=":[range]yank [x]" ID="ID_1653181689" CREATED="1453259091203" MODIFIED="1453259121208">
<node TEXT="&#x590d;&#x5236;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x7684;&#x884c;[&#x5230;&#x5bc4;&#x5b58;&#x5668; x &#x4e2d;]" ID="ID_1430739133" CREATED="1453259311285" MODIFIED="1453259315771"/>
</node>
<node TEXT=":[line]put [x]" ID="ID_1075326335" CREATED="1453259092591" MODIFIED="1453259133878">
<node TEXT="&#x5728;&#x6307;&#x5b9a;&#x884c;&#x4e0b;&#x65b9;&#x7c98;&#x8d34;&#x5bc4;&#x5b58;&#x5668; x &#x4e2d;&#x7684;&#x5185;&#x5bb9;" ID="ID_430547790" CREATED="1453259068302" MODIFIED="1453259455063"/>
</node>
<node TEXT=":[range]copy [address]" ID="ID_280085405" CREATED="1453259095264" MODIFIED="1453259154458">
<node TEXT="&#x628a;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x5185;&#x7684;&#x884c;&#x62f7;&#x8d1d;&#x5230;{address}&#x6240;&#x6307;&#x5b9a;&#x7684;&#x884c;&#x4e4b;&#x4e0b;" ID="ID_1347102950" CREATED="1453259457691" MODIFIED="1453259478985"/>
</node>
<node TEXT=":[range]move [address]" ID="ID_1123009210" CREATED="1453259098553" MODIFIED="1453259165367">
<node TEXT="&#x628a;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x5185;&#x7684;&#x884c;&#x79fb;&#x52a8;&#x5230;{address}&#x6240;&#x6307;&#x5b9a;&#x7684;&#x884c;&#x4e4b;&#x4e0b;" ID="ID_1430171407" CREATED="1453259480087" MODIFIED="1453259485675"/>
</node>
<node TEXT=":[range]join" ID="ID_1315007034" CREATED="1453259100459" MODIFIED="1453259176644">
<node TEXT="&#x8fde;&#x63a5;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x5185;&#x7684;&#x884c;" ID="ID_1193956886" CREATED="1453259488942" MODIFIED="1453259501546"/>
</node>
<node TEXT=":[range]normal {commands}" ID="ID_676896288" CREATED="1453259103004" MODIFIED="1453259188754">
<node TEXT="&#x628a;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x5185;&#x51fa;&#x73b0;{pattern}&#x7684;&#x5730;&#x65b9;&#x66ff;&#x6362;&#x4e3a;{string}" ID="ID_1609043536" CREATED="1453259512652" MODIFIED="1453259534150"/>
</node>
<node TEXT=":[range]substitute/{pattern}/{string}/[flags]" ID="ID_683450529" CREATED="1453259105481" MODIFIED="1453259216719">
<node TEXT="&#x628a;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x5185;&#x51fa;&#x73b0;{pattern}&#x7684;&#x7684;&#x65b9;&#x66ff;&#x6362;&#x4e3a;{string}" ID="ID_1397005284" CREATED="1453259540004" MODIFIED="1453259558151"/>
</node>
<node TEXT=":[range]global/{pattern}/[cmd]" ID="ID_1134408534" CREATED="1453259106950" MODIFIED="1453259244288">
<node TEXT="&#x5bf9;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x5185;&#x5339;&#x914d;{pattern}&#x7684;&#x6240;&#x6709;&#x884c;, &#x5728;&#x5176;&#x4e0a;&#x6267;&#x884c;Ex&#x547d;&#x4ee4;{cmd}" ID="ID_1262746128" CREATED="1453259560311" MODIFIED="1453259590996"/>
</node>
</node>
<node TEXT="no.34" ID="ID_266392184" CREATED="1453259879096" MODIFIED="1453259884590"/>
</node>
<node TEXT="no.28&#x5728;&#x4e00;&#x884c;&#x6216;&#x591a;&#x4e2a;&#x8fde;&#x7eed;&#x884c;&#x4e0a;&#x6267;&#x884c;&#x547d;&#x4ee4;" ID="ID_1330795148" CREATED="1453258833995" MODIFIED="1453259982059"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ex_mode/practical-vim.html
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":1 :print :$ :p :3p :2,5p" ID="ID_536641836" CREATED="1453260126903" MODIFIED="1453260170751"/>
<node TEXT="{start},{end}" ID="ID_1258440752" CREATED="1453260172145" MODIFIED="1453260188661">
<node TEXT=":/&lt;html&gt;/,/&lt;\/html&gt;/p" ID="ID_1593617994" CREATED="1453278487361" MODIFIED="1453278504535">
<node TEXT="&#x7528;&#x641c;&#x7d22;&#x6a21;&#x5f0f;&#x6307;&#x5b9a;&#x8303;&#x56f4;" ID="ID_1962078506" CREATED="1453278522748" MODIFIED="1453278533010"/>
</node>
<node TEXT=":/&lt;html&gt;/+1,/&lt;\/html&gt;-1/p" ID="ID_563226563" CREATED="1453278487361" MODIFIED="1453278567524">
<node TEXT="&#x7528;&#x641c;&#x7d22;&#x6a21;&#x5f0f;&#x52a0;&#x504f;&#x79fb;&#x5730;&#x5740;&#x6307;&#x5b9a;&#x8303;&#x56f4;" ID="ID_661368278" CREATED="1453278522748" MODIFIED="1453278582779"/>
</node>
</node>
<node TEXT=":&apos;&lt;,&apos;&gt;p" ID="ID_1729705548" CREATED="1453278459893" MODIFIED="1453278473567">
<node TEXT="&#x7528;&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x6307;&#x5b9a;&#x5185;&#x5bb9;" ID="ID_1726634217" CREATED="1453278513046" MODIFIED="1453278520819"/>
</node>
<node TEXT="&#x5730;&#x5740;&#x53ca;&#x8303;&#x56f4;&#x7b26;&#x53f7;&#x603b;&#x7ed3;" ID="ID_472303333" CREATED="1453278612598" MODIFIED="1453278639235">
<node TEXT="1" ID="ID_1389837042" CREATED="1453278641063" MODIFIED="1453278643656">
<node TEXT="&#x6587;&#x4ef6;&#x7b2c;&#x4e00;&#x884c;" ID="ID_826289565" CREATED="1453278672883" MODIFIED="1453278677777"/>
</node>
<node TEXT="$" ID="ID_1807822726" CREATED="1453278644021" MODIFIED="1453278645224">
<node TEXT="&#x6587;&#x4ef6;&#x6700;&#x540e;&#x4e00;&#x884c;" ID="ID_1771036553" CREATED="1453278705374" MODIFIED="1453278719767"/>
</node>
<node TEXT="0" ID="ID_457757453" CREATED="1453278645704" MODIFIED="1453278649423">
<node TEXT="&#x865a;&#x62df;&#x884c;,&#x4f4d;&#x4e8e;&#x6587;&#x4ef6;&#x7b2c;&#x4e00;&#x884c;&#x4e0a;&#x65b9;" ID="ID_1747347307" CREATED="1453278721519" MODIFIED="1453278734146"/>
</node>
<node TEXT="." ID="ID_258620452" CREATED="1453278650460" MODIFIED="1453278653101">
<node TEXT="&#x5f53;&#x524d;&#x884c;" ID="ID_504139001" CREATED="1453278736419" MODIFIED="1453278739111"/>
</node>
<node TEXT="&apos;m" ID="ID_954605491" CREATED="1453278653781" MODIFIED="1453278658225">
<node TEXT="&#x5305;&#x542b;&#x4f4d;&#x7f6e;&#x6807;&#x8bb0;m&#x7684;&#x884c;" ID="ID_1029788218" CREATED="1453278741124" MODIFIED="1453278749261"/>
</node>
<node TEXT="&apos;&lt;" ID="ID_518760045" CREATED="1453278659211" MODIFIED="1453278663544">
<node TEXT="&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x8d77;&#x59cb;&#x884c;" ID="ID_1696792345" CREATED="1453278751269" MODIFIED="1453278760118"/>
</node>
<node TEXT="&apos;&gt;" ID="ID_1876692300" CREATED="1453278663999" MODIFIED="1453278667052">
<node TEXT="&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x7ed3;&#x675f;&#x884c;" ID="ID_1048802854" CREATED="1453278762041" MODIFIED="1453278768109"/>
</node>
<node TEXT="%" ID="ID_1933982048" CREATED="1453278667527" MODIFIED="1453278668801">
<node TEXT="&#x6574;&#x4e2a;&#x6587;&#x4ef6;(:1,$&#x7684;&#x7b80;&#x5199;&#x5f62;&#x5f0f;)" ID="ID_1854574428" CREATED="1453278770135" MODIFIED="1453278787115"/>
</node>
</node>
</node>
<node TEXT="no.29&#x4f7f;&#x7528;&apos;:t&apos;&#x548c;&apos;:m&apos;&#x547d;&#x4ee4;&#x590d;&#x5236;&#x548c;&#x79fb;&#x52a8;&#x884c;" ID="ID_82653855" CREATED="1453258856192" MODIFIED="1453278847352"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ex_mode/shopping-list.todo
    </p>
  </body>
</html>

</richcontent>
<node TEXT="6copy ." ID="ID_1441888825" CREATED="1453278902262" MODIFIED="1453278976451"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      6t.
    </p>
    <p>
      6co.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="[range]copy {address}" ID="ID_244897051" CREATED="1453278912745" MODIFIED="1453278932012"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h copy
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="6move ." ID="ID_1356303443" CREATED="1453279047110" MODIFIED="1453279057301"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      6m.
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="[range]move {address}" ID="ID_817978821" CREATED="1453278912745" MODIFIED="1453279033666"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h move
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.30&#x5728;&#x6307;&#x5b9a;&#x8303;&#x56f4;&#x4e0a;&#x6267;&#x884c;&#x666e;&#x901a;&#x6a21;&#x5f0f;&#x547d;&#x4ee4;" ID="ID_523839292" CREATED="1453258892668" MODIFIED="1453279124846"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ex_mode/foobar.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT="A;&lt;Esc&gt; jVG :&apos;&lt;,&apos;&gt;normal ." ID="ID_1265659013" CREATED="1453279134170" MODIFIED="1453279175992"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      %normal A;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="%nomarl I//" ID="ID_1055563310" CREATED="1453279193786" MODIFIED="1453279200807"/>
<node TEXT=":normal @q" ID="ID_1224066020" CREATED="1453279210687" MODIFIED="1453279219418"/>
<node TEXT="no.67, no.69" ID="ID_481879317" CREATED="1453279220161" MODIFIED="1453279230492"/>
</node>
<node TEXT="no.31&#x91cd;&#x590d;&#x4e0a;&#x6b21;&#x7684;ex&#x547d;&#x4ee4;" ID="ID_711831958" CREATED="1453258912361" MODIFIED="1453258923150">
<node TEXT="@: @@" ID="ID_978843337" CREATED="1453279274782" MODIFIED="1453279319684"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h @:
    </p>
    <p>
      :h quote_:
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT=":bn :bp @: &lt;C-o&gt;" ID="ID_1982761817" CREATED="1453279403423" MODIFIED="1453279455430"/>
<node TEXT="&#x8868;5-1&#x547d;&#x4ee4;&#x9700;&#x8981;&#x7528; u &#x56de;&#x9000;" ID="ID_1598048666" CREATED="1453279424076" MODIFIED="1453279433206"/>
</node>
<node TEXT="no.32&#x81ea;&#x52a8;&#x8865;&#x5168;ex&#x547d;&#x4ee4;" ID="ID_1573317501" CREATED="1453258923588" MODIFIED="1453258938396">
<node TEXT=":col&lt;C-d&gt; &lt;Tab&gt; &lt;S-Tab&gt;" ID="ID_756828948" CREATED="1453279473459" MODIFIED="1453280473440"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h c_CTRL-D
    </p>
    <p>
      :h :command-complete
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x591a;&#x4e2a;&#x9009;&#x9879;&#x95f4;&#x9009;&#x62e9;" ID="ID_529248547" CREATED="1453280474145" MODIFIED="1453280552591"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h 'wildmode'
    </p>
  </body>
</html>

</richcontent>
<node TEXT="set wildmode=longst,list" ID="ID_81163559" CREATED="1453280486310" MODIFIED="1453280559275"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      like bash
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="set wildmenu&#xa;set wildmode=full" ID="ID_187572815" CREATED="1453280533373" MODIFIED="1453280572307"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      like zsh
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;Ctrl-n&gt; &lt;Ctrl-p&gt;" ID="ID_485594242" CREATED="1453280604591" MODIFIED="1453280616706"/>
</node>
</node>
</node>
<node TEXT="no.33&#x628a;&#x5355;&#x8bcd;&#x63d2;&#x5165;&#x5230;&#x547d;&#x4ee4;&#x884c;" ID="ID_1676538112" CREATED="1453258938869" MODIFIED="1453280758461"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ex_mode/loop.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;C-r&gt;&lt;C-w&gt;" ID="ID_1160332374" CREATED="1453280629881" MODIFIED="1453280901655"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h c_CTRL-R_CTRL-W
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x590d;&#x5236;&#x5149;&#x6807;&#x4e0b;&#x7684;&#x5355;&#x8bcd;&#x5e76;&#x628a;&#x5b83;&#x63d2;&#x5165;&#x547d;&#x4ee4;&#x884c;" ID="ID_1807448050" CREATED="1453280640893" MODIFIED="1453280660255"/>
</node>
<node TEXT="&lt;C-r&gt;&lt;C-a&gt;" ID="ID_367211319" CREATED="1453280903781" MODIFIED="1453280909921"/>
<node TEXT="* cw counter&lt;Esc&gt; :%s//&lt;C-r&gt;&lt;C-w&gt;/g" ID="ID_1425919564" CREATED="1453280744677" MODIFIED="1453280798139"/>
<node TEXT="no.48, no.90" ID="ID_879745552" CREATED="1453280802435" MODIFIED="1453280890310"/>
</node>
<node TEXT="no.34&#x56de;&#x6eaf;&#x5386;&#x53f2;&#x547d;&#x4ee4;" ID="ID_1246992334" CREATED="1453258961006" MODIFIED="1453258969775">
<node TEXT="set history=2000" ID="ID_968698766" CREATED="1453280940942" MODIFIED="1453280962142"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h viminfo
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="ex_mode/history-scrollers.vim" ID="ID_836328532" CREATED="1453281021508" MODIFIED="1453281032516">
<node TEXT="cnoremap &lt;C-p&gt; &lt;Up&gt;&#xa;cnoremap &lt;C-n&gt; &lt;Down&gt;" ID="ID_242541193" CREATED="1453281035295" MODIFIED="1453281066851"/>
</node>
<node TEXT=":write&#xa;:!ruby %" ID="ID_1876411782" CREATED="1453281090027" MODIFIED="1453281141112"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :write | !ruby %
    </p>
    <p>
      :h cmdwin
    </p>
  </body>
</html>

</richcontent>
<node TEXT="q: A | &lt;Esc&gt; J :s/write/update &lt;Enter&gt;" ID="ID_1936431995" CREATED="1453281390589" MODIFIED="1453281426791"/>
</node>
<node TEXT="&#x6253;&#x5f00;&#x547d;&#x4ee4;&#x884c;&#x7684;&#x51e0;&#x79cd;&#x65b9;&#x5f0f;" ID="ID_1945668915" CREATED="1453281438040" MODIFIED="1453281502691"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :q&#36864;&#20986;
    </p>
  </body>
</html>

</richcontent>
<node TEXT="q/" ID="ID_80452228" CREATED="1453281454056" MODIFIED="1453281456714"/>
<node TEXT="q:" ID="ID_1426255725" CREATED="1453281457455" MODIFIED="1453281459966"/>
<node TEXT="&lt;Ctrl-f&gt;" ID="ID_273809608" CREATED="1453281461019" MODIFIED="1453281468771"/>
</node>
<node TEXT="no.85" ID="ID_364814311" CREATED="1453281508950" MODIFIED="1453281512075"/>
</node>
<node TEXT="no.35&#x8fd0;&#x884c;shell&#x547d;&#x4ee4;" ID="ID_1644015054" CREATED="1453258970043" MODIFIED="1453281743457"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      ex_mode/emails.csv
    </p>
    <p>
      :h :shell
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x51e0;&#x79cd;&#x6267;&#x884c;shell&#x7684;&#x65b9;&#x5f0f;" ID="ID_1511348721" CREATED="1453281542463" MODIFIED="1453281561818">
<node TEXT=":!{cmd}" ID="ID_1089761668" CREATED="1453281563158" MODIFIED="1453281579085"/>
<node TEXT=":shell" ID="ID_715309587" CREATED="1453281579851" MODIFIED="1453281583212"/>
<node TEXT="!!" ID="ID_1158453373" CREATED="1453281584139" MODIFIED="1453281710597"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :read !{cmd}
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="C-z fg" ID="ID_1027306870" CREATED="1453281624325" MODIFIED="1453281629295"/>
</node>
<node TEXT="!&#x4f4d;&#x7f6e;" ID="ID_574836177" CREATED="1453281748582" MODIFIED="1453281756788">
<node TEXT=":write !sh&#xa;:write ! sh" ID="ID_1060158492" CREATED="1453281758329" MODIFIED="1453281770380">
<node TEXT="&#x5199;&#x5165;&#x5185;&#x5bb9;&#x4ea4;&#x7ed9;sh&#x6267;&#x884c;" ID="ID_595110128" CREATED="1453281779986" MODIFIED="1453281789911"/>
</node>
<node TEXT=":write! sh" ID="ID_910705477" CREATED="1453281771360" MODIFIED="1453281777546">
<node TEXT="&#x5f3a;&#x5236;&#x5199;&#x5165;sh&#x6587;&#x4ef6;" ID="ID_742601432" CREATED="1453281791554" MODIFIED="1453281798503"/>
</node>
</node>
<node TEXT="&#x4f7f;&#x7528;&#x5916;&#x90e8;&#x547d;&#x4ee4;&#x5904;&#x7406;&#x7f13;&#x51b2;&#x533a;&#x5185;&#x5bb9;" ID="ID_1686529976" CREATED="1453281825480" MODIFIED="1453281870573"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h !
    </p>
  </body>
</html>

</richcontent>
<node TEXT="!{motion}" ID="ID_94084345" CREATED="1453281891801" MODIFIED="1453281896650"/>
<node TEXT="[range]!{filter}" ID="ID_441657640" CREATED="1453281841088" MODIFIED="1453281863835"/>
</node>
</node>
</node>
</node>
</map>

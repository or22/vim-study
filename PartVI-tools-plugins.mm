<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="PartVI-tools-plugins" ID="ID_1653455412" CREATED="1451538786232" MODIFIED="1451539068046" LINK="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;.mm">
<edge STYLE="bezier" COLOR="#ff0000" WIDTH="thin"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="16-ctags" POSITION="right" ID="ID_1961111879" CREATED="1451538813047" MODIFIED="1451538821815">
<node TEXT="no.101&#x7ed3;&#x8bc6;ctags" ID="ID_1343608321" CREATED="1453703006600" MODIFIED="1453703006600"/>
<node TEXT="no.102&#x914d;&#x7f6e;Vim&#x4f7f;&#x7528;ctags" ID="ID_1687140936" CREATED="1453703006600" MODIFIED="1453703006600"/>
<node TEXT="no.103&#x4f7f;&#x7528;Vim&#x7684;&#x6807;&#x7b7e;&#x8df3;&#x8f6c;&#x547d;&#x4ee4;,&#x6d4f;&#x89c8;&#x5173;&#x952e;&#x5b57;&#x7684;&#x5b9a;&#x4e49;" ID="ID_1855839605" CREATED="1453703006605" MODIFIED="1453703006605"/>
</node>
<node TEXT="17-Quickfix" POSITION="right" ID="ID_1735276065" CREATED="1451538822946" MODIFIED="1451538832712">
<node TEXT="no.104&#x4e0d;&#x7528;&#x79bb;&#x5f00;Vim&#x4e5f;&#x80fd;&#x7f16;&#x8bd1;&#x4ee3;&#x7801;" ID="ID_953000327" CREATED="1453703017917" MODIFIED="1453703017917"/>
<node TEXT="no.105&#x6d4f;&#x89c8;Quickfix&#x5217;&#x8868;" ID="ID_780495138" CREATED="1453703017917" MODIFIED="1453703017917"/>
<node TEXT="no.106&#x56de;&#x6eaf;&#x4ee5;&#x524d;&#x7684;Quickfix&#x5217;&#x8868;" ID="ID_491188066" CREATED="1453703017924" MODIFIED="1453703017924"/>
<node TEXT="no.107&#x5b9a;&#x5236;&#x5916;&#x90e8;&#x7f16;&#x8bd1;&#x5668;" ID="ID_784667025" CREATED="1453703017926" MODIFIED="1453703017926"/>
</node>
<node TEXT="18-grep-vimgrep-etc" POSITION="right" ID="ID_593068672" CREATED="1451538833757" MODIFIED="1451538851380">
<node TEXT="no.108&#x4e0d;&#x5fc5;&#x79bb;&#x5f00;Vim&#x9875;&#x80fd;&#x8c03;&#x7528;grep" ID="ID_930122453" CREATED="1453703026819" MODIFIED="1453703026819"/>
<node TEXT="no.109&#x5b9a;&#x5236;grep&#x7a0b;&#x5e8f;" ID="ID_135438888" CREATED="1453703026819" MODIFIED="1453703026819"/>
<node TEXT="no.110&#x4f7f;&#x7528;Vim&#x5185;&#x90e8;&#x7684;Grep" ID="ID_1497625877" CREATED="1453703026824" MODIFIED="1453703026824"/>
</node>
<node TEXT="19-&#x81ea;&#x52a8;&#x8865;&#x5168;" POSITION="right" ID="ID_898900770" CREATED="1451538853062" MODIFIED="1451538859868">
<node TEXT="no.111&#x7ed3;&#x8bc6;Vim&#x7684;&#x5173;&#x952e;&#x5b57;&#x81ea;&#x52a8;&#x8865;&#x5168;" ID="ID_170527236" CREATED="1453703041381" MODIFIED="1453703041381"/>
<node TEXT="no.112&#x4e0e;&#x81ea;&#x52a8;&#x8865;&#x5168;&#x7684;&#x5f39;&#x51fa;&#x5f0f;&#x83dc;&#x5355;&#x8fdb;&#x884c;&#x4ea4;&#x4e92;" ID="ID_1866006765" CREATED="1453703041381" MODIFIED="1453703041381"/>
<node TEXT="no.113&#x638c;&#x63e1;&#x5173;&#x952e;&#x5b57;&#x7684;&#x6765;&#x9f99;&#x53bb;&#x8109;" ID="ID_1661394458" CREATED="1453703041386" MODIFIED="1453703041386"/>
<node TEXT="no.114&#x4f7f;&#x7528;&#x5b57;&#x5178;&#x4e2d;&#x7684;&#x5355;&#x8bcd;&#x8fdb;&#x884c;&#x81ea;&#x52a8;&#x8865;&#x5168;" ID="ID_1154058419" CREATED="1453703041388" MODIFIED="1453703041388"/>
<node TEXT="no.115&#x81ea;&#x52a8;&#x8865;&#x5168;&#x6574;&#x884c;&#x6587;&#x672c;" ID="ID_149282549" CREATED="1453703041390" MODIFIED="1453703041390"/>
<node TEXT="no.116&#x81ea;&#x52a8;&#x8865;&#x5168;&#x6587;&#x4ef6;&#x540d;" ID="ID_808497008" CREATED="1453703041390" MODIFIED="1453703041390"/>
<node TEXT="no.117&#x6839;&#x636e;&#x4e0a;&#x4e0b;&#x6587;&#x81ea;&#x52a8;&#x8865;&#x5168;" ID="ID_892739049" CREATED="1453703041391" MODIFIED="1453703041391"/>
</node>
<node TEXT="20-&#x81ea;&#x52a8;&#x66f4;&#x6b63;" POSITION="right" ID="ID_1734461376" CREATED="1451538860898" MODIFIED="1451538885483">
<node TEXT="no.118&#x5bf9;&#x4f60;&#x7684;&#x5de5;&#x4f5c;&#x8fdb;&#x884c;&#x62fc;&#x5199;&#x68c0;&#x67e5;" ID="ID_1051654354" CREATED="1453703055626" MODIFIED="1453703055626"/>
<node TEXT="no.119&#x4f7f;&#x7528;&#x5176;&#x4ed6;&#x62fc;&#x5199;&#x5b57;&#x5178;" ID="ID_325746021" CREATED="1453703055626" MODIFIED="1453703055626"/>
<node TEXT="no.120&#x5c06;&#x5355;&#x8bcd;&#x6dfb;&#x52a0;&#x5230;&#x62fc;&#x5199;&#x6587;&#x4ef6;&#x4e2d;" ID="ID_1953621043" CREATED="1453703055637" MODIFIED="1453703055637"/>
<node TEXT="no.121&#x5728;&#x63d2;&#x5165;&#x6a21;&#x5f0f;&#x4e0b;&#x66f4;&#x6b63;&#x62fc;&#x5199;&#x9519;&#x8bef;" ID="ID_829388828" CREATED="1453703055638" MODIFIED="1453703055638"/>
</node>
<node TEXT="21-&#x63a5;&#x4e0b;&#x6765;&#x505a;&#x4ec0;&#x4e48;" POSITION="right" ID="ID_721704096" CREATED="1451538873423" MODIFIED="1451538902711">
<node TEXT="keep practicing" ID="ID_721165516" CREATED="1453510372015" MODIFIED="1453510383198"/>
<node TEXT="DIY vim" ID="ID_297420132" CREATED="1453510384586" MODIFIED="1453510394905"/>
<node TEXT="more plugins" ID="ID_943578270" CREATED="1453510396813" MODIFIED="1453510409134"/>
</node>
</node>
</map>

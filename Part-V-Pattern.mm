<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Part-V-Pattern" ID="ID_1272520573" CREATED="1451538705033" MODIFIED="1451539066259" LINK="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;.mm">
<edge STYLE="bezier" COLOR="#7c7c00" WIDTH="thin"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="8"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="12-&#x6309;&#x6a21;&#x5f0f;&#x5339;&#x914d;&#x53ca;&#x539f;&#x4e49;&#x5339;&#x914d;" POSITION="right" ID="ID_1977832817" CREATED="1451538731472" MODIFIED="1451538748532">
<node TEXT="no.72&#x8c03;&#x6574;&#x67e5;&#x627e;&#x6a21;&#x5f0f;&#x7684;&#x5927;&#x5c0f;&#x5199;" ID="ID_34159813" CREATED="1453702934428" MODIFIED="1453702934428">
<node TEXT="&#x5168;&#x5c40;&#x8bbe;&#x7f6e;&#x5927;&#x5c0f;&#x5199;&#x654f;&#x611f;&#x6027;" ID="ID_1816048800" CREATED="1454050264985" MODIFIED="1454050273469">
<node TEXT="set ignorecase" ID="ID_580495094" CREATED="1454050274585" MODIFIED="1454050285201"/>
</node>
<node TEXT="&#x6bcf;&#x6b21;&#x67e5;&#x627e;&#x65f6;&#x8bbe;&#x7f6e;&#x5927;&#x5c0f;&#x5199;&#x654f;&#x611f;&#x6027;" ID="ID_146866128" CREATED="1454050308376" MODIFIED="1454050323028">
<node TEXT="\c \C" ID="ID_845542305" CREATED="1454050299442" MODIFIED="1454050305591"/>
</node>
<node TEXT="&#x542f;&#x7528;&#x66f4;&#x5177;&#x667a;&#x80fd;&#x7684;&#x5927;&#x5c0f;&#x5199;&#x654f;&#x611f;&#x6027;&#x8bbe;&#x7f6e;" ID="ID_298979779" CREATED="1454050331084" MODIFIED="1454050419824"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h /ignorecase
    </p>
  </body>
</html>
</richcontent>
<node TEXT="set smartcase" ID="ID_1048337231" CREATED="1454050354663" MODIFIED="1454050398774"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21482;&#35201;&#22312;&#26597;&#25214;&#27169;&#24335;&#20013;&#20351;&#29992;&#20102;&#22823;&#20889;&#23383;&#27597;, ignorecase&#23601;&#19981;&#20877;&#29983;&#25928;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="pattern   &apos;ignorecase&apos;  &apos;smartcase&apos;       matches&#xa;foo       off           -               foo&#xa;foo       on            -               foo Foo FOO&#xa;Foo       on            off             foo Foo FOO&#xa;Foo       on            on                  Foo&#xa;\cfoo     -             -               foo Foo FOO&#xa;foo\C     -             -               foo" ID="ID_1503237909" CREATED="1454050442462" MODIFIED="1454050469694"/>
</node>
</node>
<node TEXT="no.73&#x6309;&#x6b63;&#x5219;&#x8868;&#x8fbe;&#x5f0f;&#x67e5;&#x627e;&#x65f6;,&#x4f7f;&#x7528;\v&#x6a21;&#x5f0f;&#x5f00;&#x5173;" ID="ID_652758738" CREATED="1453702934428" MODIFIED="1454050605747"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#19982;Perl&#30456;&#27604;,Vim&#27491;&#21017;&#34920;&#36798;&#24335;&#30340;&#35821;&#27861;&#39118;&#26684;&#26356;&#25509;&#36817;POSIX
    </p>
    <p>
      patterns/colors.css
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x9ed8;&#x8ba4;POSIX&#x67e5;&#x627e;&#x6a21;&#x5f0f;" ID="ID_966950861" CREATED="1454050964175" MODIFIED="1454050978412">
<node TEXT="/#\([0-9a-zA-Z]\{6}\|[0-9a-zA-Z]\{3}\)" ID="ID_1739414211" CREATED="1454050614098" MODIFIED="1454050791484"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      3&#31867;&#25324;&#21495;
    </p>
    <p>
      []&#32570;&#30465;&#20855;&#26377;&#29305;&#27530;&#21547;&#20041;
    </p>
    <p>
      {}&#38656;&#35201;&#36716;&#20041;,&#21482;&#38656;&#20026;&#24320;&#25324;&#21495;&#36716;&#20041;,&#19982;&#20043;&#23545;&#24212;&#30340;&#38381;&#25324;&#21495;&#21017;&#19981;&#24517;
    </p>
    <p>
      ()&#20250;&#25353;&#21407;&#20041;&#21305;&#37197;&#23383;&#31526;(&#21450;),&#24320;&#38381;&#37117;&#38656;&#35201;&#36716;&#20041;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="very magic&#x6a21;&#x5f0f;" ID="ID_1581368735" CREATED="1454050493027" MODIFIED="1454050896989"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21487;&#20197;&#35753;Vim&#36148;&#36817;Perl&#27491;&#21017;&#35821;&#27861;;
    </p>
    <p>
      :h \v
    </p>
    <p>
      &#20301;&#20110;\v&#21518;&#36793;&#30340;&#25152;&#26377;&#23383;&#31526;&#37117;&#20855;&#26377;&#29305;&#27530;&#21547;&#20041;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="/\v#([0-9a-zA-Z]{6}|[0-9a-zA-Z]{3})" ID="ID_1947018032" CREATED="1454050817514" MODIFIED="1454050842448"/>
</node>
<node TEXT="&#x7528;&#x5341;&#x516d;&#x8fdb;&#x5236;&#x5b57;&#x7b26;&#x7c7b;&#x8fdb;&#x4e00;&#x6b65;&#x4f18;&#x5316;&#x6a21;&#x5f0f;" ID="ID_205812498" CREATED="1454050904702" MODIFIED="1454050920044">
<node TEXT="/\v#(\x{6}|\x{3})" ID="ID_1404418160" CREATED="1454050921177" MODIFIED="1454051002103"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h /character-classes
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x4fdd;&#x7559;&#x4ee5;&#x5907;&#x6269;&#x5c55;&#x65f6;&#x4f7f;&#x7528;&#x5b57;&#x7b26;" ID="ID_1486662562" CREATED="1454051016062" MODIFIED="1454051041818">
<node TEXT="&#x6bd4;&#x5982; #" ID="ID_1813497137" CREATED="1454051045265" MODIFIED="1454051057140"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h /\\
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Vim&#x6a21;&#x5f0f;&#x8bed;&#x6cd5;&#x7684;&#x4f20;&#x627f;" ID="ID_425367493" CREATED="1454051081553" MODIFIED="1454051089785">
<node TEXT="\v" ID="ID_1753664497" CREATED="1454051090806" MODIFIED="1454051236576">
<font BOLD="true"/>
</node>
<node TEXT="\V" ID="ID_1805278904" CREATED="1454051095526" MODIFIED="1454051097334"/>
<node TEXT="\m" ID="ID_172404011" CREATED="1454051097989" MODIFIED="1454051099980">
<node TEXT="magic" ID="ID_730035973" CREATED="1454051111900" MODIFIED="1454051114571">
<node TEXT="&#x81ea;&#x52a8;&#x4e3a; . * &#x4ee5;&#x53ca; [] &#x8d4b;&#x4e88;&#x7279;&#x6b8a;&#x542b;&#x4e49;, &#x8bf8;&#x5982; + ? () {} &#x672a;&#x542b;&#x6709;&#x7279;&#x6b8a;&#x542b;&#x4e49;" ID="ID_1123638423" CREATED="1454051179060" MODIFIED="1454051225821"/>
</node>
</node>
<node TEXT="\M" ID="ID_1636207257" CREATED="1454051100868" MODIFIED="1454051102925">
<node TEXT="nomagic" ID="ID_1969981536" CREATED="1454051116487" MODIFIED="1454051119496">
<node TEXT="&#x7c7b;&#x4f3c;&#x4e8e;\V, &#x4f46;&#x662f; ^ &#x4e0e; $ &#x81ea;&#x52a8;&#x5177;&#x6709;&#x7279;&#x6b8a;&#x542b;&#x4e49;" ID="ID_471202140" CREATED="1454051129706" MODIFIED="1454051158414"/>
</node>
</node>
</node>
</node>
<node TEXT="no.74&#x6309;&#x539f;&#x4e49;&#x67e5;&#x627e;&#x65f6;,&#x4f7f;&#x7528;\V&#x539f;&#x4e49;&#x5f00;&#x5173;" ID="ID_975053973" CREATED="1453702934433" MODIFIED="1454051273285"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      patterns/excerpt-also-known-as.txt
    </p>
  </body>
</html>
</richcontent>
<node TEXT="/a.k.a&#xa;/a\.k\.a\.&#xa;/\Va.k.a" ID="ID_1302841684" CREATED="1454051278477" MODIFIED="1454051338780"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h /\V
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.75&#x4f7f;&#x7528;&#x5706;&#x62ec;&#x53f7;&#x6355;&#x83b7;&#x5b50;&#x5339;&#x914d;" ID="ID_312034506" CREATED="1453702934435" MODIFIED="1454051528465"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      patterns/springtime.txt
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x5339;&#x914d;&#x91cd;&#x590d;&#x5355;&#x8bcd;&#x6b63;&#x5219;" ID="ID_904101728" CREATED="1454051394840" MODIFIED="1454051402552">
<node TEXT="/\v&lt;(\w+)\_s+\1&gt;" ID="ID_1671225323" CREATED="1454051365958" MODIFIED="1454051393705"/>
<node TEXT="() \1 - \9" ID="ID_271668409" CREATED="1454051417972" MODIFIED="1454051452654"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h /\_
    </p>
    <p>
      :h 27.8
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="\0" ID="ID_89180992" CREATED="1454051492713" MODIFIED="1454051500607"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#25972;&#20010;&#21305;&#37197;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.76 no.93" ID="ID_1133758278" CREATED="1454051508319" MODIFIED="1454051533312"/>
</node>
<node TEXT="no.76&#x754c;&#x5b9a;&#x5355;&#x8bcd;&#x7684;&#x8fb9;&#x754c;" ID="ID_1851328507" CREATED="1453702934437" MODIFIED="1453702934437">
<node TEXT="/the&#xa;/\v&lt;the&gt;&lt;CR&gt;" ID="ID_1768226811" CREATED="1454051653160" MODIFIED="1454051931961"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the problem with these new recruits is that
    </p>
    <p>
      they don't keep their boots clean.
    </p>
    <p>
      &lt;&gt;&#38646;&#23485;&#24230;&#20803;&#23383;&#31526;, &#26412;&#36523;&#19981;&#21305;&#37197;&#20219;&#20309;&#23383;&#31526;, &#20165;&#34920;&#31034;&#21333;&#35789;
    </p>
    <p>
      &#19982;&#22260;&#32469;&#27492;&#21333;&#35789;&#30340;&#31354;&#30333;&#23383;&#31526;(&#25110;&#26631;&#28857;&#31526;&#21495;)&#20043;&#38388;&#30340;&#36793;&#30028;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="# &#x6216;&#x8005; * &#x95f4;&#x63a5;&#x4f7f;&#x7528;&#x5230;&#x5355;&#x8bcd;&#x5b9a;&#x754c;&#x7b26;" ID="ID_1701489307" CREATED="1454052429801" MODIFIED="1454052495858"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h *
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="g# g* &#x5219;&#x4e0d;&#x4f1a;&#x4f7f;&#x7528;&#x5355;&#x8bcd;&#x5b9a;&#x754c;&#x7b26;" ID="ID_737942813" CREATED="1454052467354" MODIFIED="1454052483297"/>
</node>
<node TEXT="&#x4f7f;&#x7528;&#x5706;&#x62ec;&#x53f7;&#x4f46;&#x4e0d;&#x6355;&#x83b7;&#x5176;&#x5185;&#x5bb9;" ID="ID_598253786" CREATED="1454051969208" MODIFIED="1454051986108">
<node TEXT="/\v(And|D)rew Neil&#xa;/\v%(And|D)rew Neil&#xa;/\v(%(And|D)rew) (Neil)&#xa;:%s//\2, \1/g" ID="ID_879969286" CREATED="1454051988383" MODIFIED="1454052103728"/>
</node>
<node TEXT="\W\ze\w&#x6a21;&#x62df;&#x5143;&#x5b57;&#x7b26;&lt;" ID="ID_393661752" CREATED="1454052135723" MODIFIED="1454052167219">
<node TEXT="\w&#x5339;&#x914d;&#x5355;&#x8bcd;&#x7c7b;&#x5b57;&#x7b26;&#x5305;&#x62ec;&#x5b57;&#x6bcd;&#x6570;&#x5b57;&#x53ca;_" ID="ID_1975252334" CREATED="1454052195251" MODIFIED="1454052215921"/>
</node>
<node TEXT="\w\zs\W&#x6a21;&#x62df;&#x5143;&#x5b57;&#x7b26;&gt;" ID="ID_287683794" CREATED="1454052168591" MODIFIED="1454052185761">
<node TEXT="\W&#x5339;&#x914d;&#x9664;&#x5355;&#x8bcd;&#x7c7b;&#x5b57;&#x7b26;&#x610f;&#x5916;&#x7684;&#x5176;&#x4ed6;&#x5b57;&#x7b26;" ID="ID_417867783" CREATED="1454052218322" MODIFIED="1454052238117"/>
</node>
<node TEXT="&#x5728;verymagic&#x6a21;&#x5f0f;&#x4e0b;, &lt;&gt;&#x5b57;&#x7b26;&#x53ef;&#x4ee5;&#x88ab;&#x89e3;&#x6790;&#x4e3a;&#x5355;&#x8bcd;&#x5b9a;&#x754c;&#x7b26;,&#xa;&#x800c;&#x5728;magic,nomagic&#x4ee5;&#x53ca;very nomagic&#x6a21;&#x5f0f;&#x4e0b;, &#x5219;&#x9700;&#x8f6c;&#x4e49;" ID="ID_1257378055" CREATED="1454052340341" MODIFIED="1454052396361"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h /\v
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="no.77" ID="ID_1915545481" CREATED="1454051943912" MODIFIED="1454051946426"/>
</node>
<node TEXT="no.77&#x754c;&#x5b9a;&#x5339;&#x914d;&#x7684;&#x8fb9;&#x754c;" ID="ID_1072700524" CREATED="1453702934438" MODIFIED="1454052534725"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      hlsearch
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x5339;&#x914d;&#x7684;&#x8fb9;&#x754c;" ID="ID_1911148423" CREATED="1454052565386" MODIFIED="1454052570029">
<node TEXT="\zs" ID="ID_1742434528" CREATED="1454052559761" MODIFIED="1454052575187"/>
<node TEXT="\ze" ID="ID_307174659" CREATED="1454052576476" MODIFIED="1454052579547"/>
<node TEXT="&#x5229;&#x7528;&#x4ee5;&#x4e0a;&#x4e24;&#x8005;&#x6765;&#x6536;&#x7a84;&#x5339;&#x914d;&#x7684;&#x8303;&#x56f4;" ID="ID_1514447457" CREATED="1454052603785" MODIFIED="1454052646887"/>
</node>
<node TEXT="&#x7528;&#x4f8b;" ID="ID_481151503" CREATED="1454052686300" MODIFIED="1454052691704">
<node TEXT="/\v&quot;[^&quot;]+&quot;&lt;CR&gt;&#xa;/\v&quot;\zs[^&quot;]+\ze&quot;&lt;CR&gt;" ID="ID_1724707502" CREATED="1454052692825" MODIFIED="1454052724118"/>
</node>
<node TEXT="&#x73af;&#x89c6;&#x8868;&#x8fbe;&#x5f0f;" ID="ID_1852326929" CREATED="1454052777009" MODIFIED="1454052823702"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h perl-patterns
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x5b9e;&#x73b0;&#x4e0a;&#x4f8b;&#x7684;perl(\v)&#x65b9;&#x6cd5;" ID="ID_1938396865" CREATED="1454052845565" MODIFIED="1454052857731">
<node TEXT="/\v&quot;@&lt;=[^&quot;]+&quot;@=" ID="ID_1674494649" CREATED="1454052858655" MODIFIED="1454052876133"/>
</node>
<node TEXT="&#x80af;&#x5b9a;&#x578b;&#x9006;&#x5e8f;&#x73af;&#x89c6; \zs" ID="ID_24276216" CREATED="1454052784455" MODIFIED="1454052800528"/>
<node TEXT="&#x80af;&#x5b9a;&#x578b;&#x987a;&#x5e8f;&#x73af;&#x89c6; \ze" ID="ID_187971701" CREATED="1454052801490" MODIFIED="1454052814011"/>
</node>
</node>
<node TEXT="no.78&#x8f6c;&#x4e49;&#x95ee;&#x9898;&#x5b57;&#x7b26;" ID="ID_1295341749" CREATED="1453702934439" MODIFIED="1454052946972"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      patterns/search-url.markdown
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x6b63;&#x5411;&#x67e5;&#x627e;&#x65f6;&#x8981;&#x8f6c;&#x4e49;/" ID="ID_692454531" CREATED="1454052910684" MODIFIED="1454052917888">
<node TEXT="/\Vhttp://vimdoc.net/search?q=/\\" ID="ID_1565575434" CREATED="1454052954989" MODIFIED="1454053002047"/>
<node TEXT="/\Vhttp:\/\/vimdoc.net/search?q=\/\\" ID="ID_131667088" CREATED="1454052954989" MODIFIED="1454053052117"/>
</node>
<node TEXT="&#x53cd;&#x5411;&#x67e5;&#x627e;&#x65f6;&#x8981;&#x8f6c;&#x4e49;?" ID="ID_1979083901" CREATED="1454052910684" MODIFIED="1454052926169">
<node TEXT="?http://vimdoc.net/search?q=/\\" ID="ID_1836307014" CREATED="1454053070130" MODIFIED="1454053094683"/>
<node TEXT="?http://vimdoc.net/search\?q=/\\" ID="ID_561250780" CREATED="1454053096997" MODIFIED="1454053101093"/>
</node>
<node TEXT="&#x6bcf;&#x6b21;&#x90fd;&#x8981;&#x8f6c;&#x4e49;&#x7b26;&#x53f7;\" ID="ID_1819211996" CREATED="1454053122909" MODIFIED="1454053131777">
<node TEXT="/\V/\Vhttp:\/\/vimdoc.net/search?q=\/\\\\" ID="ID_946869240" CREATED="1454053137810" MODIFIED="1454053153025"/>
<node TEXT="?http://vimdoc.net/search\?q=/\\\\" ID="ID_1081394407" CREATED="1454053096997" MODIFIED="1454053168547"/>
</node>
<node TEXT="&#x7528;&#x7f16;&#x7a0b;&#x7684;&#x65b9;&#x5f0f;&#x8f6c;&#x4e49;&#x5b57;&#x7b26;" ID="ID_35627609" CREATED="1454053179653" MODIFIED="1454053190576">
<node TEXT="escape({string},{chars})" ID="ID_1962299948" CREATED="1454053202484" MODIFIED="1454053224785"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h escape()
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="/\V&lt;C-r&gt;=escape(@u, getcmdtype().&apos;\&apos;" ID="ID_893721080" CREATED="1454053259397" MODIFIED="1454053320436"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h getcmdtype()
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.86" ID="ID_440015325" CREATED="1454053344788" MODIFIED="1454053351851"/>
<node TEXT="no.83 &#x67e5;&#x627e;&#x57df;&#x7ed3;&#x675f;&#x7b26;" ID="ID_1875242794" CREATED="1454053383848" MODIFIED="1454053417240">
<node TEXT="/vim/e" ID="ID_845113278" CREATED="1454053394168" MODIFIED="1454053409713"/>
</node>
</node>
</node>
<node TEXT="13-&#x67e5;&#x627e;" POSITION="right" ID="ID_1201608246" CREATED="1451538750107" MODIFIED="1451538754805">
<node TEXT="no.79&#x7ed3;&#x8bc6;&#x67e5;&#x627e;&#x547d;&#x4ee4;" ID="ID_1199722483" CREATED="1453702952184" MODIFIED="1453702952184">
<node TEXT="&#x6784;&#x9020;&#x6b63;&#x5219;&#x8868;&#x8fbe;&#x5f0f;" ID="ID_1316615837" CREATED="1454053462715" MODIFIED="1454053469806">
<node TEXT="&#x56de;&#x6eaf;&#x67e5;&#x627e;&#x6a21;&#x5f0f;" ID="ID_265506509" CREATED="1454053471340" MODIFIED="1454053486848"/>
<node TEXT="&#x5728;&#x547d;&#x4ee4;&#x884c;&#x7a97;&#x53e3;&#x4e2d;&#x7f16;&#x8f91;" ID="ID_536389863" CREATED="1454053488053" MODIFIED="1454053497239"/>
</node>
<node TEXT="/ ? n N" ID="ID_497918577" CREATED="1454053533943" MODIFIED="1454053594731"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h wrapscan
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.80&#x9ad8;&#x4eae;&#x67e5;&#x627e;&#x5339;&#x914d;" ID="ID_1727292859" CREATED="1453702952184" MODIFIED="1454053690564"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h :noh
    </p>
  </body>
</html>
</richcontent>
<node TEXT="se hls!" ID="ID_1988858026" CREATED="1454053667955" MODIFIED="1454053683572"/>
<node TEXT="nnoremap &lt;silent&gt;&lt;C-l&gt; &quot;&lt;C-u&gt;nohlsearch&lt;CR&gt;&lt;C-l&gt;" ID="ID_458547750" CREATED="1454053698164" MODIFIED="1454053756540"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h CTRL-L
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.81&#x5728;&#x6267;&#x884c;&#x67e5;&#x627e;&#x524d;&#x9884;&#x89c8;&#x7b2c;&#x4e00;&#x5904;&#x5339;&#x914d;" ID="ID_1234842245" CREATED="1453702952188" MODIFIED="1454053792546"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h incsearch
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x68c0;&#x67e5;&#x662f;&#x5426;&#x5b58;&#x5728;&#x4e00;&#x5904;&#x5339;&#x914d;" ID="ID_1144574715" CREATED="1454053841846" MODIFIED="1454053852531">
<node TEXT="/carrot&lt;Esc&gt;" ID="ID_1780801377" CREATED="1454053853591" MODIFIED="1454053863197"/>
</node>
<node TEXT="&#x6839;&#x636e;&#x9884;&#x89c8;&#x7ed3;&#x679c;&#x5bf9;&#x67e5;&#x627e;&#x57df;&#x81ea;&#x52a8;&#x8865;&#x5168;" ID="ID_258419424" CREATED="1454053865821" MODIFIED="1454053888125">
<node TEXT="/carr&lt;C-r&gt;&lt;C-w&gt;" ID="ID_191457241" CREATED="1454053893996" MODIFIED="1454053918630"/>
<node TEXT="\v&#x6a21;&#x5f0f;&#x4e0b;&#x7a0d;&#x6709;&#x7455;&#x75b5;" ID="ID_539037409" CREATED="1454053960172" MODIFIED="1454053970437">
<node TEXT="/\vcarrcarrot" ID="ID_342561316" CREATED="1454053980864" MODIFIED="1454053996505"/>
</node>
</node>
</node>
<node TEXT="no.82&#x7edf;&#x8ba1;&#x5f53;&#x524d;&#x6a21;&#x5f0f;&#x7684;&#x5339;&#x914d;&#x4e2a;&#x6570;" ID="ID_120400142" CREATED="1453702952189" MODIFIED="1453702952189">
<node TEXT=":%s///gn" ID="ID_1827967662" CREATED="1454054009741" MODIFIED="1454054017978">
<node TEXT="n&#x4f1a;&#x6291;&#x5236;&#x66ff;&#x6362;&#x64cd;&#x4f5c;, &#x53ea;&#x7b80;&#x5355;&#x7edf;&#x8ba1;&#x5339;&#x914d;&#x6b21;&#x6570;" ID="ID_640898861" CREATED="1454054043802" MODIFIED="1454054079741"/>
</node>
</node>
<node TEXT="no.83&#x5c06;&#x5149;&#x6807;&#x504f;&#x79fb;&#x5230;&#x67e5;&#x627e;&#x5339;&#x914d;&#x8fdb;&#x884c;&#x64cd;&#x4f5c;" ID="ID_600351041" CREATED="1453702952194" MODIFIED="1454054116756"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      search/langs.txt
    </p>
    <p>
      :h search-offset
    </p>
  </body>
</html>
</richcontent>
<node TEXT="/lang/e&lt;CR&gt; auage&lt;Esc&gt; n. n." ID="ID_614840104" CREATED="1454054150127" MODIFIED="1454054186891"/>
<node TEXT="//e" ID="ID_921669792" CREATED="1454054196129" MODIFIED="1454054201818">
<node TEXT="&#x91cd;&#x7528;&#x67e5;&#x627e;&#x6a21;&#x5f0f;, &#x5e76;&#x504f;&#x79fb;&#x81f3;&#x8bcd;&#x5c3e;" ID="ID_204425220" CREATED="1454054205641" MODIFIED="1454054216400"/>
</node>
</node>
<node TEXT="no.84&#x5bf9;&#x5b8c;&#x6574;&#x7684;&#x67e5;&#x627e;&#x5339;&#x914d;&#x8fdb;&#x884c;&#x64cd;&#x4f5c;" ID="ID_1971798214" CREATED="1453702952197" MODIFIED="1454054252749"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      search/tag-heirarchy.rb
    </p>
  </body>
</html>
</richcontent>
<node TEXT="gU{motion} gU3l 3gUl gUtD" ID="ID_174172959" CREATED="1454054262292" MODIFIED="1454054285754"/>
<node ID="ID_464808086" CREATED="1454054295908" MODIFIED="1454054370640"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      /\vX(ht)?ml\C&lt;CR&gt; <b>gU//e&lt;CR&gt;</b>&#160;//&lt;CR&gt; . //&lt;CR&gt;.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="gUfl" ID="ID_1499792007" CREATED="1454054389506" MODIFIED="1454054414750"/>
<node TEXT="plugin: Kana Natsuno: textobj-lastpat" ID="ID_129044549" CREATED="1454054439175" MODIFIED="1454054531132"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      http://github.com/kana/vim-textobj-lastpat
    </p>
  </body>
</html>
</richcontent>
<node TEXT="i/&#x6587;&#x672c;&#x5bf9;&#x8c61;&#x7528;&#x4e8e;&#x64cd;&#x4f5c;&#x67e5;&#x627e;&#x5339;&#x914d;" ID="ID_562543465" CREATED="1454054547021" MODIFIED="1454054560295"/>
<node TEXT="gUi/" ID="ID_558751603" CREATED="1454054533648" MODIFIED="1454054538851"/>
</node>
</node>
<node TEXT="no.85&#x5229;&#x7528;&#x67e5;&#x627e;&#x5386;&#x53f2;,&#x8fed;&#x4ee3;&#x5b8c;&#x6210;&#x590d;&#x6742;&#x7684;&#x6a21;&#x5f0f;" ID="ID_1877616957" CREATED="1453702952199" MODIFIED="1454054744944"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      search/quoted-strings.txt
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x7c97;&#x7565;&#x5339;&#x914d;" ID="ID_1454411377" CREATED="1454054772002" MODIFIED="1454054779722">
<node TEXT="/\v&apos;.+&apos;" ID="ID_346062058" CREATED="1454054883384" MODIFIED="1454054895750"/>
</node>
<node TEXT="&#x9010;&#x6b65;&#x6c42;&#x7cbe;" ID="ID_17139291" CREATED="1454054780439" MODIFIED="1454054785653">
<node TEXT="/&lt;Up&gt;" ID="ID_1335139854" CREATED="1454055106606" MODIFIED="1454055120671"/>
<node TEXT="/\v&apos;[^&apos;].+&apos;" ID="ID_51646272" CREATED="1454054883384" MODIFIED="1454054918991"/>
</node>
<node TEXT="&#x7cbe;&#x76ca;&#x6c42;&#x7cbe;" ID="ID_1246629560" CREATED="1454054787762" MODIFIED="1454054791550">
<node TEXT="q/" ID="ID_1478091370" CREATED="1454055102436" MODIFIED="1454055105393"/>
<node TEXT="/\v&apos;([^&apos;]|&apos;\w)+&apos;" ID="ID_1943590686" CREATED="1454054926597" MODIFIED="1454054961307"/>
</node>
<node TEXT="&#x753b;&#x4e0a;&#x5b8c;&#x7f8e;&#x53e5;&#x53f7;" ID="ID_1312140306" CREATED="1454055131920" MODIFIED="1454055139155">
<node TEXT=":%s//&quot;\1&quot;/g" ID="ID_116036240" CREATED="1454055141203" MODIFIED="1454055167454"/>
</node>
</node>
<node TEXT="no.86&#x67e5;&#x627e;&#x5f53;&#x524d;&#x9ad8;&#x4eae;&#x9009;&#x4e2d;&#x7684;&#x6587;&#x672c;" ID="ID_539726607" CREATED="1453702952200" MODIFIED="1454055413705"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h visual-search
    </p>
    <p>
      patterns/visual-star.vim
    </p>
  </body>
</html>
</richcontent>
<node TEXT="plugin visual star search" ID="ID_413762023" CREATED="1454055305726" MODIFIED="1454055351570"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      https://github.com/nelstrom/vim-visual-star-search
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="xnoremap" ID="ID_42333870" CREATED="1454055374683" MODIFIED="1454055388324"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h mapmode-x
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="14-&#x66ff;&#x6362;" POSITION="right" ID="ID_151211303" CREATED="1451538755584" MODIFIED="1451538761318">
<node TEXT="no.87&#x7ed3;&#x8bc6;substitute&#x547d;&#x4ee4;" ID="ID_246545772" CREATED="1453702971309" MODIFIED="1454305530484">
<node TEXT=":substitute" ID="ID_868753303" CREATED="1455503861353" MODIFIED="1455503871842">
<node TEXT=":[range]s[ubstitute]/{pattern}/{string}/[flags]" ID="ID_1109065042" CREATED="1455503885250" MODIFIED="1455503915777"/>
</node>
<node TEXT="ranges" ID="ID_813850004" CREATED="1455503984434" MODIFIED="1455503986593">
<node TEXT="no.28" ID="ID_1503861241" CREATED="1455503980074" MODIFIED="1455503983330"/>
</node>
<node TEXT="pattern" ID="ID_477774491" CREATED="1455503988839" MODIFIED="1455503991113">
<node TEXT="chapter.12" ID="ID_63562113" CREATED="1455503932398" MODIFIED="1455503996379"/>
</node>
<node TEXT="flags" ID="ID_387975245" CREATED="1455503972648" MODIFIED="1455503975073">
<node TEXT="g" ID="ID_1601820474" CREATED="1455503999167" MODIFIED="1455504001466">
<node TEXT="&#x5f53;&#x524d;&#x4e00;&#x6574;&#x884c;" ID="ID_1166017845" CREATED="1455504018672" MODIFIED="1455505626314"/>
</node>
<node TEXT="c" ID="ID_1649293945" CREATED="1455504002113" MODIFIED="1455504003925">
<node TEXT="&#x786e;&#x8ba4;&#x6216;&#x62d2;&#x7edd;" ID="ID_1776736058" CREATED="1455504027784" MODIFIED="1455504034892"/>
</node>
<node TEXT="n" ID="ID_152940827" CREATED="1455504004527" MODIFIED="1455504006368">
<node TEXT="&#x6291;&#x5236;&#x66ff;&#x6362;&#x884c;&#x4e3a;, &#x4ec5;&#x663e;&#x793a;&#x5339;&#x914d;&#x4e2a;&#x6570;" ID="ID_495768091" CREATED="1455504037364" MODIFIED="1455504061685"/>
</node>
<node TEXT="e" ID="ID_814595204" CREATED="1455504007160" MODIFIED="1455504014436">
<node TEXT="&#x5c4f;&#x853d;&#x9519;&#x8bef;&#x63d0;&#x793a;" ID="ID_188187649" CREATED="1455504064220" MODIFIED="1455504074877"/>
</node>
<node TEXT="&amp;" ID="ID_1270002084" CREATED="1455504015056" MODIFIED="1455504016201">
<node TEXT="&#x6307;&#x793a;Vim&#x91cd;&#x7528;&#x4e0a;&#x4e00;&#x6b21;substitute&#x547d;&#x4ee4;&#x7528;&#x8fc7;&#x7684;&#x6807;&#x5fd7;&#x4f4d;" ID="ID_1110952804" CREATED="1455504077327" MODIFIED="1455504103983"/>
</node>
</node>
<node TEXT="&#x7279;&#x6b8a;&#x5b57;&#x7b26;" ID="ID_658905518" CREATED="1455504111620" MODIFIED="1455504114753">
<node TEXT="\r" ID="ID_1004312019" CREATED="1455504116021" MODIFIED="1455504120554">
<node TEXT="&#x6362;&#x884c;&#x7b26;" ID="ID_1228170545" CREATED="1455504158335" MODIFIED="1455504162561"/>
</node>
<node TEXT="\t" ID="ID_1218683870" CREATED="1455504121338" MODIFIED="1455504123219">
<node TEXT="&#x5236;&#x8868;&#x7b26;" ID="ID_1176397160" CREATED="1455504164042" MODIFIED="1455504166830"/>
</node>
<node TEXT="\\" ID="ID_210367614" CREATED="1455504123994" MODIFIED="1455504126017">
<node TEXT="\" ID="ID_855143837" CREATED="1455504168453" MODIFIED="1455504170108"/>
</node>
<node TEXT="\1" ID="ID_783098683" CREATED="1455504127936" MODIFIED="1455504129697">
<node TEXT="&#x7b2c;&#x4e00;&#x4e2a;&#x5b50;&#x5339;&#x914d;" ID="ID_751710603" CREATED="1455504174283" MODIFIED="1455504182033">
<node TEXT="no.93" ID="ID_324182094" CREATED="1455504295133" MODIFIED="1455504322358"/>
</node>
</node>
<node TEXT="\2" ID="ID_711674110" CREATED="1455504130301" MODIFIED="1455504132543">
<node TEXT="&#x7b2c;&#x4e8c;&#x4e2a;&#x5b50;&#x5339;&#x914d;" ID="ID_810208537" CREATED="1455504174283" MODIFIED="1455504187703"/>
</node>
<node TEXT="\0" ID="ID_1925733365" CREATED="1455504133413" MODIFIED="1455504135472">
<node TEXT="&#x5339;&#x914d;&#x6a21;&#x5f0f;&#x7684;&#x6240;&#x6709;&#x5185;&#x5bb9;" ID="ID_1635931876" CREATED="1455504190080" MODIFIED="1455504198554"/>
</node>
<node TEXT="&amp;" ID="ID_1667211971" CREATED="1455504136137" MODIFIED="1455504138080">
<node TEXT="&#x5339;&#x914d;&#x6a21;&#x5f0f;&#x7684;&#x6240;&#x6709;&#x5185;&#x5bb9;" ID="ID_1260748185" CREATED="1455504190080" MODIFIED="1455504198554"/>
</node>
<node TEXT="~" ID="ID_1118766014" CREATED="1455504139120" MODIFIED="1455504141951">
<node TEXT="&#x4f7f;&#x7528;&#x4e0a;&#x4e00;&#x6b21;&#x8c03;&#x7528;:substitute&#x65f6;&#x7684;{string}" ID="ID_117858309" CREATED="1455504208999" MODIFIED="1455504231510">
<node TEXT="no.92" ID="ID_316862942" CREATED="1455504310161" MODIFIED="1455504319430"/>
</node>
</node>
<node TEXT="\={Vim script}" ID="ID_17284587" CREATED="1455504142847" MODIFIED="1455504152577">
<node TEXT="&#x6267;&#x884c;{Vim Script}&#x8868;&#x8fbe;&#x5f0f;; &#x5e76;&#x5c06;&#x8fd4;&#x56de;&#x7684;&#x7ed3;&#x679c;&#x4f5c;&#x4e3a;&#x66ff;&#x6362;{string}" ID="ID_1390662065" CREATED="1455504233479" MODIFIED="1455504265844">
<node TEXT="no.94, no.95" ID="ID_648080774" CREATED="1455504278975" MODIFIED="1455504285367"/>
</node>
</node>
</node>
</node>
<node TEXT="no.88&#x5728;&#x6587;&#x4ef6;&#x8303;&#x56f4;&#x5185;&#x67e5;&#x627e;&#x5e76;&#x66ff;&#x6362;&#x6bcf;&#x4e00;&#x5904;&#x5339;&#x914d;" ID="ID_218852454" CREATED="1453702971309" MODIFIED="1455504373928"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      substitution/get-rolling.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":set hlsearch&lt;CR&gt;" ID="ID_1560353738" CREATED="1455504382017" MODIFIED="1455504450189"/>
<node ID="ID_1582760015" CREATED="1455504452691" MODIFIED="1455504452691"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :s/going/rolling
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT=":s/going/rolling/g" ID="ID_472256812" CREATED="1455504432668" MODIFIED="1455504446742"/>
<node TEXT=":%s/going/rolling/g" ID="ID_1550034756" CREATED="1455504458885" MODIFIED="1455504470586"/>
</node>
<node TEXT="no.89&#x624b;&#x52a8;&#x63a7;&#x5236;&#x6bcf;&#x4e00;&#x6b21;&#x66ff;&#x6362;&#x64cd;&#x4f5c;" ID="ID_1750871704" CREATED="1453702971314" MODIFIED="1455505662380"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the_vim_way/1_copy_content.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":%s/content/copy/gc" ID="ID_335511955" CREATED="1455505641630" MODIFIED="1455505683560"/>
<node TEXT="y/n/a/q/l/^E/^Y" ID="ID_662550965" CREATED="1455505690177" MODIFIED="1455505712310">
<node TEXT="y for yes" ID="ID_1166318672" CREATED="1455505717329" MODIFIED="1455505722581"/>
<node TEXT="n for no" ID="ID_1152530183" CREATED="1455505723536" MODIFIED="1455505726804"/>
<node TEXT="a for all" ID="ID_1553758223" CREATED="1455505727445" MODIFIED="1455505730502"/>
<node TEXT="q for quit" ID="ID_118614356" CREATED="1455505731144" MODIFIED="1455505736431"/>
<node TEXT="l for last" ID="ID_1246298193" CREATED="1455505737852" MODIFIED="1455505752551"/>
<node TEXT="&lt;C-e&gt; &#x5411;&#x4e0a;&#x6eda;&#x52a8;&#x5c4f;&#x5e55;" ID="ID_1304963847" CREATED="1455505741371" MODIFIED="1455505766643"/>
<node TEXT="&lt;C-y&gt; &#x5411;&#x4e0b;&#x6eda;&#x52a8;&#x5c4f;&#x5e55;" ID="ID_743314233" CREATED="1455505767591" MODIFIED="1455505775950"/>
</node>
<node TEXT=":h :s_c" ID="ID_681556929" CREATED="1455505780224" MODIFIED="1455505787461"/>
<node TEXT="&#x5bf9;&#x6bd4;&#x70b9;&#x8303;&#x5f0f;" ID="ID_73772874" CREATED="1455505819263" MODIFIED="1455505843970"/>
</node>
<node TEXT="no.90&#x91cd;&#x7528;&#x4e0a;&#x6b21;&#x7684;&#x67e5;&#x627e;&#x6a21;&#x5f0f;" ID="ID_249508743" CREATED="1453702971317" MODIFIED="1453702971317">
<node TEXT="no.85" ID="ID_1564540911" CREATED="1455505919454" MODIFIED="1455505923101">
<node TEXT=":%s/\v&apos;(([^&apos;]|&apos;\w)+&apos;/&quot;\1&quot;/g" ID="ID_923442507" CREATED="1455505924445" MODIFIED="1455505965413">
<node TEXT="/\v&apos;(([^&apos;]|&apos;\w)+)&apos;" ID="ID_1385850530" CREATED="1455505967471" MODIFIED="1455506016290"/>
<node TEXT=":%s//&quot;\1&quot;/g" ID="ID_1430219797" CREATED="1455506002904" MODIFIED="1455506029671"/>
</node>
</node>
<node TEXT="no.86" ID="ID_1270910693" CREATED="1455506086141" MODIFIED="1455506089998">
<node TEXT="*" ID="ID_118557340" CREATED="1455506091012" MODIFIED="1455506092178"/>
</node>
<node TEXT=":h cmdline-history" ID="ID_1372493209" CREATED="1455506140856" MODIFIED="1455506153030">
<node TEXT=":%s/&lt;C-r&gt;//&quot;\1&quot;/g" ID="ID_1348076292" CREATED="1455506104711" MODIFIED="1455506121430"/>
</node>
</node>
<node TEXT="no.91&#x7528;&#x5bc4;&#x5b58;&#x5668;&#x7684;&#x5185;&#x5bb9;&#x66ff;&#x6362;" ID="ID_250560231" CREATED="1453702971318" MODIFIED="1453702971318">
<node TEXT="&#x4f20;&#x503c;" ID="ID_1281122428" CREATED="1455506478947" MODIFIED="1455506484438">
<node TEXT=":%s//&lt;C-r&gt;0/g" ID="ID_1992047303" CREATED="1455506191664" MODIFIED="1455506203734">
<node TEXT="&#x6ce8;&#x610f;&#x8981;&#x8f6c;&#x4e49;&#x5176;&#x4e2d;&#x7684;&amp;&#x548c;~" ID="ID_374709667" CREATED="1455506217733" MODIFIED="1455506229477"/>
</node>
</node>
<node TEXT="&#x5f15;&#x7528;" ID="ID_1913445608" CREATED="1455506472672" MODIFIED="1455506487099">
<node TEXT=":%s//\=@0/g" ID="ID_1616102700" CREATED="1455506489073" MODIFIED="1455506498455"/>
<node TEXT=":%s//\=@&quot;/g" ID="ID_1602164397" CREATED="1455506489073" MODIFIED="1455506512540"/>
</node>
<node TEXT="&#x6bd4;&#x8f83;" ID="ID_1364473493" CREATED="1455506523073" MODIFIED="1455506524956">
<node TEXT=":%s/Pragmatic Vim/Practical Vim/g" ID="ID_267941671" CREATED="1455506550089" MODIFIED="1455506567417"/>
<node TEXT=":let @/=&apos;Pragmatic Vim&apos;&#xa;:let @a=&apos;Practical Vim&apos;&#xa;:%s//\=@a/g" ID="ID_1897853107" CREATED="1455506572321" MODIFIED="1455506618071"/>
</node>
</node>
<node TEXT="no.92&#x91cd;&#x590d;&#x4e0a;&#x4e00;&#x6b21;substitute&#x547d;&#x4ee4;" ID="ID_730829289" CREATED="1453702971319" MODIFIED="1455507041674"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      substitution/mixin.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":h g&amp;" ID="ID_1550861138" CREATED="1455507011140" MODIFIED="1455507022534">
<node TEXT=":s/target/replacement/g" ID="ID_1948705501" CREATED="1455506973767" MODIFIED="1455506989733">
<node TEXT="g&amp;" ID="ID_901568256" CREATED="1455506998443" MODIFIED="1455507000791"/>
</node>
</node>
<node TEXT="gv" ID="ID_551564219" CREATED="1455507045043" MODIFIED="1455507087638">
<node TEXT="no.21" ID="ID_697434224" CREATED="1455507088713" MODIFIED="1455507093567"/>
</node>
<node TEXT="&#x9519;&#x8bef;&#x66ff;&#x6362;" ID="ID_1827774731" CREATED="1455507181646" MODIFIED="1455507185246">
<node TEXT="Vjj yP :%s/Name/Number/g" ID="ID_1123375140" CREATED="1455507154008" MODIFIED="1455507180408"/>
</node>
<node TEXT="&#x7ed3;&#x8bba;" ID="ID_656253941" CREATED="1455507186428" MODIFIED="1455507382766">
<node TEXT=":&amp;&amp;" ID="ID_278453354" CREATED="1455507193546" MODIFIED="1455507203548">
<node TEXT=":&amp;&#x7528;&#x4f5c;&#x91cd;&#x590d;&#x4e0a;&#x4e00;&#x6b21;&#x7684;:substitute&#x547d;&#x4ee4;" ID="ID_1111200311" CREATED="1455507205185" MODIFIED="1455507222644">
<node TEXT=":h :&amp;" ID="ID_610174112" CREATED="1455507249572" MODIFIED="1455507252570"/>
</node>
<node TEXT="&amp;&#x91cd;&#x7528;&#x4e0a;&#x4e00;&#x6b21;:s&#x547d;&#x4ee4;&#x7684;&#x6807;&#x5fd7;&#x4f4d;" ID="ID_494713804" CREATED="1455507224768" MODIFIED="1455507241300"/>
<node TEXT="&#x8be5;&#x547d;&#x4ee4;&#x4ec5;&#x4f5c;&#x7528;&#x4e8e;&#x5f53;&#x524d;&#x884c;" ID="ID_365863091" CREATED="1455507273102" MODIFIED="1455507280391"/>
</node>
<node TEXT=":&apos;&lt;,&apos;&gt;&amp;&amp;" ID="ID_410461974" CREATED="1455507258078" MODIFIED="1455507289198">
<node TEXT="&#x4f5c;&#x7528;&#x4e8e;&#x9009;&#x533a;" ID="ID_194564008" CREATED="1455507290331" MODIFIED="1455507296917"/>
</node>
<node TEXT=":%&amp;&amp;" ID="ID_899094456" CREATED="1455507298474" MODIFIED="1455507301025">
<node TEXT="&#x4f5c;&#x7528;&#x4e8e;&#x5168;&#x5c40;&#x6587;&#x672c;, &#x5feb;&#x6377;&#x952e;&#x4e3a;g&amp;" ID="ID_43646526" CREATED="1455507301891" MODIFIED="1455507338204"/>
</node>
</node>
<node TEXT="&#x4fee;&#x6b63;&amp;&#x547d;&#x4ee4;" ID="ID_1311465334" CREATED="1455507384232" MODIFIED="1455507387915">
<node TEXT="&amp;&#x547d;&#x4ee4;&#x4e3a;:s&#x7684;&#x540c;&#x4e49;&#x8bcd;, &#x7528;&#x4e8e;&#x91cd;&#x590d;&#x4e0a;&#x4e00;&#x6b21;&#x7684;&#x66ff;&#x6362;&#x64cd;&#x4f5c;;&#xa;&#x4e0d;&#x8db3;&#x4e4b;&#x5904;&#x5728;&#x4e8e;,&#x4e0d;&#x8bba;&#x6211;&#x4eec;&#x7528;&#x4ec0;&#x4e48;&#x6807;&#x5fd7;&#x4f4d;,&amp;&#x547d;&#x4ee4;&#x90fd;&#x5c06;&#xa;&#x5ffd;&#x7565;&#x5b83;&#x4eec;,&#x8fd9;&#x610f;&#x5473;&#x7740;&#x672c;&#x6b21;&#x66ff;&#x6362;&#x7684;&#x7ed3;&#x679c;&#x53ef;&#x80fd;&#x8ddf;&#x4e0a;&#x4e00;&#x6b21;&#xa;&#x622a;&#x7136;&#x4e0d;&#x540c;" ID="ID_1363435570" CREATED="1455507392644" MODIFIED="1455507601359"/>
<node TEXT="&#x8ba9;&amp;&#x6765;&#x89e6;&#x53d1;:&amp;&amp;&#x547d;&#x4ee4;&#x4f1a;&#x66f4;&#x6709;&#x7528;, &#x56e0;&#x4e3a;&#x540e;&#x8005;&#x4f1a;&#x4fdd;&#x7559;&#x6807;&#x5fd7;&#x4f4d;,&#xa;&#x4ece;&#x800c;&#x4f7f;&#x5f97;&#x6267;&#x884c;&#x547d;&#x4ee4;&#x7684;&#x7ed3;&#x679c;&#x59cb;&#x7ec8;&#x5982;&#x4e00;" ID="ID_1793659100" CREATED="1455507439145" MODIFIED="1455507587500"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      nnoremap &amp; :&amp;&amp;&lt;CR&gt;
    </p>
    <p>
      xnoremap &amp; :&amp;&amp;&lt;CR&gt;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="no.93&#x4f7f;&#x7528;&#x5b50;&#x5339;&#x914d;&#x91cd;&#x6392;CSV&#x6587;&#x4ef6;&#x7684;&#x5b57;&#x6bb5;" ID="ID_131771230" CREATED="1453702971320" MODIFIED="1455507643402"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      substitution/subscribers.csv
    </p>
  </body>
</html>

</richcontent>
<node TEXT="/\v^([^,]),([^,]*),([^,]*)$&#xa;:%s//\3,\2,\1" ID="ID_774419601" CREATED="1455507648572" MODIFIED="1455507728527"/>
</node>
<node TEXT="no.94&#x5728;&#x66ff;&#x6362;&#x8fc7;&#x7a0b;&#x4e2d;&#x6267;&#x884c;&#x7b97;&#x672f;&#x8fd0;&#x7b97;" ID="ID_1832616155" CREATED="1453702971321" MODIFIED="1455508028486"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      substitution/headings.html
    </p>
  </body>
</html>

</richcontent>
<node TEXT="/\v\&lt;\/?h\zs\d&#xa;:%s//\=submatch(0)-1/g" ID="ID_357886102" CREATED="1455508029776" MODIFIED="1455508076085">
<node TEXT="no.77" ID="ID_1897821960" CREATED="1455508104422" MODIFIED="1455508108405"/>
</node>
</node>
<node TEXT="no.95&#x4ea4;&#x6362;&#x4e24;&#x4e2a;&#x6216;&#x66f4;&#x591a;&#x7684;&#x5355;&#x8bcd;" ID="ID_1302887935" CREATED="1453702971322" MODIFIED="1455508176729"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      substitution/who-bites.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x96be;&#x4ee5;&#x594f;&#x6548;" ID="ID_739446267" CREATED="1455508258630" MODIFIED="1455508265547">
<node TEXT=":%s/dog/man/g&#xa;:%s/man/dog/g" ID="ID_1461651564" CREATED="1455508236779" MODIFIED="1455508251445"/>
</node>
<node TEXT="&#x8fd4;&#x56de;&#x53e6;&#x4e00;&#x4e2a;&#x5355;&#x8bcd;" ID="ID_1210994971" CREATED="1455508267137" MODIFIED="1455508274990">
<node TEXT=":let swapper={&quot;dog&quot;:&quot;man&quot;,&quot;man&quot;:&quot;dog&quot;}&#xa;:echo swapper[&quot;dog&quot;]&#xa;:echo swapper[&quot;man&quot;]" ID="ID_743194440" CREATED="1455508276040" MODIFIED="1455508334835"/>
</node>
<node TEXT="&#x5339;&#x914d;&#x4e24;&#x4e2a;&#x5355;&#x8bcd;" ID="ID_1200783470" CREATED="1455508347671" MODIFIED="1455508352388">
<node TEXT="/\v(&lt;man&gt;|&lt;dog&gt;)" ID="ID_440230798" CREATED="1455508353653" MODIFIED="1455508367561"/>
</node>
<node TEXT="&#x5408;&#x800c;&#x4e3a;&#x4e00;" ID="ID_401560670" CREATED="1455508372538" MODIFIED="1455508380610">
<node TEXT="/\v(&lt;man&gt;|&lt;dog&gt;)" ID="ID_509501901" CREATED="1455508353653" MODIFIED="1455508367561"/>
<node TEXT=":%s//\={&quot;dog&quot;:&quot;man&quot;,&quot;man&quot;:&quot;dog&quot;}[submatch(1)]/g" ID="ID_879468383" CREATED="1455508392370" MODIFIED="1455508433598"/>
</node>
<node TEXT="&#x7ed3;&#x8bba;" ID="ID_1952991482" CREATED="1455508435621" MODIFIED="1455508441615">
<node TEXT="&#x5f97;&#x4e0d;&#x507f;&#x5931;" ID="ID_1865915392" CREATED="1455508442644" MODIFIED="1455508459481"/>
<node TEXT="plugin Abolish.vim tim pope: https://github.com/tpope/vim-abolish" ID="ID_875932238" CREATED="1455508471965" MODIFIED="1455508516478">
<node TEXT=":%S/{man,dog}/{dog,man}/g" ID="ID_1891238616" CREATED="1455508531009" MODIFIED="1455508548916"/>
</node>
</node>
</node>
<node TEXT="no.96&#x5728;&#x591a;&#x4e2a;&#x6587;&#x4ef6;&#x4e2d;&#x6267;&#x884c;&#x67e5;&#x627e;&#x4e0e;&#x66ff;&#x6362;" ID="ID_342734961" CREATED="1453702971324" MODIFIED="1455508620647"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      refactor-project/*.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x66ff;&#x6362;&#x547d;&#x4ee4;" ID="ID_769931142" CREATED="1455508834987" MODIFIED="1455508840265">
<node TEXT="/Pragmatic\ze Vim&#xa;:%s//Practical/g" ID="ID_1935669485" CREATED="1455508622186" MODIFIED="1455508672395"/>
</node>
<node TEXT="&#x6240;&#x6709;&#x6587;&#x4ef6;&#x4e2d;&#x6267;&#x884c;&#x67e5;&#x627e;&#x66ff;&#x6362;" ID="ID_1365425984" CREATED="1455508819641" MODIFIED="1455508828520">
<node TEXT=":args **/*.txt&#xa;:set hidden&#xa;:argdo %s//Practical/g[e]" ID="ID_635979258" CREATED="1455508683004" MODIFIED="1455508782267">
<node TEXT="&#x547d;&#x4e2d;&#x7387;&#x8f83;&#x4f4e;(&#x6563;&#x5c04;(scatter-shot))&#x6cd5;" ID="ID_434671834" CREATED="1455508741423" MODIFIED="1455508776021"/>
</node>
</node>
<node TEXT="&#x66ff;&#x4ee3;&#x65b9;&#x6848;quickfixdo" ID="ID_737758069" CREATED="1455528622651" MODIFIED="1455528664898"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      substitution/qargs.vim
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":set hidden&#xa;/Pragmatic\ze Vim&#xa;:vimgrep /&lt;C-r&gt;// **/*.txt&#xa;:Qargs&#xa;:argdo %s//Practical/g&#xa;:argdo update" ID="ID_1167217476" CREATED="1455528793301" MODIFIED="1455529080695">
<node TEXT=":copen&#xa;plugin: https://github.com/nelstrom/vim-qargs" ID="ID_1761946084" CREATED="1455528731328" MODIFIED="1455528990840"/>
</node>
</node>
<node TEXT="Tips |" ID="ID_1890212471" CREATED="1455529042787" MODIFIED="1455529050513">
<node TEXT=":Qargs | argdo %s//Practical/g | update" ID="ID_1282664850" CREATED="1455529051775" MODIFIED="1455529087180">
<node TEXT="&#x8fd9;&#x91cc;&#x7684; | &#x975e;&#x7ba1;&#x9053;, &#x800c;&#x662f;&#x7c7b;&#x4f3c;&#x4e8e;shell&#x4e2d;&#x7684;;&#x53f7;" ID="ID_1385980515" CREATED="1455529098242" MODIFIED="1455529117590"/>
</node>
<node TEXT=":h update" ID="ID_554273258" CREATED="1455529121554" MODIFIED="1455529129490"/>
<node TEXT=":h :bar" ID="ID_115445205" CREATED="1455529130150" MODIFIED="1455529138137"/>
</node>
</node>
</node>
<node TEXT="15-global&#x547d;&#x4ee4;" POSITION="right" ID="ID_1804235429" CREATED="1451538771685" MODIFIED="1451538776675">
<node TEXT="no.97&#x7ed3;&#x8bc6;global&#x547d;&#x4ee4;" ID="ID_1659798493" CREATED="1453702989480" MODIFIED="1453702989480">
<node TEXT=":h :g" ID="ID_157531407" CREATED="1455529154525" MODIFIED="1455529165476">
<node TEXT=":[range] global[!] /{pattern}/ [cmd]" ID="ID_1271209610" CREATED="1455529166761" MODIFIED="1455529191141">
<node TEXT="&#x7f3a;&#x7701;&#x60c5;&#x51b5;&#x4e0b;, :global&#x547d;&#x4ee4;&#x4f5c;&#x7528;&#x4e8e;&#x6574;&#x4e2a;&#x6587;&#x4ef6;(%), &#x8fd9;&#x4e8e;&#x5176;&#x4ed6;&#x5927;&#x591a;&#x6570;Ex&#x547d;&#x4ee4;&#x6709;&#x6240;&#x4e0d;&#x540c;" ID="ID_451559376" CREATED="1455529207151" MODIFIED="1455529243804"/>
<node TEXT="{pattern}&#x57df;&#x4e0e;&#x67e5;&#x627e;&#x5386;&#x53f2;&#x76f8;&#x5173;&#x8054;" ID="ID_1669943768" CREATED="1455529262654" MODIFIED="1455529273685"/>
<node TEXT="{cmd}&#x53ef;&#x4ee5;&#x662f;&#x9664;global&#x5916;&#x7684;&#x6240;&#x6709;&#x547d;&#x4ee4;, &#x53c2;&#x89c1;&#x8868;15-1, &#x82e5;&#x4e0d;&#x6307;&#x5b9a;{cmd}, &#x7f3a;&#x7701;&#x4f7f;&#x7528;:print" ID="ID_851525711" CREATED="1455529293344" MODIFIED="1455529351928"/>
<node TEXT=":global!&#x6216;&#x8005;:vglobal&#x8868;&#x793a;&#x53cd;&#x8f6c;" ID="ID_1515418665" CREATED="1455529368969" MODIFIED="1455529395081"/>
<node TEXT=":global&#x547d;&#x4ee4;&#x5728;&#x6307;&#x5b9a;[range]&#x5185;&#x7684;&#x6587;&#x672c;&#x884c;&#x4e0a;&#x6267;&#x884c;&#x65f6;&#x901a;&#x5e38;&#x5206;&#x4e3a;&#x4e24;&#x8f6e;;&#xa;&#x7b2c;&#x4e00;&#x8f6e;,Vim&#x4f1a;&#x5728;&#x6240;&#x6709;[pattern]&#x7684;&#x5339;&#x914d;&#x884c;&#x4e0a;&#x505a;&#x6807;&#x8bb0;;&#xa;&#x7b2c;&#x4e8c;&#x8f6e;,&#x518d;&#x5728;&#x6240;&#x6709;&#x5df2;&#x6807;&#x8bb0;&#x7684;&#x6587;&#x672c;&#x884c;&#x4e0a;&#x6267;&#x884c;[cmd];" ID="ID_1857231654" CREATED="1455529410425" MODIFIED="1455529529771"/>
<node ID="ID_552821637" CREATED="1455529531475" MODIFIED="1455529531475"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21478;&#22806;,&#30001;&#20110;[cmd]&#30340;&#33539;&#22260;&#21487;&#20197;&#21333;&#29420;&#35774;&#23450;, &#22240;&#27492;&#21487;&#22312;&#22810;&#34892;&#25991;&#26412;&#27573;&#20869;&#36827;&#34892;&#25805;&#20316;
    </p>
  </body>
</html>

</richcontent>
<node TEXT="no.100" ID="ID_1293745" CREATED="1455529542280" MODIFIED="1455529546547"/>
</node>
</node>
</node>
</node>
<node TEXT="no.98&#x5220;&#x9664;&#x6240;&#x6709;&#x5305;&#x542b;&#x6a21;&#x5f0f;&#x7684;&#x6587;&#x672c;&#x884c;" ID="ID_544188297" CREATED="1453702989480" MODIFIED="1455529568515"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      global/episodes.html
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x7528;&apos;:g/re/d&apos;&#x5220;&#x9664;&#x6240;&#x6709;&#x7684;&#x5339;&#x914d;&#x884c;" ID="ID_849758877" CREATED="1455529577720" MODIFIED="1455529785132"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      grep: g for global, re for regular expression, p&#34920;&#31034;print
    </p>
  </body>
</html>

</richcontent>
<node TEXT="/\v\&lt;\/?\w+&gt;&#xa;:g//d" ID="ID_1947046335" CREATED="1455529604757" MODIFIED="1455529639672"/>
<node TEXT="&#x4e0e;:s&#x7c7b;&#x4f3c;, &#x7559;&#x7a7a;&#x67e5;&#x627e;&#x57df;&#x53ef;&#x91cd;&#x7528;&#x4e0a;&#x6b21;&#x67e5;&#x627e;" ID="ID_1346942596" CREATED="1455529706285" MODIFIED="1455529729552"/>
</node>
<node TEXT="&#x53ea;&#x4fdd;&#x7559;&#x5339;&#x914d;&#x884c;" ID="ID_1497451769" CREATED="1455529822053" MODIFIED="1455529827176">
<node TEXT="&#x7528;&apos;:v/re/d&apos;&#x4ee5;&#x4fdd;&#x7559;&#x6240;&#x6709;&#x7684;&#x5339;&#x914d;&#x884c;" ID="ID_81790031" CREATED="1455529577720" MODIFIED="1455529880611">
<node TEXT=":v/href/d" ID="ID_469163020" CREATED="1455529604757" MODIFIED="1455529896043"/>
</node>
</node>
</node>
<node TEXT="no.99&#x5c06;TODO&#x9879;&#x641c;&#x96c6;&#x81f3;&#x5bc4;&#x5b58;&#x5668;" ID="ID_391717555" CREATED="1453702989483" MODIFIED="1455530266884"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      global/markdown.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":g/TODO" ID="ID_1677658174" CREATED="1455530269005" MODIFIED="1455530274299">
<node TEXT="&#x67e5;&#x627e;&#x5305;&#x542b;TODO&#x9879;&#x7684;&#x884c;" ID="ID_220534027" CREATED="1455530335962" MODIFIED="1455530353120"/>
</node>
<node TEXT="qaq&#xa;:reg a" ID="ID_463941952" CREATED="1455530311857" MODIFIED="1455530333252">
<node TEXT="&#x6e05;&#x7a7a;&#x5bc4;&#x5b58;&#x5668;a&#x4ee5;&#x4f9b;&#x4f7f;&#x7528;" ID="ID_1353610499" CREATED="1455530356122" MODIFIED="1455530369515"/>
</node>
<node TEXT=":g/TODO/yank A&#xa;or&#xa;:g/TODO/t$" ID="ID_1005835473" CREATED="1455530323908" MODIFIED="1455530448960">
<node TEXT="&#x4f7f;&#x7528;A&#x5c06;&#x628a;&#x5185;&#x5bb9;&#x4f9d;&#x6b21;&#x9644;&#x52a0;&#x81f3;&#x5bc4;&#x5b58;&#x5668;a, &#x5982;&#x679c;&#x4f7f;&#x7528;a&#x4f1a;&#x6709;&#x95ee;&#x9898;" ID="ID_1291117923" CREATED="1455530384140" MODIFIED="1455530413291"/>
</node>
<node TEXT="&#x6269;&#x5c55;" ID="ID_488465866" CREATED="1455530464454" MODIFIED="1455530528162">
<node TEXT="&#x5c06;:global&#x547d;&#x4ee4;&#x4e0e;:bufdo&#x6216;&#x8005;:argdo&#x4e00;&#x8d77;&#x642d;&#x914d;&#x4f7f;&#x7528;, &#x53c2;&#x8003;no.96" ID="ID_214875577" CREATED="1455530475444" MODIFIED="1455530521476"/>
</node>
</node>
<node TEXT="no.100&#x5c06;CSS&#x6587;&#x4ef6;&#x4e2d;&#x6240;&#x6709;&#x89c4;&#x5219;&#x7684;&#x5c5e;&#x6027;&#x6309;&#x7167;&#x5b57;&#x6bcd;&#x6392;&#x5e8f;" ID="ID_1147381429" CREATED="1453702989484" MODIFIED="1453702989484"/>
</node>
</node>
</map>

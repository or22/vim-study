<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="PartII-File" ID="ID_237969410" CREATED="1451538463754" MODIFIED="1451539059599" LINK="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;.mm">
<edge STYLE="bezier" COLOR="#00007c" WIDTH="thin"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="06-&#x7ba1;&#x7406;&#x591a;&#x4e2a;&#x6587;&#x4ef6;" POSITION="right" ID="ID_682612053" CREATED="1451538537173" MODIFIED="1453283847619">
<edge COLOR="#7c007c"/>
<node TEXT="no.36&#x7528;&#x7f13;&#x51b2;&#x533a;&#x5217;&#x8868;&#x7ba1;&#x7406;&#x6253;&#x5f00;&#x7684;&#x6587;&#x4ef6;" ID="ID_1664272328" CREATED="1453283942240" MODIFIED="1453528335785"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      code/files
    </p>
  </body>
</html>
</richcontent>
<node TEXT="vim *.txt :ls :bnext :bprev :bfirst :blast" ID="ID_1447213884" CREATED="1453528320921" MODIFIED="1453528374656"/>
<node TEXT=":buffers :bn :bp :bf :bl" ID="ID_303602861" CREATED="1453528376059" MODIFIED="1453528836424"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h :b
    </p>
    <p>
      #&#36718;&#25442;&#25991;&#20214;
    </p>
    <p>
      %&#24403;&#21069;&#25991;&#20214;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT=":bufdo" ID="ID_1507750950" CREATED="1453528542904" MODIFIED="1453528578339"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h :bufdo
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT=":bdelete N1 N2 N3 :{N,M}bd" ID="ID_1390674510" CREATED="1453528690826" MODIFIED="1453528734242"/>
<node TEXT="&lt;C-^&gt;&#x5f53;&#x524d;&#x6587;&#x4ef6;&#x4e0e;&#x8f6e;&#x6362;&#x6587;&#x4ef6;&#x95f4;&#x5feb;&#x901f;&#x5207;&#x6362;" ID="ID_1089593587" CREATED="1453528447817" MODIFIED="1453528828399"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      plugin :tim pope-unimpaired.vim
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="no.37" ID="ID_220676987" CREATED="1453528633894" MODIFIED="1453528638962"/>
</node>
<node TEXT="no.37&#x7528;&#x53c2;&#x6570;&#x5217;&#x8868;&#x5c06;&#x7f13;&#x51b2;&#x533a;&#x5206;&#x5f00;" ID="ID_1299551465" CREATED="1453283951212" MODIFIED="1453529029050"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      code/files/letters
    </p>
    <p>
      files/.chapters
    </p>
  </body>
</html>
</richcontent>
<node TEXT=":args" ID="ID_1313200236" CREATED="1453528791346" MODIFIED="1453528822652"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      [] &#27963;&#21160;&#25991;&#20214;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="argdo write" ID="ID_182894036" CREATED="1453528581699" MODIFIED="1453529497531"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h :argdo
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="glob&#x6a21;&#x5f0f;" ID="ID_1028783576" CREATED="1453528853641" MODIFIED="1453528860244">
<node TEXT=":args *.*" ID="ID_379102323" CREATED="1453528913686" MODIFIED="1453528920247">
<node TEXT="&#x5f53;&#x524d;&#x76ee;&#x5f55;&#x4e0b;&#x6240;&#x6709;&#x6587;&#x4ef6;" ID="ID_1695909099" CREATED="1453528943610" MODIFIED="1453528948653"/>
</node>
<node TEXT=":args **/*.js" ID="ID_981362313" CREATED="1453528922152" MODIFIED="1453528927797">
<node TEXT="&#x5f53;&#x524d;&#x76ee;&#x5f55;&#x4e0b;&#x53ca;&#x5b50;&#x76ee;&#x5f55;&#x4e2d;&#x7684;&#x6240;&#x6709;.js&#x6587;&#x4ef6;" ID="ID_1727847355" CREATED="1453528950519" MODIFIED="1453528966159"/>
</node>
<node TEXT=":args **/*.*" ID="ID_1956753840" CREATED="1453528934303" MODIFIED="1453528940614">
<node TEXT="&#x5f53;&#x524d;&#x76ee;&#x5f55;&#x4e0b;&#x53ca;&#x5b50;&#x76ee;&#x5f55;&#x4e2d;&#x6240;&#x6709;&#x6587;&#x4ef6;" ID="ID_1375312758" CREATED="1453528969135" MODIFIED="1453529000432"/>
</node>
<node TEXT=":args `cat .chapters`" ID="ID_19231205" CREATED="1453529037277" MODIFIED="1453529161247">
<font NAME="DejaVu Sans Mono"/>
<node TEXT="&#x6587;&#x4ef6;.chapters&#x4e2d;&#x7684;&#x6587;&#x4ef6;" ID="ID_816501358" CREATED="1453529173097" MODIFIED="1453529196115"/>
</node>
</node>
<node TEXT=":next :prev" ID="ID_566156632" CREATED="1453529215041" MODIFIED="1453529224935"/>
<node TEXT="no.69 no.96" ID="ID_737628413" CREATED="1453529236455" MODIFIED="1453529242150"/>
</node>
<node TEXT="no.38&#x7ba1;&#x7406;&#x9690;&#x85cf;&#x7684;&#x7f13;&#x51b2;&#x533a;" ID="ID_1429633417" CREATED="1453283951409" MODIFIED="1453529306600"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      code/files
    </p>
  </body>
</html>
</richcontent>
<node TEXT="vim *.txt Go :ls" ID="ID_730065471" CREATED="1453528320921" MODIFIED="1453529300226"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      +&#20195;&#34920;&#23384;&#22312;&#20462;&#25913;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT=":bnext!" ID="ID_1818016467" CREATED="1453529334059" MODIFIED="1453529365350"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      h&#34920;&#31034;&#38544;&#34255;&#32531;&#20914;&#21306;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT=":write(&#x5199;&#x5165;) :edit!(&#x64a4;&#x9500;&#x4fee;&#x6539;) :wa[ll] :qa[ll]" ID="ID_627133895" CREATED="1453529385037" MODIFIED="1453529440471"/>
<node TEXT="set hidden" ID="ID_1235579391" CREATED="1453529463681" MODIFIED="1453529481729"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h hidden
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="no.39&#x5c06;&#x5de5;&#x4f5c;&#x533a;&#x5207;&#x5206;&#x6210;&#x7a97;&#x53e3;" ID="ID_1564163616" CREATED="1453283951542" MODIFIED="1453510629387">
<node TEXT="&#x5207;&#x5206;&#x7a97;&#x53e3;" ID="ID_1022720185" CREATED="1453529602065" MODIFIED="1453529605965">
<node TEXT="&lt;C-w&gt;s" ID="ID_263862527" CREATED="1453529508805" MODIFIED="1453529583282"/>
<node TEXT="&lt;C-w&gt;v" ID="ID_97839087" CREATED="1453529583925" MODIFIED="1453529584675"/>
<node TEXT=":sp[lit] {file}" ID="ID_641546051" CREATED="1453529551634" MODIFIED="1453529563311"/>
<node TEXT=":vsp[lit] {file}" ID="ID_368586306" CREATED="1453529564632" MODIFIED="1453529575690"/>
</node>
<node TEXT="&#x7a97;&#x53e3;&#x95f4;&#x79fb;&#x52a8;" ID="ID_1969732155" CREATED="1453529592293" MODIFIED="1453529599348">
<node TEXT="&lt;C-w&gt;w" ID="ID_1768713484" CREATED="1453529607947" MODIFIED="1453529629040"/>
<node ID="ID_1458774420" CREATED="1453529631054" MODIFIED="1453529631054"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &lt;C-w&gt;h
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;C-w&gt;j" ID="ID_416629723" CREATED="1453529632062" MODIFIED="1453529636421"/>
<node TEXT="&lt;C-w&gt;k" ID="ID_517716225" CREATED="1453529632500" MODIFIED="1453529638948"/>
<node TEXT="&lt;C-w&gt;l" ID="ID_35134355" CREATED="1453529632866" MODIFIED="1453529640923"/>
</node>
<node TEXT="&#x5173;&#x95ed;&#x7a97;&#x53e3;" ID="ID_1064703326" CREATED="1453529656037" MODIFIED="1453529664276">
<node TEXT="&lt;C-w&gt;c" ID="ID_1152050615" CREATED="1453529665721" MODIFIED="1453529671678"/>
<node TEXT="&lt;C-w&gt;o" ID="ID_431898871" CREATED="1453529672984" MODIFIED="1453529681385"/>
</node>
<node TEXT="&#x8c03;&#x6574;&#x7a97;&#x53e3;&#x5927;&#x5c0f;&#x53ca;&#x91cd;&#x6392;&#x7a97;&#x53e3;" ID="ID_549067861" CREATED="1453529694849" MODIFIED="1453529709157">
<node TEXT="&lt;C-w&gt;=" ID="ID_1936864108" CREATED="1453529711241" MODIFIED="1453529871372">
<font BOLD="true"/>
</node>
<node TEXT="&lt;C-w&gt;_" ID="ID_596486366" CREATED="1453529711241" MODIFIED="1453529871378">
<font BOLD="true"/>
</node>
<node TEXT="&lt;C-w&gt;|" ID="ID_1205610861" CREATED="1453529711241" MODIFIED="1453529871379">
<font BOLD="true"/>
</node>
<node TEXT="N&lt;C-w&gt;_" ID="ID_1757410833" CREATED="1453529711241" MODIFIED="1453529754882"/>
<node TEXT="N&lt;C-w&gt;|" ID="ID_1825576917" CREATED="1453529711241" MODIFIED="1453529759586"/>
</node>
</node>
<node TEXT="no.40&#x7528;&#x6807;&#x7b7e;&#x9875;&#x5c06;&#x7a97;&#x53e3;&#x5206;&#x7ec4;" ID="ID_1176362561" CREATED="1453283951683" MODIFIED="1453510643452">
<node TEXT="&#x6253;&#x5f00;&#x53ca;&#x5173;&#x95ed;&#x6807;&#x7b7e;&#x9875;" ID="ID_96849505" CREATED="1453530008616" MODIFIED="1453614882802"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h CTRL-W_T
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":tabedit {filename}" ID="ID_1504626141" CREATED="1453529882810" MODIFIED="1453529977230"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h tabpage
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&lt;C-w&gt;T" ID="ID_32509979" CREATED="1453529987253" MODIFIED="1453529992636"/>
</node>
<node TEXT="lcd" ID="ID_693466138" CREATED="1453529932022" MODIFIED="1453529934880">
<node TEXT="&#x5207;&#x6362;&#x5f53;&#x524d;&#x7a97;&#x53e3;&#x7684;&#x672c;&#x5730;&#x5de5;&#x4f5c;&#x76ee;&#x5f55;" ID="ID_1687866122" CREATED="1453529943331" MODIFIED="1453529961579"/>
</node>
<node TEXT="&#x5728;&#x6807;&#x7b7e;&#x9875;&#x95f4;&#x5207;&#x6362;" ID="ID_744914574" CREATED="1453614772237" MODIFIED="1453614781337">
<node TEXT=":tabn[ext] {N} = ngt" ID="ID_1922655762" CREATED="1453614783402" MODIFIED="1453614823346"/>
<node TEXT=":tabn[ext] = gt" ID="ID_569672973" CREATED="1453614825191" MODIFIED="1453614840812"/>
<node TEXT=":tabp[revious] = gT" ID="ID_1980699555" CREATED="1453614845470" MODIFIED="1453614858799"/>
</node>
<node TEXT="&#x91cd;&#x6392;&#x6807;&#x7b7e;&#x9875;" ID="ID_90753759" CREATED="1453614902373" MODIFIED="1453614909017">
<node TEXT=":tabmove" ID="ID_694770302" CREATED="1453614910730" MODIFIED="1453614946242"/>
</node>
</node>
</node>
<node TEXT="07-&#x6253;&#x5f00;&#x53ca;&#x4fdd;&#x5b58;&#x6587;&#x4ef6;" POSITION="right" ID="ID_231417984" CREATED="1451538555587" MODIFIED="1453283847634">
<edge COLOR="#007c7c"/>
<node TEXT="no.41&#x7528;:edit&#x547d;&#x4ee4;&#x6253;&#x5f00;&#x6587;&#x4ef6;" ID="ID_815127668" CREATED="1453283985358" MODIFIED="1453614983944"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      files/mvc/
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":pwd :edit" ID="ID_167539870" CREATED="1453615012768" MODIFIED="1453615019059"/>
<node TEXT=":edit %&lt;Tab&gt;" ID="ID_1000707163" CREATED="1453615056874" MODIFIED="1453615288060"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h cmdline-special
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT=":edit %:h&lt;Tab&gt;" ID="ID_1394564521" CREATED="1453615089983" MODIFIED="1453615276264"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h ::h
    </p>
    <p>
      cnormap &lt;expr&gt; %% getcmdtype( ) == ':' ? ('%:h').'/' : '%%'
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.42&#x7528;:find&#x6253;&#x5f00;&#x6587;&#x4ef6;" ID="ID_1249762592" CREATED="1453283985527" MODIFIED="1453615328879"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      files/mvc
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x914d;&#x7f6e;path&#x9009;&#x9879;" ID="ID_1005049602" CREATED="1453615341438" MODIFIED="1453615443826"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h 'path'
    </p>
    <p>
      :h file-searching
    </p>
    <p>
      plugin tim pope rails.vim
    </p>
  </body>
</html>

</richcontent>
<node TEXT="set path+=app/**" ID="ID_698576991" CREATED="1453615376640" MODIFIED="1453615386130"/>
</node>
<node TEXT=":find nav" ID="ID_1790031525" CREATED="1453615463877" MODIFIED="1453615466878"/>
</node>
<node TEXT="no.43&#x4f7f;&#x7528;netrw&#x7ba1;&#x7406;&#x6587;&#x4ef6;&#x7cfb;&#x7edf;" ID="ID_693569180" CREATED="1453283985683" MODIFIED="1453615585166"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      files/mvc
    </p>
    <p>
      essential.vim
    </p>
    <p>
      set nocompatible
    </p>
    <p>
      filetype plugin on
    </p>
  </body>
</html>

</richcontent>
<node TEXT="vim ." ID="ID_1191674898" CREATED="1453615570984" MODIFIED="1453615573518"/>
<node TEXT=":E[xplore] = :edit {path} :edit . :edit %:h" ID="ID_1009092261" CREATED="1453615598242" MODIFIED="1453615674044"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h :Explore
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT=":Sexplore :Vexplore" ID="ID_567655751" CREATED="1453615706543" MODIFIED="1453615714407"/>
<node TEXT="&#x4f7f;&#x7528;netrw&#x5b8c;&#x6210;&#x66f4;&#x591a;&#x529f;&#x80fd;" ID="ID_1061788933" CREATED="1453615768969" MODIFIED="1453615919397"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h netrw-d
    </p>
    <p>
      :h netrw-%
    </p>
    <p>
      :h netrw-del
    </p>
    <p>
      :h netrw-rename
    </p>
    <p>
      :h netrw-ref
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.44&#x628a;&#x6587;&#x4ef6;&#x4fdd;&#x5b58;&#x5230;&#x4e0d;&#x5b58;&#x5728;&#x7684;&#x76ee;&#x5f55;&#x4e2d;" ID="ID_940514006" CREATED="1453283985827" MODIFIED="1453510715227">
<node TEXT="&lt;C-g&gt;" ID="ID_155834706" CREATED="1453616101819" MODIFIED="1453616136694"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h ctrl-G
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="!mkdir -p /path/to/save/file" ID="ID_634880116" CREATED="1453616177780" MODIFIED="1453616198607"/>
</node>
<node TEXT="no.45&#x4ee5;&#x8d85;&#x7ea7;&#x7528;&#x6237;&#x6743;&#x9650;&#x4fdd;&#x5b58;&#x6587;&#x4ef6;" ID="ID_1374384842" CREATED="1453283985970" MODIFIED="1453510726719">
<node TEXT=":w !sudo tee % &gt; /dev/null" ID="ID_458750280" CREATED="1453616288028" MODIFIED="1453616303363"/>
</node>
</node>
</node>
</map>

<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="01-Vim&#x89e3;&#x51b3;&#x95ee;&#x9898;&#x7684;&#x65b9;&#x5f0f;" ID="ID_1392050422" CREATED="1451538385727" MODIFIED="1451539054413" LINK="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;.mm">
<edge STYLE="bezier" COLOR="#00ff00" WIDTH="thin"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="vim -u NONE -N" POSITION="left" ID="ID_627548295" CREATED="1451536964812" MODIFIED="1451538621171"/>
<node TEXT="&#x7b80;&#x5316;&#x91cd;&#x590d;&#x6027;&#x64cd;&#x4f5c;" POSITION="right" ID="ID_330576922" CREATED="1451539077263" MODIFIED="1451539319649">
<icon BUILTIN="FreeplaneIcons32px/01Signs/yes"/>
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="no.1-&#x91cd;&#x590d;" POSITION="right" ID="ID_558591235" CREATED="1451539126484" MODIFIED="1453169854302">
<edge COLOR="#7c0000"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the_vim_way/0_mechanics.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="." ID="ID_32042271" CREATED="1451539329782" MODIFIED="1453164741030"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h .
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x91cd;&#x590d;&#x4e0a;&#x6b21;&#x7684;&#x4fee;&#x6539;" ID="ID_1334855536" CREATED="1453165432272" MODIFIED="1453165444754"/>
</node>
<node TEXT="@:" ID="ID_748318091" CREATED="1453165303731" MODIFIED="1453165307497">
<node TEXT="&#x91cd;&#x590d;&#x4e0a;&#x6b21;&#x6267;&#x884c;&#x7684;&#x547d;&#x4ee4;" ID="ID_1608051041" CREATED="1453165422118" MODIFIED="1453165429884"/>
</node>
<node TEXT="@@" ID="ID_515074261" CREATED="1453165308072" MODIFIED="1453165309483">
<node TEXT="&#x91cd;&#x590d;&#x4e0a;&#x6b21;&#x7684;&#x5b8f;" ID="ID_1069056844" CREATED="1453165397647" MODIFIED="1453165404396"/>
</node>
<node TEXT=";," ID="ID_432661095" CREATED="1453165310223" MODIFIED="1453165312170">
<node TEXT="&#x91cd;&#x590d;&#x4e0a;&#x6b21;&#x7684;fF,tT" ID="ID_1825965477" CREATED="1453165406541" MODIFIED="1453165506968"/>
</node>
<node TEXT="nN" ID="ID_1351254777" CREATED="1453165553681" MODIFIED="1453165559024">
<node TEXT="&#x91cd;&#x590d;&#x4e0a;&#x6b21;/&#x67e5;&#x627e;" ID="ID_1827777352" CREATED="1453165560446" MODIFIED="1453165567291"/>
</node>
<node TEXT="C-r u" ID="ID_1342621619" CREATED="1453165318066" MODIFIED="1453165324679">
<node TEXT="&#x64a4;&#x9500;&#x53ca;&#x91cd;&#x505a;" ID="ID_192586982" CREATED="1453165413989" MODIFIED="1453165420005"/>
</node>
<node TEXT="&amp;" ID="ID_1368528307" CREATED="1453165380384" MODIFIED="1453165382183">
<node TEXT="&#x91cd;&#x590d;&#x4e0a;&#x6b21;&#x7684;&#x66ff;&#x6362;&#x547d;&#x4ee4;" ID="ID_75599157" CREATED="1453165385162" MODIFIED="1453165394620"/>
</node>
</node>
<node TEXT="no.2-&#x4e0d;&#x8981;&#x81ea;&#x6211;&#x91cd;&#x590d;" POSITION="right" ID="ID_1764032573" CREATED="1451539251243" MODIFIED="1453169885403">
<edge COLOR="#ffff00"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the_vim_way/2_foo_bar.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x51cf;&#x5c11;&#x65e0;&#x5173;&#x79fb;&#x52a8;" ID="ID_61047220" CREATED="1451539270418" MODIFIED="1451539275624"/>
</node>
<node TEXT="no.3-&#x4ee5;&#x9000;&#x4e3a;&#x8fdb;" POSITION="right" ID="ID_1206599757" CREATED="1451539369106" MODIFIED="1453169901512">
<edge COLOR="#00007c"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the_vim_way/3_concat.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x4f7f;&#x4fee;&#x6539;&#x53ef;&#x91cd;&#x590d;&#xa;the_vim_way/3_concat.js&#xa;f+    s + &lt;ESC&gt;    ;.;.;." ID="ID_1432306138" CREATED="1451539386401" MODIFIED="1451539552107">
<icon BUILTIN="full-1"/>
</node>
<node TEXT="&#x4f7f;&#x79fb;&#x52a8;&#x53ef;&#x91cd;&#x590d;" ID="ID_78958620" CREATED="1451539558964" MODIFIED="1453164714261">
<icon BUILTIN="full-2"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h f
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x5408;&#x800c;&#x4e3a;&#x4e00;" ID="ID_1266679998" CREATED="1451539622168" MODIFIED="1451539632892">
<icon BUILTIN="full-3"/>
</node>
</node>
<node TEXT="no.4-&#x6267;&#x884c;-&#x91cd;&#x590d;-&#x56de;&#x9000;" POSITION="right" ID="ID_1446989117" CREATED="1451539556640" MODIFIED="1451539613643">
<edge COLOR="#007c00"/>
</node>
<node TEXT="no.5-&#x67e5;&#x627e;&#x5e76;&#x624b;&#x52a8;&#x66ff;&#x6362;" POSITION="right" ID="ID_1291807847" CREATED="1453165831694" MODIFIED="1453169926387">
<edge COLOR="#7c007c"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the_vim_way/1_copy_content.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="*" ID="ID_981265450" CREATED="1453165895990" MODIFIED="1453165904101"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h *
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="no.6-&#x7ed3;&#x8bc6;.&#x8303;&#x5f0f;" POSITION="right" ID="ID_1395134388" CREATED="1453168423848" MODIFIED="1453168441272">
<edge COLOR="#007c7c"/>
<node TEXT="&#x7406;&#x60f3;&#x6a21;&#x5f0f;:&#x4e00;&#x952e;&#x79fb;&#x52a8;,&#x53e6;&#x4e00;&#x952e;&#x6267;&#x884c;" ID="ID_417842406" CREATED="1453168453781" MODIFIED="1453168485696"/>
</node>
</node>
</map>

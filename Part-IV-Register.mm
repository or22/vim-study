<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Part-IV-Register" ID="ID_68747826" CREATED="1451538661117" MODIFIED="1451539064401" LINK="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;.mm">
<edge STYLE="bezier" COLOR="#007c7c" WIDTH="thin"/>
<hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="10-&#x590d;&#x5236;&#x4e0e;&#x7c98;&#x8d34;" POSITION="right" ID="ID_1790814730" CREATED="1451538683897" MODIFIED="1451538694447">
<node TEXT="no.59&#x7528;&#x65e0;&#x540d;&#x5bc4;&#x5b58;&#x5668;&#x5b9e;&#x73b0;&#x5220;&#x9664;&#x590d;&#x5236;&#x7c98;&#x8d34;&#x64cd;&#x4f5c;" ID="ID_1246941002" CREATED="1453702884648" MODIFIED="1453960875847"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      copy_and_paste/collection.js
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x8c03;&#x6362;&#x5b57;&#x7b26;" ID="ID_937113487" CREATED="1453960811291" MODIFIED="1453960818313">
<node TEXT="xp" ID="ID_1778742193" CREATED="1453960827428" MODIFIED="1453960829026"/>
</node>
<node TEXT="&#x8c03;&#x6362;&#x6587;&#x672c;&#x884c;" ID="ID_446894855" CREATED="1453960819189" MODIFIED="1453960825719">
<node TEXT="ddp" ID="ID_127444374" CREATED="1453960831296" MODIFIED="1453960832779"/>
</node>
</node>
<node TEXT="no.60&#x6df1;&#x5165;&#x7406;&#x89e3;Vim&#x5bc4;&#x5b58;&#x5668;" ID="ID_577081911" CREATED="1453702884648" MODIFIED="1453702884648">
<node TEXT="&#x5f15;&#x7528;&#x5bc4;&#x5b58;&#x5668;" ID="ID_1783354260" CREATED="1453960911950" MODIFIED="1453960917433">
<node TEXT="&quot;{register}" ID="ID_173834433" CREATED="1453960919010" MODIFIED="1453960925339"/>
</node>
<node TEXT="&#x771f;&#x6b63;&#x7684;&#x5220;&#x9664;&#x64cd;&#x4f5c;" ID="ID_130627794" CREATED="1453960973784" MODIFIED="1453960980824">
<node TEXT="&quot;_d{motion}" ID="ID_1578707249" CREATED="1453960982611" MODIFIED="1453961001751"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h quote_
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Ex&#x547d;&#x4ee4;" ID="ID_1389683646" CREATED="1453961041815" MODIFIED="1453961053674">
<node TEXT=":delete c" ID="ID_213804851" CREATED="1453961054848" MODIFIED="1453961063154"/>
<node TEXT=":put c" ID="ID_459911586" CREATED="1453961064038" MODIFIED="1453961068755"/>
<node TEXT="no.99" ID="ID_1271125647" CREATED="1453961072426" MODIFIED="1453961075004"/>
</node>
<node TEXT="&#x65e0;&#x540d;&#x5bc4;&#x5b58;&#x5668;" ID="ID_720525662" CREATED="1453961088003" MODIFIED="1453961150221"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h quote_quote
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&quot;&quot;" ID="ID_1087594622" CREATED="1453961093249" MODIFIED="1453961094629"/>
</node>
<node TEXT="&#x590d;&#x5236;&#x4e13;&#x7528;&#x5bc4;&#x5b58;&#x5668;" ID="ID_655272731" CREATED="1453961098810" MODIFIED="1453961114488">
<node TEXT="&quot;0" ID="ID_841246196" CREATED="1453961115792" MODIFIED="1453961117899"/>
</node>
<node TEXT="&#x6709;&#x540d;&#x5bc4;&#x5b58;&#x5668;" ID="ID_102790038" CREATED="1453961178687" MODIFIED="1453961183046">
<node TEXT="&quot;a-&quot;z" ID="ID_422348974" CREATED="1453961184327" MODIFIED="1453961191575"/>
</node>
<node TEXT="&#x7cfb;&#x7edf;&#x526a;&#x8d34;&#x677f;(&quot;+)&#x548c;&#x9009;&#x62e9;&#x4e13;&#x7528;&#x5bc4;&#x5b58;&#x5668;(&quot;*)" ID="ID_1517441116" CREATED="1453961215978" MODIFIED="1453961249114">
<node TEXT="&quot;+" ID="ID_38918948" CREATED="1453961272759" MODIFIED="1453961350509"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h quote+
    </p>
  </body>
</html>
</richcontent>
<node TEXT="X11&#x526a;&#x8d34;&#x677f;,&#x7528;&#x526a;&#x5207;&#x590d;&#x5236;&#x4e0e;&#x7c98;&#x8d34;&#x547d;&#x4ee4;&#x64cd;&#x4f5c;" ID="ID_913168977" CREATED="1453961281471" MODIFIED="1453961298651"/>
</node>
<node TEXT="&quot;*" ID="ID_1372203896" CREATED="1453961276631" MODIFIED="1453961390608"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h quotestar
    </p>
  </body>
</html>
</richcontent>
<node TEXT="X11&#x4e3b;&#x526a;&#x8d34;&#x677f;, &#x7528;&#x9f20;&#x6807;&#x4e2d;&#x952e;&#x64cd;&#x4f5c;" ID="ID_1528426292" CREATED="1453961300641" MODIFIED="1453961318923"/>
</node>
<node TEXT=":version" ID="ID_445533058" CREATED="1453961328366" MODIFIED="1453961370482">
<node TEXT="xterm_clipboard" ID="ID_344254256" CREATED="1453961372264" MODIFIED="1453961385230"/>
</node>
</node>
<node TEXT="&#x8868;&#x8fbe;&#x5f0f;&#x5bc4;&#x5b58;&#x5668;" ID="ID_1851686755" CREATED="1453961397993" MODIFIED="1453961404178">
<node TEXT="&quot;=" ID="ID_657780538" CREATED="1453961405345" MODIFIED="1453961407611"/>
</node>
<node TEXT="&#x5176;&#x4ed6;&#x5bc4;&#x5b58;&#x5668;" ID="ID_981899595" CREATED="1453961417111" MODIFIED="1453961421090">
<node TEXT="&quot;%" ID="ID_828125527" CREATED="1453961423781" MODIFIED="1453961427914">
<node TEXT="&#x5f53;&#x524d;&#x6587;&#x4ef6;&#x540d;" ID="ID_340128605" CREATED="1453961452434" MODIFIED="1453961460230"/>
</node>
<node TEXT="&quot;#" ID="ID_1297558357" CREATED="1453961428961" MODIFIED="1453961432961">
<node TEXT="&#x8f6e;&#x6362;&#x6587;&#x4ef6;&#x540d;" ID="ID_1911982914" CREATED="1453961462342" MODIFIED="1453961467905"/>
</node>
<node TEXT="&quot;." ID="ID_637107100" CREATED="1453961433658" MODIFIED="1453961438428">
<node TEXT="&#x4e0a;&#x6b21;&#x63d2;&#x5165;&#x7684;&#x6587;&#x672c;" ID="ID_668961986" CREATED="1453961470190" MODIFIED="1453961476796"/>
</node>
<node TEXT="&quot;:" ID="ID_1566412599" CREATED="1453961440152" MODIFIED="1453961445039">
<node TEXT="&#x4e0a;&#x6b21;&#x6267;&#x884c;&#x7684;Ex&#x547d;&#x4ee4;" ID="ID_21094484" CREATED="1453961479178" MODIFIED="1453961491979"/>
</node>
<node TEXT="&quot;/" ID="ID_439298683" CREATED="1453961445842" MODIFIED="1453961449302">
<node TEXT="&#x4e0a;&#x6b21;&#x67e5;&#x627e;&#x7684;&#x6a21;&#x5f0f;" ID="ID_759536387" CREATED="1453961494098" MODIFIED="1453961500179"/>
</node>
</node>
</node>
<node TEXT="no.61&#x7528;&#x5bc4;&#x5b58;&#x5668;&#x4e2d;&#x7684;&#x5185;&#x5bb9;&#x66ff;&#x6362;&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x7684;&#x6587;&#x672c;" ID="ID_766680556" CREATED="1453702884652" MODIFIED="1453702884652">
<node TEXT="gv" ID="ID_1321249407" CREATED="1453961515385" MODIFIED="1453961517877">
<node TEXT="&#x91cd;&#x9009;&#x4e0a;&#x6b21;&#x9ad8;&#x4eae;&#x9009;&#x533a;&#x7684;&#x5185;&#x5bb9;" ID="ID_1149819555" CREATED="1453961519023" MODIFIED="1453961528983"/>
</node>
<node TEXT="&#x4ea4;&#x6362;&#x4e24;&#x4e2a;&#x5355;&#x8bcd;" ID="ID_273995321" CREATED="1453961565690" MODIFIED="1453961570431">
<node TEXT="de mm ww ve p `m P" ID="ID_805561479" CREATED="1453961572238" MODIFIED="1453961626444">
<font NAME="DejaVu Sans Mono"/>
</node>
</node>
</node>
<node TEXT="no.62&#x628a;&#x5bc4;&#x5b58;&#x5668;&#x7684;&#x5185;&#x5bb9;&#x7c98;&#x8d34;&#x51fa;&#x6765;" ID="ID_23842041" CREATED="1453702884654" MODIFIED="1453702884654">
<node TEXT="p" ID="ID_921493596" CREATED="1453961647380" MODIFIED="1453961653385"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h p
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x7c98;&#x8d34;&#x9762;&#x5411;&#x5b57;&#x7b26;&#x7684;&#x533a;&#x57df;" ID="ID_339698159" CREATED="1453961687917" MODIFIED="1453961699603">
<node TEXT="puP Pup" ID="ID_1524767610" CREATED="1453961700614" MODIFIED="1453961708307"/>
<node TEXT="&#x7f16;&#x8f91;&#x6a21;&#x5f0f; &lt;C-r&gt;&quot; &lt;C-r&gt;0 &lt;C-r&gt;{register}" ID="ID_248574257" CREATED="1453961709273" MODIFIED="1453961740596"/>
</node>
<node TEXT="&#x7c98;&#x8d34;&#x9762;&#x5411;&#x884c;&#x7684;&#x533a;&#x57df;" ID="ID_1136549257" CREATED="1453961762263" MODIFIED="1453961773319">
<node TEXT="p P" ID="ID_170156471" CREATED="1453961774485" MODIFIED="1453961778736"/>
<node TEXT="gp gP" ID="ID_1290743132" CREATED="1453961779370" MODIFIED="1453961787264"/>
</node>
</node>
<node TEXT="no.63&#x4e0e;&#x7cfb;&#x7edf;&#x526a;&#x8d34;&#x677f;&#x8fdb;&#x884c;&#x4ea4;&#x4e92;" ID="ID_1768276791" CREATED="1453702884655" MODIFIED="1453961924473"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      copy_and_paste/fizz.rb
    </p>
  </body>
</html>
</richcontent>
<node TEXT=":set paste" ID="ID_1971000518" CREATED="1453962157737" MODIFIED="1453962176067"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h paste
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT=":set paste!" ID="ID_1506799582" CREATED="1453962167124" MODIFIED="1453962171163"/>
<node TEXT=":set pastetoggle=&lt;f5&gt;" ID="ID_482385103" CREATED="1453962177376" MODIFIED="1453962187151"/>
<node TEXT="&quot;+p" ID="ID_1993391794" CREATED="1453962188743" MODIFIED="1453962194499"/>
<node TEXT="&quot;*p" ID="ID_65856652" CREATED="1453962195428" MODIFIED="1453962197602"/>
</node>
</node>
<node TEXT="11-&#x5b8f;" POSITION="right" ID="ID_1629651477" CREATED="1451538695626" MODIFIED="1451538701520">
<node TEXT="no.64&#x5b8f;&#x7684;&#x8bfb;&#x53d6;&#x4e0e;&#x6267;&#x884c;" ID="ID_663523012" CREATED="1453702903478" MODIFIED="1453702903478">
<node ID="ID_977691050" CREATED="1453962284930" MODIFIED="1453962357519"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      q{register}<u>SOME MOTION and OPRATERATION</u>q
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="reg {register}" ID="ID_548740884" CREATED="1453962307763" MODIFIED="1453962313381"/>
<node TEXT="@{register} @@" ID="ID_1434760883" CREATED="1453962377755" MODIFIED="1453962392531"/>
<node TEXT="&#x4e32;&#x884c;&#x65b9;&#x5f0f;&#x56de;&#x653e;&#x5b8f;" ID="ID_1553834724" CREATED="1453962393871" MODIFIED="1453962402434"/>
<node TEXT="&#x5e76;&#x884c;&#x65b9;&#x5f0f;&#x56de;&#x653e;&#x5b8f;" ID="ID_1790504545" CREATED="1453962403152" MODIFIED="1453962410404">
<node TEXT="no.67 no.69" ID="ID_656120767" CREATED="1453962420007" MODIFIED="1453962427120"/>
</node>
</node>
<node TEXT="no.65&#x89c4;&#x8303;&#x5149;&#x6807;&#x4f4d;&#x7f6e;,&#x76f4;&#x8fbe;&#x76ee;&#x6807;&#x4ee5;&#x53ca;&#x4e2d;&#x6b62;&#x5b8f;" ID="ID_634506986" CREATED="1453702903478" MODIFIED="1453702903478">
<node TEXT="&#x89c4;&#x8303;&#x5149;&#x6807;&#x4f4d;&#x7f6e;" ID="ID_1387325814" CREATED="1454047229693" MODIFIED="1454047233943">
<node TEXT="&#x6211;&#x5728;&#x54ea;&#x91cc;? &#x6211;&#x4ece;&#x54ea;&#x91cc;&#x6765;? &#x6211;&#x8981;&#x53bb;&#x54ea;&#x91cc;?" ID="ID_1485448168" CREATED="1454047204431" MODIFIED="1454047228390"/>
</node>
<node TEXT="&#x7528;&#x53ef;&#x91cd;&#x590d;&#x7684;&#x52a8;&#x4f5c;&#x547d;&#x4ee4;&#x76f4;&#x8fbe;&#x76ee;&#x6807;" ID="ID_942535359" CREATED="1454047236045" MODIFIED="1454047245600"/>
<node TEXT="&#x5f53;&#x52a8;&#x4f5c;&#x547d;&#x4ee4;&#x5931;&#x8d25;&#x65f6;, &#x5b8f;&#x5c06;&#x4e2d;&#x6b62;&#x6267;&#x884c;" ID="ID_120595495" CREATED="1454047294168" MODIFIED="1454047327155"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h visualbell
    </p>
  </body>
</html>

</richcontent>
<node TEXT="10@a 100@a 1000@a" ID="ID_1717069642" CREATED="1454047373723" MODIFIED="1454047381548"/>
</node>
</node>
<node TEXT="no.66&#x52a0;&#x6b21;&#x6570;&#x56de;&#x653e;&#x5b8f;" ID="ID_1347797366" CREATED="1453702903484" MODIFIED="1454047409091"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      the_vim_way/3_concat.js
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x5f55;&#x5236;&#x5b8f;&#x5e76;&#x52a0;&#x6b21;&#x6570;&#x56de;&#x653e;&#x8868;" ID="ID_1685230287" CREATED="1454047440329" MODIFIED="1454047451653">
<node TEXT="f+ s + &lt;Esc&gt; qq;.q 22@q" ID="ID_804732827" CREATED="1454047453985" MODIFIED="1454047492755"/>
</node>
</node>
<node TEXT="no.67&#x5728;&#x8fde;&#x7eed;&#x7684;&#x6587;&#x672c;&#x884c;&#x4e0a;&#x8fdb;&#x884c;&#x91cd;&#x590d;" ID="ID_131368334" CREATED="1453702903486" MODIFIED="1454049262300"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      macros/consecutive-lines.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x5f55;&#x5236;&#x5de5;&#x4f5c;&#x5355;&#x5143;" ID="ID_1937369246" CREATED="1454049024874" MODIFIED="1454049050541">
<node ID="ID_1147246717" CREATED="1454049051087" MODIFIED="1454049051087"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      qa 0f. r) w~ j q
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="&#x4e32;&#x884c;&#x65b9;&#x5f0f;&#x56de;&#x653e;&#x5b8f;" ID="ID_1192004216" CREATED="1453962393871" MODIFIED="1453962402434">
<node TEXT="@a" ID="ID_230055245" CREATED="1454049076526" MODIFIED="1454049082821"/>
<node TEXT="3@a" ID="ID_416253195" CREATED="1454049084067" MODIFIED="1454049088055"/>
</node>
<node TEXT="&#x5e76;&#x884c;&#x65b9;&#x5f0f;&#x56de;&#x653e;&#x5b8f;" ID="ID_516084669" CREATED="1453962403152" MODIFIED="1453962410404">
<node TEXT="no.30" ID="ID_670372898" CREATED="1454049113585" MODIFIED="1454049117309"/>
<node TEXT="jVG :&apos;&lt;,&apos;&gt;normal @a" ID="ID_311676996" CREATED="1454049119282" MODIFIED="1454049146056"/>
</node>
<node TEXT="&#x4e32;&#x884c;&#x8fd8;&#x662f;&#x5e76;&#x884c;" ID="ID_1338626737" CREATED="1454049161399" MODIFIED="1454049168546">
<node TEXT="&#x6c38;&#x8fdc;&#x770b;&#x60c5;&#x51b5;" ID="ID_760939828" CREATED="1454049169754" MODIFIED="1454049174655"/>
</node>
</node>
<node TEXT="no.68&#x7ed9;&#x5b8f;&#x8ffd;&#x52a0;&#x547d;&#x4ee4;" ID="ID_529258029" CREATED="1453702903488" MODIFIED="1453702903488">
<node TEXT="q{A-Z}" ID="ID_1584828030" CREATED="1454049213796" MODIFIED="1454049220092">
<node TEXT="qA j q" ID="ID_1146993189" CREATED="1454049187253" MODIFIED="1454049210120"/>
<node TEXT="no.71" ID="ID_1364170686" CREATED="1454049228829" MODIFIED="1454049232729"/>
</node>
</node>
<node TEXT="no.69&#x5728;&#x4e00;&#x7ec4;&#x6587;&#x4ef6;&#x4e2d;&#x6267;&#x884c;&#x5b8f;" ID="ID_492235824" CREATED="1453702903490" MODIFIED="1454049257382"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      macros/ruby_module/animal.rb
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":source marcros/rc.vim" ID="ID_1751380926" CREATED="1454049278887" MODIFIED="1454049393030"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      set nocompatible
    </p>
    <p>
      filetype plugin indent on
    </p>
    <p>
      set hidden
    </p>
    <p>
      if has (&quot;autocmd&quot;)
    </p>
    <p>
      &#160;&#160;&#160;&#160;autocmd FileType ruby setlocal ts=2 sts=2 sw=2 expandtab
    </p>
    <p>
      endif
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="&#x5efa;&#x7acb;&#x76ee;&#x6807;&#x6587;&#x4ef6;&#x5217;&#x8868;" ID="ID_1665830589" CREATED="1454049378273" MODIFIED="1454049387957">
<node TEXT=":cd macros/ruby_module&#xa;:args *.rb&#xa;:args" ID="ID_1998244405" CREATED="1454049397111" MODIFIED="1454049433634"/>
</node>
<node TEXT="&#x5f55;&#x5236;&#x5b8f;" ID="ID_417508726" CREATED="1454049438102" MODIFIED="1454049443500">
<node TEXT=":first qa gg/class&lt;CR&gt; 0module Rank&lt;Esc&gt; j&gt;G Goend&lt;Esc&gt; q" ID="ID_1034056584" CREATED="1454049444501" MODIFIED="1454049486875"/>
</node>
<node TEXT="&#x5e76;&#x884c;&#x65b9;&#x5f0f;&#x56de;&#x653e;&#x5b8f;" ID="ID_1746320361" CREATED="1453962403152" MODIFIED="1453962410404">
<node TEXT=":edit!&#xa;:argdo normal @a" ID="ID_1364376616" CREATED="1454049503510" MODIFIED="1454049539613"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h :edit!
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="&#x4e32;&#x884c;&#x65b9;&#x5f0f;&#x56de;&#x653e;&#x5b8f;" ID="ID_258000414" CREATED="1453962393871" MODIFIED="1453962402434">
<node TEXT="3@a 22@a" ID="ID_1129115647" CREATED="1454049553813" MODIFIED="1454049574926"/>
</node>
<node TEXT="&#x4fdd;&#x5b58;&#x6240;&#x6709;&#x6587;&#x4ef6;&#x6539;&#x52a8;" ID="ID_1425319130" CREATED="1454049582241" MODIFIED="1454049589285">
<node TEXT=":wall" ID="ID_91967812" CREATED="1454049590357" MODIFIED="1454049612584"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h :wa
    </p>
    <p>
      :h :wn
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="no.70&#x7528;&#x8fed;&#x4ee3;&#x6c42;&#x503c;&#x7684;&#x65b9;&#x5f0f;&#x7ed9;&#x5217;&#x8868;&#x7f16;&#x53f7;" ID="ID_1159382608" CREATED="1453702903491" MODIFIED="1454049639790"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      macros/incremental.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x57fa;&#x672c;&#x7684;vim&#x811a;&#x672c;" ID="ID_639704016" CREATED="1454049643998" MODIFIED="1454049654037">
<node TEXT=":let i=0&#xa;:echo i&#xa;:let i += 1&#xa;:echo i" ID="ID_1691030013" CREATED="1454049655430" MODIFIED="1454049676732"/>
</node>
<node TEXT="&#x5f55;&#x5236;&#x5b8f;" ID="ID_1239467585" CREATED="1454049679599" MODIFIED="1454049682486">
<node TEXT=":let i=1" ID="ID_252082634" CREATED="1454049687742" MODIFIED="1454049695904"/>
<node TEXT="qa I&lt;C-r&gt;=i&lt;CR&gt;)&lt;Esc&gt;" ID="ID_383161122" CREATED="1454049697742" MODIFIED="1454049730189"/>
<node TEXT=":let i += 1" ID="ID_553353696" CREATED="1454049731609" MODIFIED="1454049738340"/>
<node TEXT="q" ID="ID_950239238" CREATED="1454049739375" MODIFIED="1454049741710"/>
</node>
<node TEXT="&#x6267;&#x884c;&#x5b8f;" ID="ID_9405481" CREATED="1454049750515" MODIFIED="1454049753410">
<node TEXT="jVG" ID="ID_115836160" CREATED="1454049764913" MODIFIED="1454049783812"/>
<node ID="ID_867132639" CREATED="1454049785091" MODIFIED="1454049785091"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :normal @a
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="no.71&#x7f16;&#x8f91;&#x5b8f;&#x7684;&#x5185;&#x5bb9;" ID="ID_91735370" CREATED="1453702903492" MODIFIED="1454049833368"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      macros/mixed-lines.txt
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&#x95ee;&#x9898;:&#x975e;&#x6807;&#x51c6;&#x683c;&#x5f0f;" ID="ID_1147608058" CREATED="1454049835124" MODIFIED="1454049889897"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h ~
    </p>
    <p>
      :h v_U
    </p>
  </body>
</html>

</richcontent>
<node TEXT="&lt;Esc&gt; &lt;C-[&gt;" ID="ID_955717583" CREATED="1454049924150" MODIFIED="1454049949223">
<node TEXT="^[" ID="ID_722274730" CREATED="1454049950403" MODIFIED="1454049957757"/>
</node>
<node TEXT="&#x9000;&#x683c;" ID="ID_231030424" CREATED="1454049959696" MODIFIED="1454049962826">
<node TEXT="&lt;80&gt;kb" ID="ID_1644976686" CREATED="1454049963901" MODIFIED="1454049972136"/>
</node>
</node>
<node TEXT="&#x5c06;&#x5b8f;&#x7c98;&#x8d34;&#x5230;&#x6587;&#x6863;&#x4e2d;" ID="ID_1223071677" CREATED="1454049907438" MODIFIED="1454049989373">
<node TEXT=":put a" ID="ID_1411390083" CREATED="1454049990840" MODIFIED="1454050021758"/>
</node>
<node TEXT="&#x7f16;&#x8f91;&#x5b8f;" ID="ID_623855653" CREATED="1454050025206" MODIFIED="1454050031790">
<node TEXT="f~ svU&lt;Esc&gt;" ID="ID_205830422" CREATED="1454050033441" MODIFIED="1454050044771"/>
</node>
<node TEXT="&#x5c06;&#x5b8f;&#x4ece;&#x6587;&#x6863;&#x590d;&#x5236;&#x56de;&#x5bc4;&#x5b58;&#x5668;" ID="ID_688404362" CREATED="1454050049764" MODIFIED="1454050062417">
<node TEXT="&quot;add" ID="ID_1338099274" CREATED="1454050063889" MODIFIED="1454050084420">
<node TEXT="&#x4f1a;&#x4ea7;&#x751f;&#x62d6;&#x5c3e;&#x5b57;&#x7b26;^J" ID="ID_667904475" CREATED="1454050085674" MODIFIED="1454050102065"/>
</node>
<node TEXT="&quot;ay$" ID="ID_1580708125" CREATED="1454050109912" MODIFIED="1454050113774"/>
</node>
<node TEXT="&#x5bf9;&#x5b8f;&#x5bc4;&#x5b58;&#x5668;&#x505a;&#x66ff;&#x6362;&#x64cd;&#x4f5c;" ID="ID_1247077327" CREATED="1454050134183" MODIFIED="1454050235092"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      :h substitute()
    </p>
    <p>
      :h function-list
    </p>
  </body>
</html>

</richcontent>
<node TEXT=":let @a=substitute(@a, &apos;\~&apos;, &apos;vU&apos;, &apos;g&apos;)" ID="ID_1070050407" CREATED="1454050137689" MODIFIED="1454050214985"/>
</node>
</node>
</node>
</node>
</map>

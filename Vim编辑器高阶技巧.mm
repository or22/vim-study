<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Vim&#x7f16;&#x8f91;&#x5668;&#x9ad8;&#x9636;&#x6280;&#x5de7;" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1451536877685"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note">
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="YaHei Consolas Hybrid" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
<font NAME="YaHei Consolas Hybrid"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<font NAME="YaHei Consolas Hybrid"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font NAME="YaHei Consolas Hybrid" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font NAME="YaHei Consolas Hybrid" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font NAME="YaHei Consolas Hybrid" SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font NAME="YaHei Consolas Hybrid" SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font NAME="YaHei Consolas Hybrid" SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="12"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<node TEXT="01-Vim&#x89e3;&#x51b3;&#x95ee;&#x9898;&#x7684;&#x65b9;&#x5f0f;" POSITION="left" ID="ID_1120759443" CREATED="1451539054440" MODIFIED="1451539054446" LINK="01-Vim&#x89e3;&#x51b3;&#x95ee;&#x9898;&#x7684;&#x65b9;&#x5f0f;.mm">
<edge STYLE="bezier" COLOR="#00ff00" WIDTH="thin"/>
</node>
<node TEXT="PartI-Mode" POSITION="left" ID="ID_1042701023" CREATED="1451539057372" MODIFIED="1453283795292" LINK="PartI-Mode.mm">
<edge STYLE="bezier" COLOR="#007c00" WIDTH="thin"/>
</node>
<node TEXT="PartII-File" POSITION="left" ID="ID_839298867" CREATED="1451539059639" MODIFIED="1453283769447" LINK="PartII-File.mm">
<edge STYLE="bezier" COLOR="#00007c" WIDTH="thin"/>
</node>
<node TEXT="Part-IV-Register" POSITION="right" ID="ID_511643799" CREATED="1451539064430" MODIFIED="1451539064434" LINK="Part-IV-Register.mm">
<edge STYLE="bezier" COLOR="#007c7c" WIDTH="thin"/>
</node>
<node TEXT="Part-V-Pattern" POSITION="right" ID="ID_1133779490" CREATED="1451539066282" MODIFIED="1451539066285" LINK="Part-V-Pattern.mm">
<edge STYLE="bezier" COLOR="#7c7c00" WIDTH="thin"/>
</node>
<node TEXT="PartVI-tools-plugins" POSITION="right" ID="ID_1416189759" CREATED="1451539068074" MODIFIED="1451539068076" LINK="PartVI-tools-plugins.mm">
<edge STYLE="bezier" COLOR="#ff0000" WIDTH="thin"/>
</node>
<node TEXT="Append-diy" POSITION="right" ID="ID_535149181" CREATED="1451539069999" MODIFIED="1451539070001" LINK="Append-diy.mm">
<edge STYLE="bezier" COLOR="#0000ff" WIDTH="thin"/>
</node>
<node TEXT="Part-III-MoveFast" POSITION="left" ID="ID_1293854218" CREATED="1451539061646" MODIFIED="1451539061648" LINK="Part-III-MoveFast.mm">
<edge STYLE="bezier" COLOR="#7c007c" WIDTH="thin"/>
</node>
</node>
</map>
